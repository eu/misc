/*
 * Next bigger number with the same digits
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE 20

int factorial(int n)
{
	if (n<0) 
		return 0;
	return (!n) ? 1 : n * factorial(n-1);
}

void permutations(char *a)
{
	int len = strlen(a);
	register int i=len-2, j, k;
	char swap;

	while (i >= 0 && a[i] >= a[i+1]) 
		--i;

	if (i >= 0) {
		j = i+1;
		while (a[i] < a[j+1])
			j++;
		swap = a[i];
		a[i] = a[j];
		a[j] = swap;
	}

	for (j=i+1, k=len-1; j<k; j=j+1, k=k-1) {
		swap = a[j]; 
		a[j] = a[k]; 
		a[k] = swap;
	}
}

long long next_bigger_number(long long n)
{
	char a[SIZE];
	sprintf(a, "%lld", n);
	int num = factorial(strlen(a));
	long long result = -1;

	for (register int i=0; i<num; i++) { 
		permutations(a);
		long long r = atoll(a);
		if (r > n && (r < result || result == -1))
			result = r;
		if (i==0)
			break;
	}
	return result;
}

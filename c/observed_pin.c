/*
 * The observed PIN
 */

#include <stdlib.h>

int pins_size;

const char** get_pins(const char* observed, size_t* count)
{
	const char keypad[10][6] = {
		{'0', '8'},
		{'1', '2', '4'},
		{'2', '1', '3', '5'},
		{'3', '2', '6'},
		{'4', '1', '5', '7'},
		{'5', '2', '4', '6', '8'},
		{'6', '3', '5', '9'},
		{'7', '4', '8'},
		{'8', '5', '7', '9', '0'},
		{'9', '6', '8'}
	};
	const char keypad_len[10] = {1, 2, 3, 2, 3, 4, 3, 2, 4, 2};
	int s[] = {0, 0, 0, 0, 0, 0, 0, 0};
	int buf[] = {0, 0, 0, 0, 0, 0, 0, 0};
	register int i, z, k;

	int code_size = strlen(observed);
	for (i=0; i<code_size; i++)
		s[i] = keypad_len[observed[i]-'0'];

	for (pins_size=1, i=0; i<code_size; i++)
		pins_size *= s[i]+1;
	*count = pins_size;

	char **pins = (char **) malloc(pins_size * sizeof(char *));
	for(i=0; i<pins_size; i++)
		*(pins+i) = (char *) malloc(pins_size * sizeof(9));

	for (int j=0; j<pins_size; j++) {
		for (k=code_size-1; k>=0; k--) {
			if (buf[k] < s[k]) {
				buf[k]++;
				break;
			} else {
				for (int z=k; z<code_size; z++)
					buf[z] = 0;
			}
		}
		for (i=0; i<code_size; i++)
			pins[j][i] = keypad[observed[i]-'0'][buf[i]];
		pins[j][i] = 0;
	}
	return pins;
}

void free_pins(const char ** pins) {
	for (register int i=0; i<pins_size; i++)
		free(*(pins+i));
	free(pins);
}

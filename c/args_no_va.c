/*
 * Variable Number of Arguments in C, without "va"

   movl	$9, %edi
   movl	$8, %esi
   movl	$7, %edx
   movl	$6, %ecx
   movl	$5, %r8d
   movl	$4, %r9d
   movl	$3, (%rsp)
   movl	$2, 8(%rsp)
   movl	$1, 16(%rsp)
   movl	$0, 24(%rsp)
   ...
*/

int n_sum(int n, ...)
{
	int p[6];
	__asm__("movl %%edi, %0" : "=r" (p[0]));
	__asm__("movl %%esi, %0" : "=r" (p[1]));
	__asm__("movl %%edx, %0" : "=r" (p[2]));
	__asm__("movl %%ecx, %0" : "=r" (p[3]));
	__asm__("movl %%r8d, %0" : "=r" (p[4]));
	__asm__("movl %%r9d, %0" : "=r" (p[5]));

	int i, res=0;
	for (i=1; i <= ((n<6) ? n : 5); i++)
		res += p[i];

	if (n<6)
		return res;

	int *stack = __builtin_frame_address(0);
	for(i=0; i<(n-5); i++) 
		res += *(stack + 4 +2*i);

	return res;
}

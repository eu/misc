/*
 * Chess Fun #1: Chess Board Cell Color
 */

#include <stdbool.h>

bool chess_board_cell_color(const char* cell1, const char *cell2)
{
	return ('A'-cell1[0] + cell1[1])%2 == ('A'-cell2[0] + cell2[1])%2;
}

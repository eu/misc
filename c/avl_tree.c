/*
 * AVL Tree (Adelson-Velsky and Landis)
 */

#include <stdio.h>
#include <stdlib.h>

#define MAX(a, b) (a) > (b) ? (a) : (b)

typedef struct node_t
{
	unsigned char height;
	struct node_t *left;
	struct node_t *right;
	int key;
} node;

node *root = NULL;

node *new_node(int key)
{
	node *n = (node *) malloc(sizeof(node));
	if (!n)
		return NULL;

	n->left = NULL;
	n->right = NULL;
	n->height = 1;
	n->key = key;

	if (!root)
		root = n;
	return n;
}

unsigned char height(node *n)
{
	return (n) ? n->height : 0;
}

int bfactor(node *n)
{
	return height(n->right) - height(n->left);
}

void fix_height(node *n)
{
	unsigned char l = height(n->left);
	unsigned char r = height(n->right);
	n->height = MAX(l, r);
	n->height++;
}

node *rotate_right(node *n)
{
	if (n == root) 
		root = n->left;

	node *m = n->left;
	n->left = n->right;
	m->right = n;
	fix_height(n);
	fix_height(m);
	return m;
}

node *rotate_left(node *n)
{
	if (n == root) 
		root = n->right;

	node *m = n->right;
	n->right = m->left;
	m->left = n;
	fix_height(n);
	fix_height(m);
	return m;
}

node *balance(node *n)
{
	fix_height(n);
	if (bfactor(n) == 2) {
		if (bfactor(n->right) < 0)
			n->right = rotate_right(n->right);
		return rotate_left(n);
	}
	if (bfactor(n) == -2) {
		if (bfactor(n->left) > 0)
			n->left = rotate_left(n->left);
		return rotate_right(n);
	}
	return n; 
}

node *insert(node *n, int k)
{
	if (!n)
		return new_node(k);
	if (k < n->key)
		n->left = insert(n->left, k);
	else
		n->right = insert(n->right, k);
	return balance(n);
}

node *find_min(node *n) 
{
	return n->left ? find_min(n->left) : n;
}

node *remove_min(node *n) 
{
	if (!n->left)
		return n->right;
	n->left = remove_min(n->left);
	return balance(n);
}

node *del(node *n, int k) 
{
	if (!n)
		return 0;
	if (k < n->key)
		n->left = del(n->left, k);
	else if (k > n->key)
		n->right = del(n->right, k);
	else {
		node *l = n->left;
		node *r = n->right;
		free(n);
		if (!r)
			return l;
		node* min = find_min(r);
		min->right = remove_min(r);
		min->left = l;
		return balance(min);
	}
	return balance(n);
}

void display(node *n)
{
	int left = (n->left) ? n->left->key : 0;
	int right = (n->right) ? n->right->key : 0;

	printf("[%d]<-[%d]->[%d]\n", left, n->key, right);

	if (n->left)
		display(n->left);
	if (n->right)
		display(n->right);
}

int main()
{
	node *n = new_node(1);

	n = insert(n, 2);
	n = insert(n, 3);
	n = insert(n, 4);
	n = insert(n, 5);
	n = insert(n, 5);

	display(root); 

	return 0; 
}

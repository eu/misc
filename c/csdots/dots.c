/*
 * Copyright (c) 2006 Eugene P. <pieu@mail.ru>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <winsock2.h>
#include <except.h>
#include <windows.h>

char class_name[] = "Dots";
HINSTANCE hinst;

#include "dotsr.h"
#include "defines.h"
#include "variables.h"
#include "dialogs.h"
#include "dotsr.h"
#include "dots.h"

#include "field_procs.h"

#include "ai.h"
#include "aisun.h"
#include "aisres.h"
#include "aiarea.h"
#include "aidynsa.h"
#include "aiscanner.h"
#include "aicheck.h"

#include "net_game.h"

#include "about_dialog.h"
#include "colors_dialog.h"
#include "scr_dialog.h"
#include "rules_dialog.h"
#include "netgame_dialog.h"
#include "msg_dialog.h"
#include "init_dialog.h"

#include "main_window.h"

int WINAPI WinMain(HINSTANCE this_inst, HINSTANCE prev_inst,
		   LPSTR command_line, int state_of_window)
{
	HWND hwnd;
        HACCEL haccel;
        MSG msg;
        WNDCLASSEX wcl;

        if (_allocate_memory()) {
		MessageBox(0, ERROR_MSG00, GE, MB_OK|MB_ICONSTOP);
		return 0;
	}

	WSADATA wsaData;
	WORD version_requested = MAKEWORD(2, 0);
	int err;
	err = WSAStartup(version_requested, &wsaData);
	if (err) {
		MessageBox(0, ERROR_MSG03, GE, MB_OK|MB_ICONSTOP);
		return 0;
	}
	if (LOBYTE(wsaData.wVersion)!=2 || HIBYTE(wsaData.wVersion)!=0) {
		MessageBox(0, ERROR_MSG04, GE, MB_OK|MB_ICONSTOP);
		WSACleanup();
		return 0;
	}

        wcl.hInstance = this_inst;
        wcl.lpszClassName = class_name;
        wcl.lpfnWndProc = main_window_proc;
        wcl.style = 0;
        wcl.cbSize = sizeof(WNDCLASSEX);
        wcl.hIcon = LoadIcon(this_inst, "ICON_BALL");
        wcl.hIconSm = LoadIcon(this_inst, "ICON_SMALL");
        wcl.hCursor = LoadCursor(NULL, IDC_ARROW);
        wcl.lpszMenuName = "DOTS_MENU";
        wcl.cbClsExtra = 0;
        wcl.cbWndExtra = 0;
        ///wcl.hbrBackground = (HBRUSH)CreateSolidBrush(RGB(255,255,128));
	wcl.hbrBackground = (HBRUSH)GetStockObject(LTGRAY_BRUSH);// WHITE_BRUSH
	if (!RegisterClassEx(&wcl))
        	return 0;

	// Calculate window size.
	int horizontal_frames = GetSystemMetrics(SM_CXFIXEDFRAME) * 2;

        int vertical_frames = GetSystemMetrics(SM_CYFIXEDFRAME)*2 +
				  	       GetSystemMetrics(SM_CYCAPTION) +
					       GetSystemMetrics(SM_CYMENU);

        int x_screenres = GetSystemMetrics(SM_CXSCREEN);
        int y_screenres = GetSystemMetrics(SM_CYSCREEN) - 32;

	int grid1, grid2;
        grid1 = (y_screenres-vertical_frames)/(_FIELD_HEIGHT - 1);
        grid2 = (x_screenres-horizontal_frames)/(_FIELD_WIDTH - 1);

        grid_size = grid1 < grid2 ? grid1 : grid2;

        int wwidth  = (FIELD_WIDTH-1) * grid_size + horizontal_frames+1;
        int wheight = (FIELD_HEIGHT-1) * grid_size + vertical_frames+1;
        int wx = (x_screenres - wwidth) / 2;
        int wy = (y_screenres - wheight) / 2;

        hwnd = CreateWindow(class_name, DOTS_CAPTION,
        		    WS_OVERLAPPEDWINDOW^WS_MAXIMIZEBOX^WS_SIZEBOX,
                            wx, wy, wwidth, wheight, HWND_DESKTOP, NULL,
                            this_inst, NULL);

        haccel = LoadAccelerators(this_inst, "DOTS_ACCELS");

	hinst = this_inst;
        // INIT DIALOG //
	DialogBox(hinst, "INIT_DIALOG", hwnd, (DLGPROC) init_dialog_proc);
       	ShowWindow(hwnd, state_of_window);
        UpdateWindow(hwnd);

	err = net_start_server(hwnd);
	HMENU hmenu;
	hmenu = GetMenu(hwnd);
	if (err) {
		///
		EnableMenuItem(hmenu, MENU_GAME_NET, MF_GRAYED);
		EnableMenuItem(hmenu, MENU_INFO_MSG, MF_GRAYED);
	}

	ghwnd = hwnd;

        while (GetMessage(&msg, NULL, 0, 0)) {
        	if (!TranslateAccelerator(hwnd, haccel, &msg)) {
        		TranslateMessage(&msg);
                	DispatchMessage(&msg);
                }
        }
	net_stop_server();
	WSACleanup();
        _free_memory();
        return msg.wParam;
}

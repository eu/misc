/*
 * Copyright (c) 2006 Eugene P. <pieu@mail.ru>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

//#define DBG_RBP2
//#define DBG_NET_CLOSESOCKET_SHOW
//#define DBG_NET_SOCKET_SHOW
//#define DBG_AI_SHOW_SUN
//#define DBG_AI_SUN_LEVEL
//#define DBG_ENABLE_GLOBAL_HDC
//#define DBG_ENABLE_RESIZE_MESSAGE
//#define DBG_AI_SHOW_SCA
//#define DBG_AI_SHOW_CHECK

#define GAME_MODEA	0
#define GAME_MODEB	1
#define ANALYZE_DEEP	100
#define ANALYZE_FAST	50

#define DEFAULT_USER_NAME "Unknown"

#define INIT_DIALOG_DELAY 2000

#define AIRET_OK	0
#define AIRET_END	1
#define AIRET_ERROR1	-1
#define AIRET_ERROR2	-2

#define AI_SUN_LEVELS	10
#define AI_ONE		1
#define AI_EXCEPT	2

#define AI_SCANNER_LESS20	6
#define AI_SCANNER_LESS50	4
#define AI_SCANNER_LESS100      2
#define AI_SCANNER_MORE100      1
#define AI_SCANNER_TD	AI_SCANNER_LESS20

#define _FIELD_WIDTH	37	// 37
#define _FIELD_HEIGHT	24	// 24

#define FIELD_WIDTH	34
#define FIELD_HEIGHT	_FIELD_HEIGHT

#define FREQ_SIZE		FIELD_WIDTH*FIELD_HEIGHT
#define DYNSA_INITIAL_SIZE	25
#define DYNSA_ADDITIONAL_SIZE	25

#define PLAYER1		1
#define PLAYER2		2

#define EMPTY		0	// zero
#define M		-1
#define MD		-2
#define MZ		-3
#define MC		-4


#define P1N		1
#define P1C		2
#define P1_DEAD		3
#define P1_RESCUED	4
#define P1_CLOSED	5

#define P2N		6
#define P2C		7
#define P2_DEAD		8
#define P2_RESCUED	9
#define P2_CLOSED	10

#define NM		0
#define EMP		-1
#define NE		-2
#define U		-10
#define T		-100
// 1 2 3 4 5 ... for sun levels

#define NETGAME_STATE_DISABLED		0
#define NETGAME_STATE_WAIT		1
#define NETGAME_STATE_PUTDOT		2

#define NETGAME_TYPE_WAIT		1
#define NETGAME_TYPE_SERVER		2
#define NETGAME_TYPE_CLIENT		3

#define DN_DBG		001
#define DN_START	100
#define DN_YX		101
#define DN_RESTART	102
#define DN_MSG		103
#define DN_ERROR	104
#define DN_BUSY		105

#define ASYNC_MSG	WM_USER+1

/*
 * Copyright (c) 2006 Eugene P. <pieu@mail.ru>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

void default_setup(HMENU);

LRESULT CALLBACK main_window_proc(HWND hwnd, UINT message, WPARAM wparam,
				  LPARAM lparam)
{
	HDC hdc;
        PAINTSTRUCT paintstruct;
	HMENU hmenu;
        int rsp;

	switch (message) {
		//==============================================================
		case WM_CREATE:
		//==============================================================
		lock = 1;
                _clear_dots_matrix();
                fp_change_scores(hwnd);
                hmenu = GetMenu(hwnd);
		default_setup(hmenu);
                player1.normal = P1N;
                player2.normal = P2N;
                player1.connected = P1C;
                player2.connected = P2C;
                player1.dead = P1_DEAD;
                player2.dead = P2_DEAD;
                player1.rescued = P1_RESCUED;
                player2.rescued = P2_RESCUED;
                player1.closed = P1_CLOSED;
                player2.closed = P2_CLOSED;
		p1_dy = p1_dx = p2_dy = p2_dx = 0;
                aigame_mode = AI_MODEA;
		lstrcpy(this_user_name, DEFAULT_USER_NAME);
		netgame_state = NETGAME_STATE_DISABLED;
		aianalyze = ANALYZE_DEEP;
		char i;
		for (i=0; i<FIELD_WIDTH; i++)
			_dots[0][i] = _dots[FIELD_HEIGHT-1][i] = M;
		for (i=1; i<FIELD_HEIGHT-1; i++)
			_dots[i][0] = _dots[i][FIELD_WIDTH-1] = M;

		lock = 0;
		break;
		//==============================================================
		case ASYNC_MSG:
		//==============================================================
		net_handlemsg(hwnd, wparam, lparam);
		break;
		//==============================================================
                case WM_LBUTTONUP:
#ifndef DBG_RBP2
		case WM_RBUTTONUP:
#endif
		//==============================================================
		if (lock) {
			MessageBeep(MB_OK);
			break;
		}
		lock = 1;
#ifdef DBG_RBP2
		rsp = fp_add_dot(LOWORD(lparam), HIWORD(lparam), P1N);
#else
		rsp = fp_add_dot(LOWORD(lparam), HIWORD(lparam));
#endif
               	if (rsp) {
			MessageBeep(MB_OK);
			lock = 0;
                        break;
		}
		hdc = GetDC(hwnd);
		if (!fp_check_walls(&player2, &player1)) {
			fp_redraw_walls(hdc, &player2);
			fp_redraw_dots(hdc, &player2);
			fp_change_scores(hwnd);
		}
		if (!fp_check_walls(&player1, &player2)) {
			fp_redraw_walls(hdc, &player1);
			fp_redraw_dots(hdc, &player2);
			fp_change_scores(hwnd);
		}
		fp_redraw_dots(hdc, &player1);
		if (netgame_state == NETGAME_STATE_PUTDOT)
			net_putdot();

#ifdef DBG_ENABLE_GLOBAL_HDC
		ghdc = hdc;
#endif
		if (netgame_state == NETGAME_STATE_DISABLED) {
			SetWindowText(hwnd, DOTS_CAPTION_AI_WAIT);
			int retval;
                	retval = ai_gamer();
			switch (retval) {
				case AIRET_ERROR1:
                        		MessageBox(hwnd, ERROR_MSG01, GE, MB_OK);
                        		SendMessage(hwnd, WM_DESTROY, 0, 0);
                        		break;
                        	case AIRET_ERROR2:
                        		MessageBox(hwnd, ERROR_MSG02, GE, MB_OK);
                        		SendMessage(hwnd, WM_DESTROY, 0, 0);
                        		break;
                        	case AIRET_END:
                        		DialogBox(hinst, "SCR_DIALOG", hwnd,
                        	  		  (DLGPROC) scr_dialog_proc);
                        		// restart.
                        		_clear_dots_matrix();
                        		InvalidateRect(hwnd, NULL, 1);
                        		fp_change_scores(hwnd);
                        		break;
				default:
					fp_blink_dot(hdc);
					if (!fp_check_walls(&player1, &player2)) {
						fp_redraw_dots(hdc, &player1);
                        			fp_redraw_walls(hdc, &player1);
                        			fp_change_scores(hwnd);
                			}
					if (!fp_check_walls(&player2, &player1)) {
						fp_redraw_dots(hdc, &player1);
						fp_redraw_walls(hdc, &player2);
						fp_change_scores(hwnd);
					}
					fp_redraw_dots(hdc, &player2);
					break;
                	}			
			SetWindowText(hwnd, DOTS_CAPTION_AI);
			ReleaseDC(hwnd, hdc);
			lock = 0;
		} else {
			netgame_state = NETGAME_STATE_WAIT;
			SetWindowText(hwnd, DOTS_CAPTION_NET_WAIT);
			ReleaseDC(hwnd, hdc);
		}
		break;
#ifdef DBG_RBP2
                //==============================================================
	 	case WM_RBUTTONUP:
		//==============================================================
		if (lock) {
			MessageBeep(MB_OK);
			break;
		}
		rsp = fp_add_dot(LOWORD(lparam), HIWORD(lparam), P2N);
               	if (rsp) {
			MessageBeep(MB_OK);
                        break;
		}

		hdc = GetDC(hwnd);
		if (!fp_check_walls(&player1, &player2)) {
			fp_redraw_dots(hdc, &player1);
                        fp_redraw_walls(hdc, &player1);
                        fp_change_scores(hwnd);
                }
		if (!fp_check_walls(&player2, &player1)) {
			fp_redraw_dots(hdc, &player1);
			fp_redraw_walls(hdc, &player2);
			fp_change_scores(hwnd);
		}
		fp_redraw_dots(hdc, &player2);
		ReleaseDC(hwnd, hdc);
		break;
#endif
		//==============================================================
        	case WM_PAINT:
		//==============================================================
                hdc = BeginPaint(hwnd, &paintstruct);
		fp_redraw_field(hdc);
                fp_redraw_dots(hdc, &player1);
                fp_redraw_dots(hdc, &player2);
		fp_redraw_walls(hdc, &player1);
		fp_redraw_walls(hdc, &player2);
		ReleaseDC(hwnd, hdc);
		break;
		//==============================================================
		case WM_COMMAND:
		//==============================================================
		switch(LOWORD(wparam)) {
			//------------------------------------------------------
			case MENU_GAME_RESTART:
			//------------------------------------------------------
			rsp = MessageBox(hwnd, MSG01, CPT01, MB_YESNO|MB_ICONQUESTION);
			if (rsp == IDYES) {
				_clear_dots_matrix();
				InvalidateRect(hwnd, NULL, 1);
				fp_change_scores(hwnd);
				if (netgame_state != NETGAME_STATE_DISABLED) {
					SOCKET socket;
					socket = acpt_socket;
					if (netgame_type == NETGAME_TYPE_CLIENT)
						socket = client_socket;
					char str[2];
					str[0] = DN_RESTART;
					send(socket, str, 1, 0);
					SetWindowText(hwnd, DOTS_CAPTION_NET_WAIT);
                                        lock = 1;
				} else {
                                	SetWindowText(hwnd, DOTS_CAPTION_AI);
					lock = 0;
                                }
			}
			break;
			//------------------------------------------------------
			case MENU_GAME_AI:
			//------------------------------------------------------
			rsp = MessageBox(hwnd, MSG02, CPT02, MB_YESNO|MB_ICONQUESTION);
			if (rsp == IDYES) {
				if (netgame_state != NETGAME_STATE_DISABLED)
					net_disconnect();
				SetWindowText(hwnd, DOTS_CAPTION_AI);
				_clear_dots_matrix();
				InvalidateRect(hwnd, NULL, 1);
				fp_change_scores(hwnd);
				lock = 0;
			}
			break;
			//------------------------------------------------------
			case MENU_GAME_NET:
			//------------------------------------------------------
			DialogBox(hinst, "NET_DIALOG", hwnd,
				        (DLGPROC) netgame_dialog_proc);
			break;
                        //------------------------------------------------------
			case MENU_GAME_QUIT:
			//------------------------------------------------------
                        if ((netgame_type==NETGAME_TYPE_SERVER) ||
                            (netgame_type==NETGAME_TYPE_CLIENT)) {
				rsp = MessageBox(hwnd, MSG06, CPT06,
                                		 MB_YESNO|MB_ICONQUESTION);

			} else {
				rsp = MessageBox(hwnd, MSG03, CPT03,
                                		 MB_YESNO|MB_ICONQUESTION);
                        }
                        if (rsp != IDYES)
                        	break;
                        SendMessage(hwnd, WM_DESTROY, 0, 0);
			break;
			//------------------------------------------------------
			case MENU_OPTIONS_COLORS:
			//------------------------------------------------------
                        rsp = DialogBox(hinst, "CD", hwnd,
				        (DLGPROC) colors_dialog_proc);
			if (rsp)
				InvalidateRect(hwnd, NULL, 1);
			break;
                        //------------------------------------------------------
			case MENU_INFO_ABOUT:
			//------------------------------------------------------
			DialogBox(hinst, "ABOUT_DIALOG", hwnd,
				  (DLGPROC) about_dialog_proc);
			break;
			//------------------------------------------------------
			case MENU_INFO_MSG:
			//------------------------------------------------------
			if ((netgame_type==NETGAME_TYPE_SERVER) ||
			    (netgame_type==NETGAME_TYPE_CLIENT)) {
				DialogBox(hinst, "MSG", hwnd,
				  	  (DLGPROC) msg_dialog_proc);
			} else
				MessageBox(hwnd, MSG04, GE, MB_OK|MB_ICONSTOP);
			break;
			//------------------------------------------------------
			case MENU_SCORES:
                        case MENU_GAME_SCORES:
			//------------------------------------------------------
			DialogBox(hinst, "SCR_DIALOG", hwnd,
                        	  (DLGPROC) scr_dialog_proc);
			break;
			//------------------------------------------------------
			case MENU_INFO_RULES:
			//------------------------------------------------------
			DialogBox(hinst, "RULES_DIALOG", hwnd,
                        	  (DLGPROC) rules_dialog_proc);
			break;
                        //------------------------------------------------------
			case MENU_SETUP_RESET:
			//------------------------------------------------------
                        hmenu = GetMenu(hwnd);
			default_setup(hmenu);
                        InvalidateRect(hwnd, NULL, 1);
			break;
			//------------------------------------------------------
			case MENU_DS_SMALL:
			//------------------------------------------------------
                        dot_size = (char) (grid_size/4);
			wall_thickness = 1;
                        InvalidateRect(hwnd, NULL, 1);
			hmenu = GetMenu(hwnd);
                        CheckMenuItem(hmenu, MENU_DS_SMALL, MF_CHECKED);
			CheckMenuItem(hmenu, MENU_DS_NORMAL, MF_UNCHECKED);
			CheckMenuItem(hmenu, MENU_DS_BIG, MF_UNCHECKED);
                        break;
			//------------------------------------------------------
			case MENU_DS_NORMAL:
			//------------------------------------------------------
                        dot_size = (char) (grid_size/3+1);
			wall_thickness = 2;
                        InvalidateRect(hwnd, NULL, 1);
			hmenu = GetMenu(hwnd);
                        CheckMenuItem(hmenu, MENU_DS_SMALL, MF_UNCHECKED);
			CheckMenuItem(hmenu, MENU_DS_NORMAL, MF_CHECKED);
			CheckMenuItem(hmenu, MENU_DS_BIG, MF_UNCHECKED);
                        break;
			//------------------------------------------------------
			case MENU_DS_BIG:
			//------------------------------------------------------
                        dot_size = (char) (grid_size/2+4);
			wall_thickness = 3;
                        InvalidateRect(hwnd, NULL, 1);
			hmenu = GetMenu(hwnd);
                        CheckMenuItem(hmenu, MENU_DS_SMALL, MF_UNCHECKED);
			CheckMenuItem(hmenu, MENU_DS_NORMAL, MF_UNCHECKED);
			CheckMenuItem(hmenu, MENU_DS_BIG, MF_CHECKED);
			break;
                        //------------------------------------------------------
			case MENU_GRID_DARK:
			//------------------------------------------------------
                        grid_rgb = 0x000000;
                        InvalidateRect(hwnd, NULL, 1);
			hmenu = GetMenu(hwnd);
                        CheckMenuItem(hmenu, MENU_GRID_DARK, MF_CHECKED);
			CheckMenuItem(hmenu, MENU_GRID_NORMAL, MF_UNCHECKED);
			CheckMenuItem(hmenu, MENU_GRID_LIGHT, MF_UNCHECKED);
			break;
                        //------------------------------------------------------
			case MENU_GRID_NORMAL:
			//------------------------------------------------------
                        grid_rgb = 0x808080;
                        InvalidateRect(hwnd, NULL, 1);
			hmenu = GetMenu(hwnd);
                        CheckMenuItem(hmenu, MENU_GRID_DARK, MF_UNCHECKED);
			CheckMenuItem(hmenu, MENU_GRID_NORMAL, MF_CHECKED);
			CheckMenuItem(hmenu, MENU_GRID_LIGHT, MF_UNCHECKED);
			break;
                        //------------------------------------------------------
			case MENU_GRID_LIGHT:
			//------------------------------------------------------
                        grid_rgb = 0xFFFFFF;
                        InvalidateRect(hwnd, NULL, 1);
			hmenu = GetMenu(hwnd);
                        CheckMenuItem(hmenu, MENU_GRID_DARK, MF_UNCHECKED);
			CheckMenuItem(hmenu, MENU_GRID_NORMAL, MF_UNCHECKED);
			CheckMenuItem(hmenu, MENU_GRID_LIGHT, MF_CHECKED);
			break;
                        //------------------------------------------------------
			case AI_MODEA:
			//------------------------------------------------------
                        aigame_mode = GAME_MODEA;
			hmenu = GetMenu(hwnd);
                        CheckMenuItem(hmenu, AI_MODEA, MF_CHECKED);
			CheckMenuItem(hmenu, AI_MODEB, MF_UNCHECKED);
			break;
                        //------------------------------------------------------
			case AI_MODEB:
			//------------------------------------------------------
                        aigame_mode = GAME_MODEB;
			hmenu = GetMenu(hwnd);
                        CheckMenuItem(hmenu, AI_MODEA, MF_UNCHECKED);
			CheckMenuItem(hmenu, AI_MODEB, MF_CHECKED);
			break;
			//------------------------------------------------------
			case AI_ANALYZE: 
			//------------------------------------------------------
			if (aianalyze == ANALYZE_DEEP) {
				aianalyze = ANALYZE_FAST;
				hmenu = GetMenu(hwnd);
				CheckMenuItem(hmenu, AI_ANALYZE, MF_UNCHECKED);
				break;
			}
			aianalyze = ANALYZE_DEEP;
			hmenu = GetMenu(hwnd);
			CheckMenuItem(hmenu, AI_ANALYZE, MF_CHECKED);
			break;
		}
                break;
		//==============================================================
        	case WM_DESTROY:
		//==============================================================
		PostQuitMessage(0);
		break;
		//==============================================================
        	case WM_CLOSE:
		//==============================================================
		if ((netgame_type==NETGAME_TYPE_SERVER) ||
		    (netgame_type==NETGAME_TYPE_CLIENT)) {
			rsp = MessageBox(hwnd, MSG06, CPT06, MB_YESNO|MB_ICONQUESTION);
			if (rsp != IDYES)
				break;

		}
		SendMessage(hwnd, WM_DESTROY, 0, 0);
		break;
                //==============================================================
		default:
		return DefWindowProc(hwnd, message, wparam, lparam);
        }

        return 0;
}
//------------------------------------------------------------- default_setup --
void default_setup(HMENU hmenu)
{
	dot_size = (char) (grid_size/3+1);
        wall_thickness = 2;
	CheckMenuItem(hmenu, MENU_DS_SMALL, MF_UNCHECKED);
        CheckMenuItem(hmenu, MENU_DS_NORMAL, MF_CHECKED);
        CheckMenuItem(hmenu, MENU_DS_BIG, MF_UNCHECKED);

        grid_rgb = 0x808080;
	CheckMenuItem(hmenu, MENU_GRID_DARK, MF_UNCHECKED);
        CheckMenuItem(hmenu, MENU_GRID_NORMAL, MF_CHECKED);
        CheckMenuItem(hmenu, MENU_GRID_LIGHT, MF_UNCHECKED);

        player1.color_rgb = 0x0000FF;
        lstrcpy(player1.color, "red");
        player2.color_rgb = 0xFF0000;
        lstrcpy(player2.color, "blue");
 }

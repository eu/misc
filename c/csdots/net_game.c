/*
 * Copyright (c) 2006 Eugene P. <pieu@mail.ru>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

int net_start_server(HWND);
void net_stop_server(void);
int net_make_connect(void);
void net_disconnect(void);
void net_putdot(void);

void net_handlemsg(HWND, WPARAM, LPARAM);
int net_accept(HWND);
void net_send_busy();
void net_close(SOCKET);
void net_read(SOCKET);
void net_connect(SOCKET, int);

static SOCKET server_socket, acpt_socket, client_socket;
static char netgame_state = NETGAME_STATE_DISABLED, netgame_type=0;
//------------------------------------------------------------- net_handlemsg --
void net_handlemsg(HWND hwnd, WPARAM wparam, LPARAM lparam)
{
	int err = WSAGETSELECTERROR(lparam);

	switch(WSAGETSELECTEVENT(lparam))
	{
		char str[100];
		case FD_CONNECT:
			///MessageBox(hwnd, "FD_CONNECT", "", MB_OK);
			if (netgame_type) {
                        	net_connect((SOCKET)wparam, err);
                        }
			break;

		case FD_ACCEPT:
			///MessageBox(hwnd, "FD_ACCEPT", "", MB_OK);
			if (!netgame_type) {
				if (net_accept(hwnd)) {
                                	lock = 0;
					netgame_type = NETGAME_TYPE_SERVER;
					netgame_state = NETGAME_STATE_WAIT;
					lstrcpy(str+1, this_user_name);
					str[0] = DN_START;
					send(acpt_socket, str,
					     strlen(this_user_name)+1, 0);
				}
			} else  {
                        	net_send_busy();
                        }
			break;
		case FD_READ:
			///MessageBox(hwnd, "FD_READ", "", MB_OK);
			net_read((SOCKET)wparam);
			break;
		case FD_CLOSE:
			///MessageBox(hwnd, "FD_CLOSE", "", MB_OK);
			net_close((SOCKET)wparam);
			break;
	}
}
//---------------------------------------------------------- net_restart_game --
void net_restart_game()
{
	lock = 1;
	_clear_dots_matrix();
	InvalidateRect(ghwnd, NULL, 1);
	fp_change_scores(ghwnd);
}
//---------------------------------------------------------------- net_putdot --
void net_putdot()
{
	char buf[4];
	SOCKET socket;
	buf[0] = DN_YX;
	buf[1] = p1_dy;
	buf[2] = p1_dx;
	socket = acpt_socket;
	if (netgame_type == NETGAME_TYPE_CLIENT)
		socket = client_socket;
	send(socket, buf, 3, 0);
}
//----------------------------------------------------------- net_stop_server --
void net_stop_server()
{
#ifdef DBG_NET_CLOSESOCKET_SHOW
MessageBox(ghwnd, "close server_socket", "dbg message", MB_OK);
#endif
	closesocket(server_socket);
}
//---------------------------------------------------------- net_start_server --
int net_start_server(HWND shwnd)
{
	SOCKADDR_IN srv_sa;
	u_short myport;
	LPHOSTENT host_info;
	struct in_addr *ip_addr;
	int err;

	lstrcpy(remote_host, "remotehost.remotedomain");
	remote_port = 1024;
#ifdef DBG_NET_SOCKET_SHOW
MessageBox(ghwnd, "create server_socket", "dbg message", MB_OK);
#endif
	server_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (server_socket == INVALID_SOCKET) {
		MessageBox(shwnd, NETERROR_MSG01, GE,
			   MB_OK|MB_ICONSTOP);
		return 1;
	}

	err = WSAAsyncSelect(server_socket, shwnd, ASYNC_MSG, FD_ACCEPT|FD_READ|
			     FD_CLOSE);
	if (err == SOCKET_ERROR) {
#ifdef DBG_NET_CLOSESOCKET_SHOW
MessageBox(ghwnd, "close server_socket", "dbg message", MB_OK);
#endif
		closesocket(server_socket);
		MessageBox(shwnd, NETERROR_MSG02, GE,
			   MB_OK|MB_ICONSTOP);
		return 1;
	}

	srv_sa.sin_family = AF_INET;
	srv_sa.sin_addr.s_addr = INADDR_ANY;
	myport = 1024;
	while(1) {
		srv_sa.sin_port = htons(myport);
		err = bind(server_socket, (sockaddr *)&srv_sa, sizeof(srv_sa));
		if (err == SOCKET_ERROR) {
			myport++;
			if (myport > 1124) {
#ifdef DBG_NET_CLOSESOCKET_SHOW
MessageBox(ghwnd, "close server_socket", "dbg message", MB_OK);
#endif
				closesocket(server_socket);
				MessageBox(shwnd, NETERROR_MSG03, GE, MB_OK|MB_ICONSTOP);
				return 1;
			}
		} else {
			this_port = myport;
			break;
		}
	}

	err = listen(server_socket, 10);
	if (err == SOCKET_ERROR) {
#ifdef DBG_NET_CLOSESOCKET_SHOW
MessageBox(ghwnd, "close server_socket", "dbg message", MB_OK);
#endif
		closesocket(server_socket);
		MessageBox(shwnd, NETERROR_MSG04, GE, MB_OK|MB_ICONSTOP);
		return 1;
	}

	gethostname(this_host, sizeof(this_host));
	if (err == SOCKET_ERROR) {
#ifdef DBG_NET_CLOSESOCKET_SHOW
MessageBox(ghwnd, "close server_socket", "dbg message", MB_OK);
#endif
		closesocket(server_socket);
		MessageBox(shwnd, NETERROR_MSG05, GE, MB_OK|MB_ICONSTOP);
		return 1;
	}

        host_info = gethostbyname(this_host);
	if (host_info == NULL) {
#ifdef DBG_NET_CLOSESOCKET_SHOW
MessageBox(ghwnd, "close server_socket", "dbg message", MB_OK);
#endif
		closesocket(server_socket);
		MessageBox(shwnd, NETERROR_MSG06, GE, MB_OK|MB_ICONSTOP);
		return 1;
	}

	ip_addr = ((LPIN_ADDR) host_info->h_addr_list[0]);
	lstrcpy(this_ip, inet_ntoa(*ip_addr));

	return 0;
}
//---------------------------------------------------------------- net_accept --
int net_accept(HWND hwnd)
{
	SOCKADDR_IN sa;
	int len;

	len = sizeof(SOCKADDR_IN);
#ifdef DBG_NET_SOCKET_SHOW
MessageBox(ghwnd, "accept acpt_socket", "dbg message", MB_OK);
#endif
	acpt_socket = accept(server_socket, (sockaddr *)&sa, &len);
	if (acpt_socket == SOCKET_ERROR) {
		MessageBox(hwnd, NETERROR_MSG07, GE, MB_OK|MB_ICONSTOP);
		return 0;
	}
	WSAAsyncSelect(acpt_socket, hwnd, ASYNC_MSG, FD_READ|FD_CLOSE);
	if (acpt_socket == SOCKET_ERROR) {
		MessageBox(hwnd, NETERROR_MSG07, GE, MB_OK|MB_ICONSTOP);
		return 0;
	}

	char str[256];
	int want;
	lstrcpy(remote_host, inet_ntoa(sa.sin_addr));
	wsprintf(str, NET_MSG01, remote_host);
	want = MessageBox(hwnd, str, NG, MB_YESNO|MB_ICONQUESTION);
	if (want != IDYES) {
#ifdef DBG_NET_CLOSESOCKET_SHOW
MessageBox(ghwnd, "close acpt_socket", "dbg message", MB_OK);
#endif
		closesocket(acpt_socket);
		return 0;
	}
	return 1;
}
//------------------------------------------------------------- net_send_busy --
void net_send_busy()
{
	SOCKET temp_socket;
	SOCKADDR_IN sa;
	int len;

	len = sizeof(SOCKADDR_IN);
#ifdef DBG_NET_SOCKET_SHOW
MessageBox(ghwnd, "accept temp_socket", "dbg message", MB_OK);
#endif
	temp_socket = accept(server_socket, (sockaddr *)&sa, &len);
        if (temp_socket == SOCKET_ERROR) {
		return;
	}
        char buf[1];
	buf[0] = DN_BUSY;
	send(temp_socket, buf, 1, 0);
#ifdef DBG_NET_CLOSESOCKET_SHOW
MessageBox(ghwnd, "close temp_socket", "dbg message", MB_OK);
#endif
        closesocket(temp_socket);
}
//------------------------------------------------------------ net_disconnect --
void net_disconnect()
{
	if (netgame_type == NETGAME_TYPE_SERVER) {
#ifdef DBG_NET_CLOSESOCKET_SHOW
MessageBox(ghwnd, "close acpt_socket", "dbg message", MB_OK);
#endif
		closesocket(acpt_socket);
        }
	if ((netgame_type==NETGAME_TYPE_CLIENT) ||
            (netgame_type==NETGAME_TYPE_WAIT)) {
#ifdef DBG_NET_CLOSESOCKET_SHOW
MessageBox(ghwnd, "close client_socket", "dbg message", MB_OK);
#endif
		closesocket(client_socket);
	}
        //SetWindowText(ghwnd, DOTS_CAPTION);
	netgame_type = 0;
	netgame_state = NETGAME_STATE_DISABLED;
}
//----------------------------------------------------------------- net_close --
void net_close(SOCKET socket)
{
#ifdef DBG_NET_CLOSESOCKET_SHOW
if (socket == server_socket)
	MessageBox(ghwnd, "close server_socket", "dbg message", MB_OK);
else if (socket == client_socket)
	MessageBox(ghwnd, "close client_socket", "dbg message", MB_OK);
else if (socket == acpt_socket)
	MessageBox(ghwnd, "close acpt_socket", "dbg message", MB_OK);
else
	MessageBox(ghwnd, "close unknown socket", "dbg message", MB_OK);
#endif
	closesocket(socket);
	netgame_type = 0;
        netgame_state = NETGAME_STATE_DISABLED;
        SetWindowText(ghwnd, DOTS_CAPTION);
        MessageBox(ghwnd, NET_MSG07, NG, MB_OK|MB_ICONSTOP);
	lock = 1;
}
//------------------------------------------------------------------ net_read --
void net_read(SOCKET socket)
{
	static char buf[2048];
	char str[256];
	memset(buf, 0, sizeof(buf));

	int n;
	n = recv(socket, buf, sizeof(buf)-1, 0);
	if (n == SOCKET_ERROR)
	{
		if (WSAGetLastError() == WSAEWOULDBLOCK)
			return;
		net_close(socket);
		MessageBox(ghwnd, NETERROR_MSG08, GE, MB_OK|MB_ICONSTOP);
		return;
	}
	switch (buf[0]) {
		case DN_START:
			net_restart_game();
			lstrcpy(remote_user_name, buf+1);
			wsprintf(str, NET_MSG02, remote_user_name, remote_host);
			MessageBox(ghwnd, str, NGS, MB_OK|MB_ICONINFORMATION);
			if (netgame_type == NETGAME_TYPE_WAIT) {
				netgame_type = NETGAME_TYPE_CLIENT;
				netgame_state = NETGAME_STATE_WAIT;
				SetWindowText(ghwnd, DOTS_CAPTION_NET_WAIT);
				lstrcpy(str+1, this_user_name);
				str[0] = DN_START;
				send(client_socket, str,
				     strlen(this_user_name)+1, 0);
			}
			if (netgame_type == NETGAME_TYPE_SERVER) {
				SetWindowText(ghwnd, DOTS_CAPTION_NET);
				netgame_state = NETGAME_STATE_PUTDOT;
				lock = 0;
			}
			break;
		case DN_YX:
			p2_dy = buf[1];
			p2_dx = buf[2];
			if (dots[p2_dy][p2_dx] != EMPTY) {
				MessageBox(ghwnd, NETERROR_MSG11, GE,
			   		   MB_OK|MB_ICONSTOP);
				SendMessage(ghwnd, WM_DESTROY, 0, 0);
				break;
			}
			dots[p2_dy][p2_dx] = P2N;
			HDC hdc;
			hdc = GetDC(ghwnd);
			fp_blink_dot(hdc);
			if (!fp_check_walls(&player1, &player2)) {
				fp_redraw_dots(hdc, &player1);
                        	fp_redraw_walls(hdc, &player1);
                        	fp_change_scores(ghwnd);
                	}
			if (!fp_check_walls(&player2, &player1)) {
				fp_redraw_dots(hdc, &player1);
				fp_redraw_walls(hdc, &player2);
				fp_change_scores(ghwnd);
			}
			fp_redraw_dots(hdc, &player2);
			SetWindowText(ghwnd, DOTS_CAPTION_NET);
			ReleaseDC(ghwnd, hdc);
			netgame_state = NETGAME_STATE_PUTDOT;
			lock = 0;
			break;
		case DN_RESTART:
				MessageBox(ghwnd, NET_MSG03, NG,
					   MB_OK|MB_ICONINFORMATION);
				net_restart_game();
				netgame_state = NETGAME_STATE_PUTDOT;
                                SetWindowText(ghwnd, DOTS_CAPTION_NET);
                                lock = 0;
			break;
		case DN_MSG:
			buf[0] = ' ';
			wsprintf(str, NET_MSG04, remote_user_name);
			MessageBox(ghwnd, buf, str, MB_OK|MB_ICONINFORMATION);
			break;
                case DN_BUSY:
                        MessageBox(ghwnd, MSG07, NG, MB_OK|MB_ICONINFORMATION);
                        break;
		default:
			// Unknown data. Reserverd.
			// need send sync request. (next ver).
			MessageBox(ghwnd, NETERROR_MSG13, NGE, MB_OK|MB_ICONSTOP);
	}
}
//--------------------------------------------------------------- net_connect --
void net_connect(SOCKET socket, int err)
{
	if (err) {
		if (err == WSAETIMEDOUT) {
			MessageBox(ghwnd, NETERROR_MSG09, NG, MB_OK|MB_ICONSTOP);
		} else {
			MessageBox(ghwnd, NETERROR_MSG10, NG,
				   MB_OK|MB_ICONSTOP);
		}
#ifdef DBG_NET_CLOSESOCKET_SHOW
if (socket == server_socket)
	MessageBox(ghwnd, "close server_socket", "dbg message", MB_OK);
else if (socket == client_socket)
	MessageBox(ghwnd, "close client_socket", "dbg message", MB_OK);
else if (socket == acpt_socket)
	MessageBox(ghwnd, "close acpt_socket", "dbg message", MB_OK);
else
	MessageBox(ghwnd, "close unknown socket", "dbg message", MB_OK);
#endif
		closesocket(socket);
                SetWindowText(ghwnd, DOTS_CAPTION);
                netgame_type = 0;
		return;
	}
	char str[100];
	wsprintf(str, DOTS_CAPTION_NET_FOUND, remote_host);
	SetWindowText(ghwnd, str);
}
//---------------------------------------------------------- net_make_connect --
int net_make_connect()
{
	if (netgame_type) {
		MessageBox(ghwnd, NET_MSG05, NGE, MB_OK|MB_ICONSTOP);
		return 0;
	}

	int err;
	SOCKADDR_IN sa;
	LPHOSTENT host_info;
#ifdef DBG_NET_SOCKET_SHOW
MessageBox(ghwnd, "create client_socket", "dbg message", MB_OK);
#endif
	client_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (client_socket == INVALID_SOCKET) {
		MessageBox(ghwnd, NETERROR_MSG01, GE, MB_OK|MB_ICONSTOP);
		return 0;
	}

	err = WSAAsyncSelect(client_socket, ghwnd, ASYNC_MSG,
			     FD_CONNECT|FD_READ|FD_CLOSE);
	if (err == SOCKET_ERROR) {
#ifdef DBG_NET_CLOSESOCKET_SHOW
MessageBox(ghwnd, "close client_socket", "dbg message", MB_OK);
#endif
		closesocket(client_socket);
		MessageBox(ghwnd, NETERROR_MSG02, GE,
			   MB_OK|MB_ICONSTOP);
		return 0;
	}

	sa.sin_family = AF_INET;
	sa.sin_port = htons(remote_port);

	unsigned long addr;
	addr = inet_addr(remote_host);
	if (addr != INADDR_NONE)
		sa.sin_addr.s_addr = addr;
	else {
		host_info = gethostbyname(remote_host);
		if (host_info == NULL) {
			MessageBox(ghwnd, NETERROR_MSG12,
				   NGE, MB_OK|MB_ICONSTOP);
#ifdef DBG_NET_CLOSESOCKET_SHOW
MessageBox(ghwnd, "close client_socket", "dbg message", MB_OK);
#endif
                        closesocket(client_socket);
			return 1;
		}
		((unsigned long *) &sa.sin_addr)[0] =
        	((unsigned long **) host_info->h_addr_list)[0][0];
	}

	connect(client_socket, (sockaddr *)&sa, sizeof(sa));
	char str[100];
	wsprintf(str, DOTS_CAPTION_NET_TRY, remote_host);
	SetWindowText(ghwnd, str);
	netgame_type = NETGAME_TYPE_WAIT;
	return 0;
}


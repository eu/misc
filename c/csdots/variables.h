/*
 * Copyright (c) 2006 Eugene P. <pieu@mail.ru>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

static HWND ghwnd;
static COLORREF grid_rgb;
static char **dots, **_dots, **xdots, **sca, *this_area;
static char lock, p2_dx, p2_dy, p1_dx, p1_dy, aigame_mode, aianalyze;
static int grid_size, dot_size, wall_thickness;
static int **area, *freq, *pos, *pos_last, **dynsa, dynsa_size;

static u_short this_port;
static char this_host[128];
static char this_ip[24];
static char this_user_name[26], remote_user_name[26];
static char remote_host[128];
static u_short remote_port;

#ifdef DBG_ENABLE_GLOBAL_HDC
HDC ghdc;
#endif

static struct dot
{
	int scr;
	COLORREF color_rgb;
	char color[10];
	char normal, connected, dead, rescued, closed;
        char **sun;
        int *sres;
} player1, player2;


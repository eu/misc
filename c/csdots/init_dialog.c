/*
 * Copyright (c) 2006 Eugene P. <pieu@mail.ru>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#define INIT_TIMER 1000

BOOL CALLBACK init_dialog_proc(HWND hdwnd, UINT message, WPARAM wparam,
			       LPARAM lparam)
{
	switch (message) {
                case WM_TIMER:
                	KillTimer(hdwnd, INIT_TIMER);
                        EndDialog(hdwnd, 1);
                        break;
		case WM_INITDIALOG:
			SetFocus(hdwnd);
                        SetTimer(hdwnd, INIT_TIMER, INIT_DIALOG_DELAY, NULL);
			break;
	}
	return 0;
}

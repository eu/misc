/*
 * Copyright (c) 2006 Eugene P. <pieu@mail.ru>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

BOOL CALLBACK netgame_dialog_proc(HWND hdwnd, UINT message, WPARAM wparam,
		  		  LPARAM lparam)
{
	switch (message) {
		case WM_COMMAND:
		switch (LOWORD(wparam)) {
			case NET_CLOSE:
                        	GetDlgItemText(hdwnd, NET_EDIT4, this_user_name, 26);				
				EndDialog(hdwnd, 0);
				return 1;
			case NET_CTRL:
                        	if (netgame_type) {
					net_disconnect();
					SetDlgItemText(hdwnd, NET_CTRL, "Co&nnect");
					SetWindowText(ghwnd, DOTS_CAPTION);
					break;
				}
                                if (!netgame_type) {
					char ch_port[6];
                                        GetDlgItemText(hdwnd, NET_EDIT4, this_user_name, 26);
					GetDlgItemText(hdwnd, NET_EDIT5,
                                        	       remote_host, 100);
					GetDlgItemText(hdwnd, NET_EDIT6,
                                        	       ch_port, 5);
					remote_port = (u_short)(atoi(ch_port));

					if (!net_make_connect())
						EndDialog(hdwnd, 0);
                                }
				break;
		}
		break;

		case WM_INITDIALOG:
			SetFocus(hdwnd);
			SetDlgItemText(hdwnd, NET_EDIT1, this_host);
			SetDlgItemText(hdwnd, NET_EDIT2, this_ip);
			SetDlgItemText(hdwnd, NET_EDIT4, this_user_name);
			SetDlgItemText(hdwnd, NET_EDIT5, remote_host);
			char aport[10];
			itoa(this_port, aport, 10);
			SetDlgItemText(hdwnd, NET_EDIT3, aport);
			itoa(remote_port, aport, 10);
			SetDlgItemText(hdwnd, NET_EDIT6, aport);                        
			SendDlgItemMessage(hdwnd, NET_EDIT5, EM_SETSEL, 0, -1);
			
			break;

		case WM_ACTIVATE:
			if (netgame_type)
                        	SetDlgItemText(hdwnd, NET_CTRL, "&Disconnect");
                        else
                        	SetDlgItemText(hdwnd, NET_CTRL, "Co&nnect");
			break;
                case WM_CLOSE:
	                EndDialog(hdwnd, 0);
	}
	return 0;
}

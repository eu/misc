/*
 * Copyright (c) 2006 Eugene P. <pieu@mail.ru>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

BOOL CALLBACK colors_dialog_proc(HWND hdwnd, UINT message, WPARAM wparam,
				LPARAM lparam)
{
	static COLORREF color1, color2;
	switch (message) {
		case WM_COMMAND:
		switch (LOWORD(wparam)) {
			case CD_RED1:
			color1 = 0x0000FF;
			break;
			case CD_BLUE1:
			color1 = 0xFF0000;
			break;
			case CD_BLACK1:
			color1 = 0x0;
			break;
		        case CD_RED2:
			color2 = 0x0000FF;
			break;
			case CD_BLUE2:
			color2 = 0xFF0000;
			break;
			case CD_BLACK2:
			color2 = 0x0;
			break;

			case CD_OK:
			player1.color_rgb = color1;
			player2.color_rgb = color2;
			if (color1==0xFF0000)
				lstrcpy(player1.color, "blue");
			else if (color1==0x0000FF)
				lstrcpy(player1.color, "red");
			else lstrcpy(player1.color, "black");
			if (color2==0xFF0000)
				lstrcpy(player2.color, "blue");
			else if (color2==0x0000FF)
				lstrcpy(player2.color, "red");
			else lstrcpy(player2.color, "black");
			case CD_CANCEL:
			EndDialog(hdwnd, 1);
			return 1;
		}
		break;

		case WM_INITDIALOG:
		int rb;
		color1 = player1.color_rgb;
		if (color1 == 0x0000FF)
			rb = CD_RED1;
		else if (color1 == 0xFF0000)
			rb = CD_BLUE1;
		else if (color1 == 0x0)
			rb = CD_BLACK1;
		SendDlgItemMessage(hdwnd, rb, BM_SETCHECK, BST_CHECKED, 0);

		color2 = player2.color_rgb;
		if (color2 == 0x0000FF)
			rb = CD_RED2;
		else if (color2 == 0xFF0000)
			rb = CD_BLUE2;
		else if (color2 == 0x0)
			rb = CD_BLACK2;
		SendDlgItemMessage(hdwnd, rb, BM_SETCHECK, BST_CHECKED, 0);
		SetFocus(hdwnd);
		break;

                case WM_CLOSE:
                EndDialog(hdwnd, 0);
	}
	return 0;
}

/*
 * Copyright (c) 2006 Eugene P. <pieu@mail.ru>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

void ai_empdead(struct dot*, struct dot*, int *, int*);
int ai_gamer(void);
int ai_simple(void);
int ai_deep(char *);
//
void aisun_ignore_bad_pos(char **, char);
#ifdef DBG_AI_SUN_LEVEL && DBG_ENABLE_GLOBAL_HDC
void aisun_show_levels(char **, char);
#endif
int aisun_make_levels(struct dot*);
int aisun_unenm(struct dot*, struct dot*);
void aisun_clear_level(char **, char);
char aisun_spos(char **, char, char, char);
char aisun_dpos(char **, char, char, char);
int next_sun(char **, char);
//
int aisres_whois_victim(struct dot**, struct dot**, char *sres_index);
void aisres_fill(struct dot*, struct dot*);
//
void aiarea_create(char **, char, int*);
int open_area(char **, char);
void mark_area(char **, char);
void close_area(char **);
void write_area(char **, int);
//
int aidynsa_except(struct dot*, struct dot*, int, int*);
int aidynsa_one(struct dot*, struct dot*, int, int*);
void aidynsa_sort(int);
void swapix(int, int);
//
int aiscanner(char, struct dot*, struct dot*, int);
char check_max_area_size(int);
void clear_sca(void);
int test_one(struct dot*, struct dot*, char);
int test_except(struct dot*, struct dot*, char, int);
void prepare_this_area(int);
#ifdef DBG_AI_SHOW_SCA && DBG_ENABLE_GLOBAL_HDC
void aiscanner_show_sca(void);
#endif
//
int aicheck_1dot(void);
int aicheck_onegp(char);
int aicheck_near(void);
int checkp1_1dot(char, char);
int checkp2_1dot(char, char);
int aicheck_this_dot(char, char);
char move_dot(char, char);
int p1dist(char, char);
char sca_around(char, char);

//------------------------------------------------------------------ ai_gamer --
int ai_gamer()
{
	int bug;

	if (!aicheck_1dot())
		return AIRET_OK;

	char attack_mode;
        bug = ai_deep(&attack_mode);
	if (!bug) {
        	if (!aicheck_onegp(attack_mode))
        		return AIRET_OK;
	} else if (bug < 0) {
        	return bug;
        }

        if (!aicheck_near())
        	return AIRET_OK;

	if (!ai_simple())
		return AIRET_OK;

	return AIRET_END;
}
//------------------------------------------------------------------- ai_deep --
int ai_deep(char *attack_mode)
{
	struct dot *attacking, *victim;
        int bug, dynsa_total, check;
        char sres_index, i;

        aisun_unenm(&player1, &player2);
        aisun_unenm(&player2, &player1);
        aisun_make_levels(&player1);
        aisun_make_levels(&player2);
        aisres_fill(&player1, &player2);
        aisres_fill(&player2, &player1);
        check = aisres_whois_victim(&attacking, &victim, &sres_index);

	if (check)
		return 1;

	if (attacking == &player2)
		*attack_mode = 1;
	else
		*attack_mode = 0;

        for (i=AI_SUN_LEVELS; i>sres_index; i--)
        	aisun_clear_level(attacking->sun, i);

	for (i=AI_SUN_LEVELS; i>0; i--)
        	aisun_ignore_bad_pos(attacking->sun, i);
#ifdef DBG_AI_SUN_LEVEL && DBG_ENABLE_GLOBAL_HDC
	if (sres_index) {
		for (char ix=1; ix<=sres_index; ix++) {
        		aisun_show_levels(attacking->sun, ix);
	        }
        }
#endif
	if (aigame_mode == GAME_MODEA)
        	sres_index = 1;
	int areas_total;
	aiarea_create(attacking->sun, sres_index, &areas_total);
	aisun_clear_level(attacking->sun, sres_index);
	bug = aidynsa_one(attacking, victim, areas_total, &dynsa_total);
        if (bug) {
		return AIRET_ERROR1;
	}
        if (dynsa_total) {
                aidynsa_sort(dynsa_total);
		check = aiscanner(AI_ONE, attacking, victim, dynsa_total);
		if (check)
			return 1;
#ifdef DBG_AI_SHOW_SCA && DBG_ENABLE_GLOBAL_HDC
        	aiscanner_show_sca();
#endif
		return 0;

        } else {
		bug = aidynsa_except(attacking, victim, areas_total,
        			     &dynsa_total);
                if (bug) {
			return AIRET_ERROR2;
		}
        }
        if (dynsa_total) {
        	aidynsa_sort(dynsa_total);
		check = aiscanner(AI_EXCEPT, attacking, victim, dynsa_total);
		if (check)
			return 1;
#ifdef DBG_AI_SHOW_SCA && DBG_ENABLE_GLOBAL_HDC
        	aiscanner_show_sca();
#endif
		return 0;
        }
	return 1;
}
//----------------------------------------------------------------- ai_simple --
int ai_simple()
{
	char dy, dx, d1, d2, d3, d4, d5, d6, d7, d8, bst_dy, bst_dx;
	for (dx=1; dx<FIELD_WIDTH-1; dx++) {
		for (dy=1; dy<FIELD_HEIGHT-1; dy++) {
			if (dots[dy][dx] == P1N) {
				d1 = dots[dy-1][dx];
				d2 = dots[dy-1][dx+1];
				d3 = dots[dy][dx+1];
				d4 = dots[dy+1][dx+1];
				d5 = dots[dy+1][dx];
				d6 = dots[dy+1][dx-1];
				d7 = dots[dy][dx-1];
				d8 = dots[dy-1][dx-1];
				if (d1==EMPTY && d2==EMPTY && d3==EMPTY &&
				     d4==EMPTY && d5==EMPTY && d6==EMPTY &&
				     d7==EMPTY && d8==EMPTY) {
					char tdx;
					if (dy == 1)
						tdx = (char)(dy+1);
					else
						tdx = (char)(dy-1);

					if (!aicheck_this_dot(tdx, dx)) {
						p2_dy = tdx;
						p2_dx = dx;
						dots[tdx][dx] = P2N;
						return 0;
					}
				}
			}
		}
	}

	int dist, bst_dist;
	bst_dy = bst_dx = 0;
	bst_dist = FIELD_WIDTH * FIELD_HEIGHT;
	for (dx=1; dx<FIELD_WIDTH-1; dx++) {
		for (dy=1; dy<FIELD_HEIGHT-1; dy++) {
			if (dots[dy][dx] == EMPTY) {
				d1 = dots[dy-1][dx];
				d2 = dots[dy-1][dx+1];
				d3 = dots[dy][dx+1];
				d4 = dots[dy+1][dx+1];
				d5 = dots[dy+1][dx];
				d6 = dots[dy+1][dx-1];
				d7 = dots[dy][dx-1];
				d8 = dots[dy-1][dx-1];
				if (d1==EMPTY && d2==EMPTY && d3==EMPTY && 
				    d4==EMPTY && d5==EMPTY && d6==EMPTY && 
				    d7==EMPTY && d8==EMPTY) {
					if (dy > p2_dy)
						dist = dy - p2_dy;
					else
						dist = p2_dy - dy;
					if (dx > p2_dx)
						dist += dx - p2_dx;
					else
						dist += p2_dx - dx;
					if ((dist<bst_dist) && 
					    (!aicheck_this_dot(dy, dx))) {
						bst_dy = dy;
						bst_dx = dx;
						bst_dist = dist;
					}
				}
			}
		}
	}
	if (bst_dy) {
		p2_dy = bst_dy;
		p2_dx = bst_dx;
		dots[bst_dy][bst_dx] = P2N;
		return 0;
	}

	return 1;
}
//--------------------------------------------------------------- ai_deadzone --
void ai_empdead(struct dot *attacking, struct dot *victim, int *emp, int *dead)
{
	char dy, dx, rep, ch, _norm, _cntd, _de4d, _resc;
	int _dead, _emp;

	_norm = attacking->normal;
	_cntd = attacking->connected;

	do {
		rep = 0;
                for (dx=1; dx<FIELD_WIDTH-1; dx++) {
			for (dy=1; dy<FIELD_HEIGHT-1; dy++) {
                        	ch = _dots[dy][dx];
				if (ch!=M && ch!=MD) {
					if (_dots[dy-1][dx] == M) {
                                        	rep = 1;
                                		if (ch==_norm || ch==_cntd)
                					_dots[dy][dx] = MD;
                				else
                					_dots[dy][dx] = M;
                                        } else if (_dots[dy][dx-1] == M) {
                                        	rep = 1;
						if (ch==_norm || ch==_cntd)
                					_dots[dy][dx] = MD;
                				else
                					_dots[dy][dx] = M;
                                        }
				}
			}
		}
		for (dx=FIELD_WIDTH-2; dx>0; dx--) {
			for (dy=FIELD_HEIGHT-2; dy>0; dy--) {
                        	ch = _dots[dy][dx];
				if (ch!=M && ch!=MD) {
					if (_dots[dy+1][dx] == M) {
                                        	rep = 1;
						if (ch==_norm || ch==_cntd)
                					_dots[dy][dx] = MD;
                				else
                					_dots[dy][dx] = M;
                                        } else if (_dots[dy][dx+1] == M) {
                                        	rep = 1;
						if (ch==_norm || ch==_cntd)
                					_dots[dy][dx] = MD;
                				else
                					_dots[dy][dx] = M;
                                        }
				}
			}
		}
	} while (rep);

	_dead = _emp = 0;
        _norm = victim->normal;
        _de4d = attacking->dead;
	_resc = victim->rescued;
        for (dy=2; dy<FIELD_HEIGHT-2; dy++) {
        	for (dx=2; dx<FIELD_WIDTH-2; dx++) {
                	ch = _dots[dy][dx];
                        if (ch==_norm || ch==_de4d || ch==_resc)
                		_dead++;
                        else if (ch==EMPTY)
                        	_emp++;
                }
        }
        *emp = _emp;
	*dead = _dead;
}

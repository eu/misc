#define DOTS_COPYRIGHT	"Copyright (C) 2006 CybraSoft"
#define DOTS_VER	"Version 1.0"
#define DOTS_TEXT	"THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT WITHOUT ANY WARRANTY.\nTHE AUTHOR WILL NOT BE LIABLE FOR DATA LOSS, DAMAGES, LOSS OF PROFITS OR ANY OTHER KIND OF LOSS WHILE USING THIS SOFTWARE."
#define DOTS_WWW	"http:\x2F\x2Fdots.netfast.org"
#define DOTS_EMAIL	"e-mail: csdots@gala.net"

//------------------------------------------------------------------ Captions --
#define DOTS_CAPTION		"CS Dots."
#define DOTS_CAPTION_AI 	"CS Dots. Ai Game. YOUR DOT"
#define DOTS_CAPTION_AI_WAIT 	"CS Dots. Ai Game. THINKING ..."
#define DOTS_CAPTION_NET 	"CS Dots. Network Game. YOUR DOT"
#define DOTS_CAPTION_NET_WAIT 	"CS Dots. Network Game. PLEASE WAIT ..."
#define DOTS_CAPTION_NET_FOUND 	"CS Dots. Network Game: Host (%s) found. PLEASE WAIT ..."
#define DOTS_CAPTION_NET_TRY 	"CS Dots. Network Game: Trying to find host (%s) ..."
//---------------------------------------------------------------------- Main --
#define GE "Error"

#define MSG01 "Do you wish to restart the game ?"
#define CPT01 "Restart"
#define MSG02 "Do you wish to (re)start Ai game ?"
#define CPT02 "Ai Game"
#define MSG03 "Do you wish to quit Dots ?"
#define CPT03 "Quit"
#define MSG04 "Network Game not started."
#define MSG06 "Network game still running.\nDo you really want to quit Dots ?"
#define CPT06 CPT03
#define MSG07 "Remote host busy.\nTry later."

// #01: aidynsa_one
// #02: aidynsa_except
// #03: couldn't find a usable WinSock DLL
#define ERROR_MSG00 "Memory allocation failure!"
#define ERROR_MSG01 "#01"
#define ERROR_MSG02 "#02"
#define ERROR_MSG03 "Initialize WinSock failed!"
#define ERROR_MSG04 "Wrong WinSock version!"
//-------------------------------------------------------------- Network Game --
#define NG	"Network Game"
#define NGS 	"Network Game Started"
#define NGE	"Network Game Error"

#define NET_MSG01 "\nHost: %s want to play with you.\n\nAccept ?\n"
#define NET_MSG02 "User: %s\nHost: %s\n"
#define NET_MSG03 "Remote host restart game."
#define NET_MSG04 "Message from %s"
#define NET_MSG05 "Network game already started!"
#define NET_MSG06 "Connection closed."
#define NET_MSG07 "Remote host close connection.\nBye."
#define NET_MSG08 "Dot out of limited field!\nPlease register program!\n"

#define NETERROR_MSG01	"Could not create socket"
#define NETERROR_MSG02	"WSAAsyncSelect() error"
#define NETERROR_MSG03	"bind() error" // scan ports from 1024 to 1124
#define NETERROR_MSG04	"listen() error"
#define NETERROR_MSG05	"gethostname() error"
#define NETERROR_MSG06	"gethostbyname() == NULL"
#define NETERROR_MSG07	"accept() error"
#define NETERROR_MSG08	"recv() error"
#define NETERROR_MSG09	"Attempt to connect timed out without establishing a connection."
#define NETERROR_MSG10	"Can't connect."
#define NETERROR_MSG11	"Fatal error!"
#define NETERROR_MSG12	"Please check remote host/ip."
#define NETERROR_MSG13	"Bad data!"
//------------------------------------------------------------------------------
#define CD_GROUPBOX1	210
#define CD_RED1		211
#define CD_BLUE1	212
#define CD_BLACK1	213
#define CD_GROUPBOX2	220
#define CD_RED2		221
#define CD_BLUE2	222
#define CD_BLACK2	223
#define CD_CANCEL	201
#define CD_OK		202


#define MENU_GAME_RESTART	1105
#define MENU_GAME_SCORES	1104
#define MENU_GAME_AI		1103
#define MENU_GAME_NET		1102
#define MENU_GAME_QUIT		1101

#define MENU_SETUP_SAVE		1211
#define AI_MODEA		1210
#define AI_MODEB		1209
#define AI_ANALYZE		1208
#define MENU_GRID_LIGHT		1207
#define MENU_GRID_NORMAL	1206
#define MENU_GRID_DARK		1205
#define MENU_DS_BIG		1204
#define MENU_DS_NORMAL		1203
#define MENU_DS_SMALL		1202
#define MENU_OPTIONS_COLORS	1201

#define MENU_INFO_MSG		1303
#define MENU_INFO_ABOUT		1302
#define MENU_INFO_RULES		1301

#define MENU_SCORES		1400
#define MENU_SETUP_RESET	1500

#define ABOUT_STATICFRAME1	112
#define ABOUT_STATICFRAME2	111
#define ABOUT_STATICFRAME3	110
#define ABOUT_PIC1		109
#define ABOUT_STATICTEXT1	108
#define ABOUT_STATICTEXT2	107
#define ABOUT_STATICTEXT3	106
#define ABOUT_STATICTEXT4	105
#define ABOUT_STATICTEXT5	104
#define ABOUT_CLOSE		101

#define SCR_STATICTEXT1		302
#define SCR_STATICTEXT2		303
#define SCR_STATICTEXT3		304
#define SCR_STATICTEXT4		305
#define SCR_STATICFRAME1	306
#define SCR_PIC1		301
#define SCR_OK			307

#define RULES_CLOSE		401
#define RULES_STATICFRAME1	402
#define RULES_STATICTEXT1	403
#define RULES_STATICTEXT2	404
#define RULES_STATICTEXT3	405
#define RULES_STATICICON1	406
#define RULES_STATICICON2	407
#define RULES_STATICICON3	408

#define NET_STATICTEXT6			516
#define NET_STATICTEXT5			515
#define NET_GROUPBOX2			514
#define NET_STATICTEXT4			513
#define NET_STATICTEXT3			512
#define NET_STATICTEXT2			511
#define NET_STATICTEXT1			510
#define NET_EDIT6			509
#define NET_EDIT5			508
#define NET_EDIT4			507
#define NET_EDIT3			506
#define NET_EDIT2			505
#define NET_EDIT1			504
#define NET_GROUPBOX1			503
#define NET_CLOSE			502
#define NET_CTRL			501

#define NET_GROUPBOX	603
#define MSG_EDIT	602
#define MSG_SEND	601
#define MSG_CLOSE	600

#define INIT_STATICBITMAP	703
#define INIT_STATICTEXT1	702
#define INIT_STATICTEXT2	701
#define INIT_STATICFRAME1	700

/*
 * Copyright (c) 2006 Eugene P. <pieu@mail.ru>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

void fp_redraw_field(HDC);
#ifdef DBG_RBP2
int fp_add_dot(int mx, int my, char pxn);
#else
int fp_add_dot(int mx, int my);
#endif
void fp_redraw_dots(HDC, struct dot*);
char fp_check_walls(struct dot*, struct dot*);
void fp_redraw_walls(HDC, struct dot*);
void fp_change_scores(HWND);
void fp_blink_dot(HDC);

//-------------------------------------------------------------- redraw_field --
void fp_redraw_field(HDC hdc)
{
	HDC _hdc;
	HMETAFILE hmf;
	HPEN _pen;

	_hdc = CreateMetaFile(NULL);
	_pen = CreatePen(PS_SOLID, 1, grid_rgb);
	SelectObject(_hdc, _pen);

	int field_width = grid_size * (FIELD_WIDTH-1);
	int field_height = grid_size * (FIELD_HEIGHT-1);

	register int i;
        for (i=0; i<=field_width; i+=grid_size) {
        	MoveToEx(_hdc, i, 0, NULL);
                LineTo(_hdc, i, field_height);
        }

        for (i=0; i<=field_height; i+=grid_size) {
        	MoveToEx(_hdc, 0, i, NULL);
                LineTo(_hdc, field_width, i);
        }
	MoveToEx(_hdc, 1, 1, NULL);
        LineTo(_hdc, field_width-1, 1);
	LineTo(_hdc, field_width-1, field_height-1);
	LineTo(_hdc, 1, field_height-1);
	LineTo(_hdc, 1, 1);
	MoveToEx(_hdc, 3, 3, NULL);
        LineTo(_hdc, field_width-3, 3);
	LineTo(_hdc, field_width-3, field_height-3);
	LineTo(_hdc, 3, field_height-3);
	LineTo(_hdc, 3, 3);
	hmf = CloseMetaFile(_hdc);
	DeleteObject(_pen);
	PlayMetaFile(hdc, hmf);
	DeleteMetaFile(hmf);
}
//------------------------------------------------------------------- add_dot --
#ifdef DBG_RBP2
int fp_add_dot(int mx, int my, char pxn)
#else
int fp_add_dot(int mx, int my)
#endif
{
 	char dx, dy;

	dx = (char)(mx/grid_size);
	if (mx%grid_size > grid_size/2)
		dx++;

	dy = (char)(my/grid_size);
	if (my%grid_size > grid_size/2)
		dy++;

	if (!dx || !dy)
		return 1;

	if (dx>FIELD_WIDTH-2 || dy>FIELD_HEIGHT-2)
		return 1;

	if (!dots[dy][dx]) {
#ifdef DBG_RBP2
		dots[dy][dx] = pxn;
#else
		dots[dy][dx] = P1N;
#endif
		p1_dx = dx;
        	p1_dy = dy;
	} else
		return 2;

	return 0;
}
//--------------------------------------------------------------- redraw_dots --
void fp_redraw_dots(HDC hdc, struct dot *this_player)
{
	char dx, dy, dt, _norm, _cntd, _rescued, _dead, _closed;
	HPEN old_pen, new_pen;

        _norm = this_player->normal;
        _cntd = this_player->connected;
        _rescued = this_player->rescued;
        _dead = this_player->dead;
	_closed = this_player->closed;

        new_pen = CreatePen(PS_DOT, dot_size, this_player->color_rgb);
        old_pen = SelectObject(hdc, new_pen);

        for (dy=1; dy<FIELD_HEIGHT-1; dy++) {
        	for (dx=1; dx<FIELD_WIDTH-1; dx++) {
                	dt = dots[dy][dx];
                        if (dt==_norm || dt==_cntd ||
			    dt==_rescued || dt==_closed) {
                		MoveToEx(hdc, dx*grid_size, dy*grid_size, NULL);
        			LineTo(hdc, dx*grid_size, dy*grid_size);
                        }
                }

        }

        SelectObject(hdc, old_pen);
        DeleteObject(new_pen);
	new_pen = CreatePen(PS_SOLID, dot_size, this_player->color_rgb&0x808080);
        SelectObject(hdc, new_pen);

        for (dy=1; dy<FIELD_HEIGHT-1; dy++) {
        	for (dx=1; dx<FIELD_WIDTH-1; dx++) {
                	dt = dots[dy][dx];
                        if (dt == _dead) {
                		MoveToEx(hdc, dx*grid_size, dy*grid_size, NULL);
        			LineTo(hdc, dx*grid_size, dy*grid_size);
                        }
                }

        }

        SelectObject(hdc, old_pen);
        DeleteObject(new_pen);

}
//--------------------------------------------------------------- check_walls --
char fp_check_walls(struct dot *attacking, struct dot *victim)
{
        return 0;
}
//-------------------------------------------------------------- redraw_walls --
void fp_redraw_walls(HDC hdc, struct dot *this_player)
{
	char dx, dy, ch;
        int x, y;
	HDC _hdc;
	HMETAFILE hmf;
	HPEN _pen;

	_hdc = CreateMetaFile(NULL);
	_pen = CreatePen(PS_SOLID, wall_thickness, this_player->color_rgb);
	SelectObject(_hdc, _pen);

	for (dy=1; dy<FIELD_HEIGHT-1; dy++) {
		for (dx=1; dx<FIELD_WIDTH-1; dx++) {
			if (dots[dy][dx]==this_player->connected ||
			    dots[dy][dx]==this_player->closed) {
                            	x = dx * grid_size;
        			y = dy * grid_size;
				ch = xdots[dy][dx];
				if (ch & 0x1) {
					MoveToEx(_hdc, x, y, NULL);
					LineTo(_hdc, x+grid_size, y-grid_size);
				}
				if (ch & 0x2) {
					MoveToEx(_hdc, x, y, NULL);
					LineTo(_hdc, x+grid_size, y);
				}
				if (ch & 0x4) {
					MoveToEx(_hdc, x, y, NULL);
					LineTo(_hdc, x+grid_size, y+grid_size);
				}
				if (ch & 0x8) {
					MoveToEx(_hdc, x, y, NULL);
					LineTo(_hdc, x, y+grid_size);
				}
			}
		}
	}

	hmf = CloseMetaFile(_hdc);
	DeleteObject(_pen);
	PlayMetaFile(hdc, hmf);
	DeleteMetaFile(hmf);
}
//-------------------------------------------------------------- change_score --
void fp_change_scores(HWND hwnd)
{
	HMENU hmenu;
        char dx, dy, ch, scr[16];
        player1.scr = player2.scr = 0;
        for (dy=2; dy<FIELD_HEIGHT-2; dy++) {
        	for (dx=2; dx<FIELD_WIDTH-2; dx++) {
                	ch = dots[dy][dx];
                        if (ch == P2_DEAD)
                        	player1.scr++;
                        else if (ch == P1_DEAD)
                        	player2.scr++;
                }
        }

	wsprintf(scr, "%i : %i", player1.scr, player2.scr);
	hmenu = GetMenu(hwnd);	

	MENUITEMINFO mii;
	mii.cbSize = sizeof(MENUITEMINFO);
	mii.fMask = MIIM_TYPE;
	mii.fType = MFT_STRING | MFT_RIGHTJUSTIFY;
	mii.wID = MENU_SCORES;
	mii.dwTypeData = (LPSTR) &scr;
	mii.cch = sizeof(scr);        
        SetMenuItemInfo(hmenu, (UINT)MENU_SCORES, FALSE, &mii);
        DrawMenuBar(hwnd);
}
//----------------------------------------------------------------- blink_dot --
void fp_blink_dot(HDC hdc)
{
	HPEN old_pen, new_pen;

        new_pen = CreatePen(PS_DOT, dot_size, 0xFFFFFF);
        old_pen = SelectObject(hdc, new_pen);
	MoveToEx(hdc, p2_dx*grid_size, p2_dy*grid_size, NULL);
	LineTo(hdc, p2_dx*grid_size, p2_dy*grid_size);
        SelectObject(hdc, old_pen);
        DeleteObject(new_pen);
	Sleep(50);
	new_pen = CreatePen(PS_SOLID, dot_size, player2.color_rgb);
        SelectObject(hdc, new_pen);
	MoveToEx(hdc, p2_dx*grid_size, p2_dy*grid_size, NULL);
	LineTo(hdc, p2_dx*grid_size, p2_dy*grid_size);
        SelectObject(hdc, old_pen);
        DeleteObject(new_pen);
	Sleep(50);
	new_pen = CreatePen(PS_SOLID, dot_size, 0xFFFFFF);
        SelectObject(hdc, new_pen);
	MoveToEx(hdc, p2_dx*grid_size, p2_dy*grid_size, NULL);
	LineTo(hdc, p2_dx*grid_size, p2_dy*grid_size);
        SelectObject(hdc, old_pen);
        DeleteObject(new_pen);
	Sleep(50);
	new_pen = CreatePen(PS_SOLID, dot_size, player2.color_rgb);
        SelectObject(hdc, new_pen);
	MoveToEx(hdc, p2_dx*grid_size, p2_dy*grid_size, NULL);
	LineTo(hdc, p2_dx*grid_size, p2_dy*grid_size);
        SelectObject(hdc, old_pen);
        DeleteObject(new_pen);
}

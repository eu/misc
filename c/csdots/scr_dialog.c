/*
 * Copyright (c) 2006 Eugene P. <pieu@mail.ru>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

BOOL CALLBACK scr_dialog_proc(HWND hdwnd, UINT message, WPARAM wparam,
				LPARAM lparam)
{
	switch (message) {
		case WM_COMMAND:
			switch (LOWORD(wparam)) {
				case SCR_OK:
					EndDialog(hdwnd, 1);
					return 1;
			}
			break;

		case WM_INITDIALOG:
                	char text[32];
                	wsprintf(text, "Player 1 - (%s)", player1.color);
                	SetDlgItemText(hdwnd, SCR_STATICTEXT1, text);
                	wsprintf(text, "%i", player1.scr);
                	SetDlgItemText(hdwnd, SCR_STATICTEXT2, text);
                	wsprintf(text, "Player 2 - (%s)", player2.color);
                	SetDlgItemText(hdwnd, SCR_STATICTEXT3, text);
                	wsprintf(text, "%i", player2.scr);
                	SetDlgItemText(hdwnd, SCR_STATICTEXT4, text);
			SetFocus(hdwnd);
			break;

                case WM_CLOSE:
                	EndDialog(hdwnd, 0);
	}
	return 0;
}

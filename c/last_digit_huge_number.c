/*
 * Last digit of a huge number
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int last_digit(const unsigned long int *arr, size_t arr_size)
{
	const char last_digit[10][4] = {
		{0}, {1}, {2, 4, 8, 6}, {3, 9, 7, 1}, {4, 6},
		{5}, {6}, {7, 9, 3, 1}, {8, 4, 2, 6}, {9, 1}
	};
	const char num_digits[10] = {1, 1, 4, 4, 2, 1, 1, 4, 4, 2};
	const char magic_code[10][2] = {
		{0, 0}, {0, 0}, {3, 3}, {0, 2}, {3, 3},
		{0, 0}, {3, 3}, {0, 2}, {3, 3}, {0, 0} 
	};
	unsigned long int *a = (unsigned long int *) malloc(arr_size * sizeof(long int));
	if (!a)
		return -1;
	memcpy(a, arr, arr_size * sizeof(long int));

	size_t t=arr_size;
	for(int i=arr_size-1; (int) i>=0; i--) {
		if (i<arr_size-1 && !a[i+1])
			a[i] = 1;
		if (a[i] == 1)
			t = i;
	}
	arr_size = t;
	if (!arr_size) {
		free(a);
		return 1;
	}
	// *arr = [N, D, E, ?0 ... garbage];

	unsigned long int e, d, n = a[0] % 10;
	unsigned int s = num_digits[n];

	if (arr_size > 2 && !a[2]) {
		free(a);
		return n;
	}
	if (arr_size==1 || a[0]==1) {
		free(a);
		return n;
	}
	if (arr_size == 2) {
		d = a[1];
		free(a);
		if (!d) 
			return 1;
		d = (d > s) ? (d % s) : d;
		if (!d)
			d = s;
		return last_digit[n][d-1];
	}
	d = (a[1] - 8 * ((a[1]-10) / 8 + 1));
	e = a[2] % 2;
	free(a);
	return last_digit[n][ magic_code[d][e] % s ];

}

/*
 * Valid Parentheses
 */

#include <stdbool.h>

bool validParentheses(const char* strin)
{
	int ob, cb, b, len;
	len = strlen(strin);
	ob = cb = b = 0;
	for (register int i=0; i<len; i++) {
		if (strin[i] == '(') {
			ob++;
			b++;
		} else if (strin[i] == ')') {
			cb++;
			if (b)
				b--;
		}
	}
	return !b && ob == cb;
}

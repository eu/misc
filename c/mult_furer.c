/*
 * Multiplying numbers as strings
 * Fürer's algorithm
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define MAX(a, b) (((a) > (b)) ? (a) : (b))

#define ERR_MEM "Memory Allocation Failed!"
#define ERR_STR "Invalid character in string!"

char *multiply(char *p1, char *p2)
{
	int l1 = strlen(p1);
	int l2 = strlen(p2);
	int n = MAX(l1, l2);

	char *a   = (char *) calloc(n, sizeof(char));
	char *b   = (char *) calloc(n, sizeof(char));
	char *m   = (char *) calloc(n * n, sizeof(char));
	char *res = (char *) malloc(n * 2 + 1 * sizeof(char));

	if (!a || !b || !m || !res)
		return ERR_MEM;

	register int i, j;
	for (i=0; i<l1; i++) {
		if (!isdigit(p1[i]))
			return ERR_STR;
		a[i+n-l1] = p1[i] - '0';
	}
	for (i=0; i<l2; i++) {
		if (!isdigit(p2[i]))
			return ERR_STR;
		b[i+n-l2] = p2[i] - '0';
	}

	char *p = m;
	for (j=0; j<n; j++) {
		for (i=0; i<n; i++)
			*(p++) = a[j] * b[i];
	}
	free(a);
	free(b);

	unsigned long int o=0;
	int s;
	p = res + n*2;
	for (j=n*2-2; j>=0; j--) {
		unsigned long int sum = o;
		register int k;
		for (i=n-1; i>=0; i--) {
			k = j-i;
			if (k>=0 && k<n && i+k == j)
				sum += *(m + n*k + i);
		}
		s = sum % 10;
		*(--p) = '0' + s;
		o = (sum - s) / 10;
	}

	while (o) {
		s = o % 10;
		*(--p) = '0' + s;
		o = (o - s) / 10;
	}
	free(m);

	for (i=n*2 - (p-res); i>1; i--) {
		if (*p != '0')
			break;
		p++;
	}

	s = (res + n*2) - p;
	char *result = (char *) malloc(s+1 * sizeof(char));
	if (!result)
		return ERR_MEM;
	strncpy(result, p, s);
	*(result + s) = 0;
	free(res);

	return result;
}

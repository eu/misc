/*
 * Data Reverse
 */

#include <stddef.h>

/* Do not allocate any memory but just use rdata and return it back */

unsigned char *data_reverse(unsigned char *rdata, const unsigned char *data, size_t nblk)
{
	register int i, j;
	for (j=0; j<nblk; j++) {
		for(i=0; i<8; i++) 
			rdata[8*(nblk-j-1)+i] = data[8*j+i];
	}
	return rdata;
}

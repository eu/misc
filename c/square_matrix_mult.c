/*
 * Square Matrix Multiplication
 */

#include <stdio.h>
#include <stdlib.h>

int **matrix_multiplication(int **a, int **b, int n) {
	register i, r;
	int j, res;
	int **c = (char *) malloc(n * sizeof(int *));
	for(i=0; i<n; i++)
		*(c+i) = malloc(n * sizeof(int));

	for (j=0; j<n; j++) {
		for (i=0; i<n; i++) {
			for (r=0, res=0; r<n; r++)
				res += a[j][r] * b[r][i]; 
			c[j][i] = res;
		}
	}
	return c;
}

/*
 * Next smaller number with the same digits
 */

#include <stdio.h>
#include <string.h>

#define SIZE 21

void swap(char *a, char *b)
{
	char swap = *a;
	*a = *b;
	*b = swap;
}

int comp(const char *a, const char *b)
{
	return *b - *a;
}

unsigned long long next_smaller_number(unsigned long long n)
{
	char a[SIZE];
	sprintf(a, "%llu", n);
	size_t len = strlen(a);

	register int i=len-2, j;
	while (i>=0 && a[i] <= a[i+1])
		i--;

	if (i>=0) {
		j = i+1;
		char max=len;
		for (j=i; j<len; j++) {
			if (a[i] > a[j] && a[j] > a[max])
				max = j;
		}
		swap(&a[i], &a[max]);
		if (a[0] == '0')
			return -1;
		qsort(&a[i+1], len-i-1, sizeof(char), 
				(int(*)(const void *, const void *)) comp);

		unsigned long long res=0;
		for(i=0; i<len; i++) 
			res = res * 10 + a[i]-'0';
		return res;
	}
	return -1;
}

#include <stdio.h>

char a[4][7]	= {"Common", "Point", "Boost", "Better"};
char (*b[4])[7] = {a+3, a+1, a, a+2};

char (*(*c(void))[4])[7]
{
	return &b;
}

char (*(*d(void)))[7]
{
      return c()[1]-3;
}

void test(char (*(*f(void)))[7])
{

	printf("Point == %s\n", *f()[0]);
	printf("er == %s\n", *((**f)()-1)[0]+4/* ? */); //Bett|er$
	printf("mon == %s\n", (*f())[0/* ? */]-4); // Com|mon$
	printf("st = %s\n", f()[1][2]+3);
	printf("er == %s\n", *((**f)()-1)[0]+4/* ? */); //Bett|er$
}

int main()
{
	test(d);
}

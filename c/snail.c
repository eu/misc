#include <stdio.h>
#include <stdlib.h>

#define D 3
#define L 12
#define U 48
#define R 192

int main()
{
	char a[4][4] = {
		{ 1, 2, 3, 4 },
		{ 2, 3, 4, 5 },
		{ 1, 6, 5, 6 },
		{ 0, 9, 8, 7 }
	};
	char size = 4;
	int *res = (int *) calloc(size * size, sizeof(int));
	if (!res)
		return -1;
	int p;
	for (p=0; p<size; p++)
		*(res+p) += a[0][p];

	unsigned char d=D;
	int i=size-1, j=0, s = 0;
	while (p < size*size) {

		switch (d) {
			case D: j++; break;
			case U: j--; break;
			case R: i++; break;
			case L: i--; break;
		}

		*(res+p++) = a[j][i];
		printf("[%d][%d]\n", j, i);
		if ((d == D && j == size-s-1) || (d == U && !(j-s)) ||
		    (d == R && i == size-s-1) || (d == L && !(i-s))) {
			d = (d << 2) | (d >> (8-2));
			if (d == U)
				s++;
		}
	}

for (i=0; i<size*size; i++) {
	printf("%d ", *(res + i));
}
printf("\n");
}

/*
 * Permutation generator
 *
 * f(3, 2);
 *
 * xxO
 * xOx
 * Oxx
 */

#include <stdlib.h>
#include <stdio.h>

int factorial(int n)
{
	if (n<0)
		return 0;
	return (!n) ? 1 : n * factorial(n-1);
}

int f(int m, int n)
{
	unsigned long int total = 0;

	int *p  = (int *) malloc(n * sizeof(int));
	int *pl = (int *) malloc(n * sizeof(int));
	if (!p || !pl)
		return 1;

	// init
	register int i, j;
	for (i = 0; i < n; i++) {
		p[i] = i;
		pl[i] = m - (n - i);
	}

	while (1) {
		total++;
		for (j = 0; j < m; j++) {
			char ch = 'O';
			for (i = 0; i < n; i++)
				if (p[i] == j)
					ch = 'x';
			printf("%c", ch);
		}
		printf("\n");

		if (p[0] == pl[0])
			break;

		for (j = n - 1; j >= 0; j--) {
			if (p[j] != pl[j]) {
				int k = p[j] + 1;
				for (i = j; i < n; i++)
					p[i] = i - j + k;

				break;
			}
		}
	}

	free(pl);
	free(p);

	printf("Total: %lu (%lu)\n", total, factorial(m) / (factorial(m - n) * factorial(n)));
	return 0;
}

int main() {
	return f(3, 2);
}


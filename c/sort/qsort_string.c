#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

static int cmp(const void *p1, const void *p2)
{
	return (*(char *) p1 - *(char *) p2);
}

int main()
{
	char str[41];
	printf("Input string (max 40 chars): ");
	//scanf("%40[0-9 A-Za-z]", str);
	scanf("%40[^\n]", str);

	unsigned char i, j, offset = 0, len = strlen(str);
	for (i = 0; i < len; i++)
		str[i] = toupper(str[i]);

	for (j = 0; j < len; j++) {
		if (str[j] == ' ') {
			offset++;
			continue;
		}
		for (i = j + 1; i < len; i++)
			if (str[j] == str[i])
				str[i] = ' ';
	}

	printf("Unique chars: %s\n", str);
	qsort(str, len, sizeof(char), cmp);
	printf("Sorted string: %s\n", str + offset);
	return 0;
}

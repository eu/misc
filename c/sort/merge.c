#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#define MAX_LEN 100

#define MIN(a, b) (((a) < (b)) ? (a) : (b))

static int* merge_sort(int *src, int *dst, unsigned int sz)
{
	if (sz == 1) {
		*dst = *src;
		return dst;
	}

	int i;
	for (i = 0; i < sz-1; i++)
		if (src[i] > src[i+1])
			break;
	if (i == sz-1)
		return src;

	unsigned int middle = sz / 2;

	int *right = merge_sort(src + middle, dst + middle, sz - middle);
	int *left  = merge_sort(src, dst, middle);

	int *ptr = left == src ? dst : src;

	unsigned int n, l, r;
	for (n = l = r = 0; n < sz; n++) {
		if (l == middle) {
			if (ptr+n != right+r)
				memcpy(ptr+n, right+r, sizeof(int) * (sz - n));
			break;
		}

		if (r == sz-middle) {
			if (ptr+n != left+l)
				memcpy(ptr+n, left+l, sizeof(int) * (sz - n));
			break;
		}
		ptr[n] = left[l] < right[r] ? left[l++] : right[r++];
	}
	return ptr;
}

int main()
{
	int *a1 = (int *) malloc(MAX_LEN * sizeof(int));
	int *a2 = (int *) malloc(MAX_LEN * sizeof(int));

	if (!a1 || !a2)
		return 1;

	// Set random data
	srand(time(NULL));

	unsigned int i;
	for (i = 0; i < MAX_LEN; i++)
		a1[i] = rand() % 1000;

	for (i = 0; i < MAX_LEN; i++)
		printf("%d ", a1[i]);
	printf("\n---\n");

	int *ptr = merge_sort(a1, a2, MAX_LEN);

	for (i = 0; i < MAX_LEN; i++)
		printf("%d ", ptr[i]);
	printf("\n");

	free(a2);
	free(a1);
	return 0;
}

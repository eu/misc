#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SIZE 10

void main()
{
	int i, j;
	int array[SIZE];

	srand(time(0));
	printf("Неотсортированный массив: ");
	for (i=0; i<SIZE; i++) {
		array[i] = (rand() % (20 - 0)) + 0;
		printf("%d ", array[i]);
	}
	printf("\n");

	int min, tmp;
	for (i=0; i<SIZE; i++) 	{
		min = i;
		for (j=i; j<SIZE; j++) {
			if (array[j] < array[min])
				min = j;
		}
		if (min != i) {
			tmp = array[i];
			array[i] = array[min];
			array[min] = tmp;
		}
	}

	printf("Отсортированный массив:   ");
	for (i=0; i<SIZE; i++)
		printf("%d ", array[i]);
	printf("\n");
}

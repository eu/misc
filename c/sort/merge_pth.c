/*
 * Multithreaded version of merge sort
 * cc -pthread -O3 sort_pth.c -o sort_pth
 */

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define THREADS 4
#define MAX_LEN 100

#define MIN(a, b) (((a) < (b)) ? (a) : (b))

typedef struct {
	int id;
	int *src, *dst;
	unsigned int sz;
} tdata;

//pthread_mutex_t lock;

static int* merge_sort(int *src, int *dst, unsigned int sz)
{
	if (sz == 1) {
		*dst = *src;
		return dst;
	}

	int i;
	for (i = 0; i < sz-1; i++)
		if (src[i] > src[i+1])
			break;
	if (i == sz-1)
		return src;

	unsigned int middle = sz / 2;

	int *right = merge_sort(src + middle, dst + middle, sz - middle);
	int *left  = merge_sort(src, dst, middle);

	int *ptr = left == src ? dst : src;

	unsigned int n, l, r;
	for (n = l = r = 0; n < sz; n++) {
		if (l == middle) {
			if (ptr+n != right+r)
				memcpy(ptr+n, right+r, sizeof(int) * (sz - n));
			break;
		}

		if (r == sz-middle) {
			if (ptr+n != left+l)
				memcpy(ptr+n, left+l, sizeof(int) * (sz - n));
			break;
		}
		ptr[n] = left[l] < right[r] ? left[l++] : right[r++];
	}
	return ptr;
}

void *thread_func(void *thread_data)
{
	tdata *data = (tdata *) thread_data;
/*
	pthread_mutex_lock(&lock);
	printf("Thread %d\n", data->id);
	for (int i = 0; i < data->sz; i++)
		printf("%d ", data->src[i]);
	printf("\n");
	pthread_mutex_unlock(&lock);
*/
	int *ptr = merge_sort(data->src, data->dst, data->sz);
	if (ptr != data->dst)
		memcpy(data->dst, data->src, sizeof(int) * data->sz);

	return NULL;
}


int main()
{
	pthread_t *threads = (pthread_t *) malloc(THREADS * sizeof(pthread_t));
	tdata *data = (tdata *) malloc(THREADS * sizeof(tdata));
	int *a1 = (int *) malloc(MAX_LEN * sizeof(int));
	int *a2 = (int *) malloc(MAX_LEN * sizeof(int));

	if (!a1 || !a2 || !threads || !data)
		return 1;

	// Set random data
	srand(time(NULL));
	int i, len = MAX_LEN;
	for (i = 0; i < MAX_LEN; i++)
		a1[i] = rand() % 1000;

	for (i = 0; i < MAX_LEN; i++)
		printf("%d ", a1[i]);
	printf("\n");

/*
	if (pthread_mutex_init(&lock, NULL) != 0) {
		printf("Mutex init failed\n");
		return 1;
	}
*/
	// Divide array and start threads
	for (int j = MIN(THREADS, len); j > 0; j /= 2) {
		for (i = 0; i < j; i++) {
			data[i].id = i;
			data[i].src = a1 + len / j * i;
			data[i].dst = a2 + len / j * i;
			data[i].sz = len / j;

			if (i == j - 1) // 9 / 2 = 4 + 5
				data[i].sz += len % j;

			if (pthread_create(&(threads[i]), NULL, thread_func, &data[i]))
				printf("Can't create thread\n");
		}

		// Wait for sorting threads
		for (i = 0; i < j; i++)
			pthread_join(threads[i], NULL);
	}

	printf("\nResult: ");
	for (i = 0; i < MAX_LEN; i++)
		printf("%d ", a2[i]);
	printf("\n");

//	pthread_mutex_destroy(&lock);
	free(a2);
	free(a1);
	free(data);
	free(threads);

	return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SIZE 20

void main()
{
	int i;
	int array[SIZE];

	// Set
	srand(time(0));
	printf("Неотсортированный массив: ");
	for (i=0; i<SIZE; i++) {
		array[i] = (rand() % (20 - 0)) + 0;
		printf("%d ", array[i]);
	}
	printf("\n");

	// Shell
 	int j, tmp;
	int step = SIZE / 2;

 	while (step) {
		for (i=step; i<SIZE; i++) {
			tmp = array[i];

			for(j=i-step; j>=0 && array[j]>tmp; j-=step)
        			array[j+step] = array[j];

 			array[j+step] = tmp;
		}
		step /= 2;
	}

	// Out
	printf("Отсортированный массив:   ");
	for (i=0; i<SIZE; i++)
		printf("%d ", array[i]);
	printf("\n");
}

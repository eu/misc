#include <stdlib.h>
#include <stdio.h>

int f(int m, int n)
{
	int *a  = (int *) malloc(m * sizeof(int));
	int *p  = (int *) malloc(n * sizeof(int));
	int *pl = (int *) malloc(n * sizeof(int));
	if (!a || !p || !pl)
		return 1;

	register int i, j;
	int new_pos;
	for (i=0; i<n; i++) {
		p[i] = i;
		pl[i] = m - (n - i);
	}

	for (i=0; i<n; i++)
		printf("x");

	for (i=0; i<m-n; i++)
		printf("O");
	printf("\n");

	do {		
		for (j=n-1; j>=0; j--) {
			if (p[j] != pl[j]) {
				new_pos = p[j];
				new_pos++;
				for (i=j; i<n; i++)
					p[i] = new_pos + i - j;
				break;
			}
		}
		for (j=0; j<m; j++) {
			char ch;
			ch = 'O';
			for (i=0; i<n; i++) {
				if (p[i] == j)
					ch = 'x';
			}
			printf("%c", ch);
		}
		printf("\n");
	} while (p[0] != pl[0]);

	free(pl);
	free(p);
	free(a);
	return 0;
}

int main() {
	return f(8, 5);
}


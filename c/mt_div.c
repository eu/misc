/*
 *               (m+2)(m-1)                          n
 * n * (m - 1) - ---------- - n % all divisors; m = ---
 *                   2                               2
 */

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h> 

#define THREADS 2

typedef unsigned long long int ull;
typedef struct {
	ull first, last, n, result;
} tdata;

void *thread_func(void *thread_data)
{
	tdata *data = (tdata*) thread_data;
	ull from = data->first, to = data->last;
	ull n = data->n, result=0;

	for (ull i=from; i<=to; i++) {
		result += n % i;
	}

	data->result = result;
	pthread_exit(0);
}

ull csod(const ull n) {

	if (!n || n==1)
		return 0;
  
	pthread_t *threads = (pthread_t *) malloc(THREADS * sizeof(pthread_t));
	tdata *data = (tdata *) malloc(THREADS * sizeof(tdata));

	if (!threads || !data)
		return 0;

	ull result, m = n / 2;
	result = n * (m-1) - (m+2)*(m-1)/2;
	ull range = (m / THREADS)-1;
	ull r = 1;

	int id;
	for (id=0; id<THREADS; id++) {
		data[id].result = 0;
		data[id].n = n;
		r++;
		data[id].first = r;
		r += range;
		data[id].last = (id == THREADS-1) ? m : r;

		if (pthread_create(&(threads[id]), NULL, thread_func,&data[id]))
			printf("Can't create thread\n");

		printf("%9lld - %9lld\n", data[id].first, data[id].last);
	}

	for (id=0; id < THREADS; id++)
		pthread_join(threads[id], NULL);
		
	for (id=0; id < THREADS; id++)
		result -= data[id].result;

	return result;
}

#define MAX 100000000

int main()
{
	int c=0;
	for (ull i=MAX; i>MAX-2000; i--) {
		if (c++ > 100) { 
			c = 0; 
			printf("%9d of %d\n", i, MAX-2000);
		}
		printf("Result: %lld\n", csod(i));
	}

	return 0;
}

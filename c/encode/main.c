#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include "main.h"

char *progname;
char *decoded_file, *encoded_file, *square_file;

#ifdef GENERATE_SQUARE
extern int square_gen_normal();
extern int square_gen_reverse();
#endif

#ifdef POLYBIOUS
extern int pol_decode();
extern int pol_encode();
#endif

#ifdef FEISTEL
extern int fei_decode();
extern int fei_encode();
#endif

void usage() {
#ifdef FEISTEL
	fprintf(stderr, "Usage: %s -d decoded_file -e encoded_file\n", progname);
#else
	fprintf(stderr, "Usage: %s -s square_file -d decoded_file -e encoded_file\n", progname);
#endif
	exit(1);
}

int file_exists(char *file_name)
{	
	int file;
	file = open(file_name, O_RDONLY);
	if (file == -1)
		return 0;
	close(file);
	return 1;
}

int main(int argc, char **argv)
{
	progname = *argv;	
        	
#ifdef FEISTEL
	if (argc != 5)
		usage();
#else
	if (argc != 7)
		usage();
#endif

	int i;
	for (i=1; i<argc; i++) {

		char *arg = *(argv + i);
		if (*arg = '-') {
			i++;
			switch (*(arg + 1)) {
			case 'd':
				decoded_file = *(argv + i);
				break;			
			case 'e':
				encoded_file = *(argv + i);
				break;
#ifdef POLYBIUS
			case 's':
				square_file = *(argv + i);
				break;
#endif
			default:
				usage();
			}
		} else {
			usage();
		}
	}

#ifdef GENERATE_SQUARE
	if(!square_gen_reverse()) {
		fprintf(stderr, "%s: error. can't generate square.\n", progname);
		return 1;
	}
	return 0;
#endif		
        	
	if (file_exists(decoded_file)) {
		if (file_exists(encoded_file)) {
			fprintf(stderr, "%s: All files exists.\n", progname);
			return 1;
		}

		/***************/
		/* encode file */
		/***************/
		fprintf(stdout, "Encode %s > %s\n", decoded_file, encoded_file);
#ifdef POLYBIUS
		if(!square_create()) {
			fprintf(stderr, "%s: error. can't create square.\n", progname);
			return 1;
		}		
		if (!pol_encode()) {
			fprintf(stderr, "%s: error. please check files.\n", progname);
		}
		square_delete();
		return 0;
#endif
#ifdef FEISTEL
		if (!fei_encode()) {
			fprintf(stderr, "%s: error. please check files.\n", progname);
		}		
		return 0;
#endif
	}
	if (file_exists(encoded_file)) {

		/***************/
		/* decode file */
		/***************/
		fprintf(stdout, "Decode %s > %s\n", encoded_file, decoded_file);
#ifdef POLYBIUS
		if(!square_create()) {
			fprintf(stderr, "%s: error. can't create square.\n", progname);
			return 1;
		}	
		if (!pol_decode()) {
			fprintf(stderr, "%s: error. please check files.\n", progname);
		}
		square_delete();
		return 0;
#endif
#ifdef FEISTEL
		if (!fei_decode()) {
			fprintf(stderr, "%s: error. bad encode file.\n", progname);
			return 1;
		}
		return 0;
#endif	
	}
	fprintf(stderr, "%s: error. please check files.\n", progname);

	return 1;
}

#include <stdio.h>
#include <fcntl.h>

#include "main.h"

extern char *decoded_file, *encoded_file;

int fei_decode(void)
{
	int dst_file;
	int src_file;
#ifdef _WIN32	
	dst_file = open(decoded_file, O_WRONLY|O_CREAT|O_TRUNC|O_BINARY, 0644);
#else
	dst_file = open(decoded_file, O_WRONLY|O_CREAT|O_TRUNC, 0644);
#endif
	if (dst_file == -1)
		return 0;
#ifdef _WIN32
	src_file = open(encoded_file, O_RDONLY|O_BINARY);
#else
	src_file = open(encoded_file, O_RDONLY);
#endif
	if (src_file == -1) {
		close(dst_file);
		return 0;
	}

/*read 4x32 bits*/
	char i, b;
	unsigned int x[4], y[4];
	do {
		for (b=0; b<4; b++) {
			if (!read(src_file, &x[b], sizeof(unsigned int))) {
				close(dst_file);
				close(src_file);
				return 1;
			}
		}
		
		/* decode *x -> *y */
		for(i=ROUNDS-1; i>=0; i--) {
			y[0] = x[3];
			x[3] = x[3] ^ i; /* function XOR */
			y[3] = x[2] ^ x[3];
			y[2] = x[1] ^ x[3];
			y[1] = x[0] ^ x[3];			

			x[0] = y[0];
			x[1] = y[1];
			x[2] = y[2];
			x[3] = y[3];
		}

		write(dst_file, &y[0], sizeof(unsigned int));
		write(dst_file, &y[1], sizeof(unsigned int));
		write(dst_file, &y[2], sizeof(unsigned int));
		write(dst_file, &y[3], sizeof(unsigned int));
	} while (1);	
}

int fei_encode(void)
{
	int dst_file;
	int src_file;
#ifdef _WIN32	
	dst_file = open(encoded_file, O_WRONLY|O_CREAT|O_TRUNC|O_BINARY, 0644);	
#else
	dst_file = open(encoded_file, O_WRONLY|O_CREAT|O_TRUNC, 0644);	
#endif
	if (dst_file == -1)
		return 0;
#ifdef _WIN32
	src_file = open(decoded_file, O_RDONLY|O_BINARY);
#else
	src_file = open(decoded_file, O_RDONLY);
#endif
	if (src_file == -1) {
		close(dst_file);
		return 0;
	}

	/*read 4x32 bits*/
	char end = 0, i;
	unsigned int x[4], y[4];
	do {
		x[0] = x[1] = x[2] = x[3] = 0;
		if (!read(src_file, &x[0], sizeof(unsigned int))) {
			end = 1;
			break;
		}
		if (!read(src_file, &x[1], sizeof(unsigned int))) {
			end = 1;			
		}
		if (!read(src_file, &x[2], sizeof(unsigned int))) {
			end = 1;			
		}
		if (!read(src_file, &x[3], sizeof(unsigned int))) {
			end = 1;
		}
		
		/* encode *x -> *y */
		for(i=0; i<ROUNDS; i++) {
			y[3] = x[0];
			x[0] = x[0] ^ i; /* function XOR */
			y[0] = x[1] ^ x[0];
			y[1] = x[2] ^ x[0];
			y[2] = x[3] ^ x[0];			

			x[0] = y[0];
			x[1] = y[1];
			x[2] = y[2];
			x[3] = y[3];
		}

		write(dst_file, &y[0], sizeof(unsigned int));
		write(dst_file, &y[1], sizeof(unsigned int));
		write(dst_file, &y[2], sizeof(unsigned int));
		write(dst_file, &y[3], sizeof(unsigned int));
	} while (!end);
	
	close(dst_file);
	close(src_file);
	
	return 1;
}

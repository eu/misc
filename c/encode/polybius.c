#include <stdio.h>
#include <fcntl.h>

#include "main.h"

extern char *decoded_file, *encoded_file, *square_file;
extern unsigned char *square;

int pol_decode(void)
{
	int dst_file;
	int src_file;
#ifdef _WIN32	
	dst_file = open(decoded_file, O_WRONLY|O_CREAT|O_TRUNC|O_BINARY, 0644);
#else
	dst_file = open(decoded_file, O_WRONLY|O_CREAT|O_TRUNC, 0644);
#endif
	if (dst_file == -1)
		return 0;
#ifdef _WIN32
	src_file = open(encoded_file, O_RDONLY|O_BINARY);
#else
	src_file = open(encoded_file, O_RDONLY);
#endif
	if (src_file == -1) {
		close(dst_file);
		return 0;
	}
	
	unsigned char ch;
	int i;
	while (read(src_file, &ch, sizeof(char))) {
		i = *(square+ch);
		write(dst_file, &i, sizeof(unsigned char));
	}
	close(dst_file);
	close(src_file);
	
	return 1;
}

int pol_encode(void)
{
	int dst_file;
	int src_file;
#ifdef _WIN32	
	dst_file = open(encoded_file, O_WRONLY|O_CREAT|O_TRUNC|O_BINARY, 0644);	
#else
	dst_file = open(encoded_file, O_WRONLY|O_CREAT|O_TRUNC, 0644);	
#endif
	if (dst_file == -1)
		return 0;
#ifdef _WIN32
	src_file = open(decoded_file, O_RDONLY|O_BINARY);
#else
	src_file = open(decoded_file, O_RDONLY);
#endif

	if (src_file == -1) {
		close(dst_file);
		return 0;
	}
	
	unsigned char ch;
	int i;
	while (read(src_file, &ch, sizeof(char))) {
		for (i=0; i<SQUARE_SIZE; i++) {
			if (ch == *(square+i)) {
				write(dst_file, &i, sizeof(unsigned char));
				break;
			}
		}
	}
	close(dst_file);
	close(src_file);
	
	return 1;
}

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>

#include "main.h"

extern char *square_file;

unsigned char *square;

int square_create(void)
{
	
	if (!(square=(unsigned char *)malloc(sizeof(unsigned char) * SQUARE_SIZE))) {
		return 0;
	}

	int file;
	int i;
#ifdef _WIN32
	file = open(square_file, O_RDONLY|O_BINARY);
#else
	file = open(square_file, O_RDONLY);
#endif
	if (file == -1)
		return 0;

	for (i=0; i<SQUARE_SIZE; i++) {
		read(file, square+i, sizeof(char));
	}

	close(file);        
	return 1;
}

void square_delete(void)
{
	if (square)
		free(square);
}

int square_gen_normal(void)
{
	int file;
	int i;
#ifdef _WIN32
	file = open(square_file, O_WRONLY|O_CREAT|O_TRUNC|O_BINARY, 0644);
#else
	file = open(square_file, O_WRONLY|O_CREAT|O_TRUNC, 0644);	
#endif
	if (file == -1)
		return 0;

	for (i=0; i<SQUARE_SIZE; i++) {
		write(file, &i, sizeof(char));
	}

	close(file);
	return 1;
}

int square_gen_reverse(void)
{
	int file;
	int i;
#ifdef _WIN32
	file = open(square_file, O_WRONLY|O_CREAT|O_TRUNC|O_BINARY, 0644);
#else
	file = open(square_file, O_WRONLY|O_CREAT|O_TRUNC, 0644);
#endif
	if (file == -1)
		return 0;

	for (i=SQUARE_SIZE-1; i>=0; i--) {
		write(file, &i, sizeof(char));
	}

	close(file);
	return 1;
}

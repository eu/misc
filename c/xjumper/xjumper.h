#define SCAN_SIZE 10
#define MAX_JUMPS 100

struct jump {
	int x, y;
	unsigned long scan[SCAN_SIZE];
};


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <X11/Xlib.h>
#include <X11/cursorfont.h>

#include "xjumper.h"

extern char *progname;
extern Display *dpy;

extern char *jumps_file;
extern int steps;

struct jump *jmp;

int check_pointer(struct jump *);
void click(void);
int jumps_init(int);
void jumps_free(void);
int jumps_load(void);
int jumps_save(void);
int jump(struct jump *);
int jump_set(struct jump *);

int check_pointer(struct jump *jmp) {
	Window root_window;
	root_window = DefaultRootWindow(dpy);
	XWarpPointer(dpy, None, root_window, 0, 0, 0, 0, jmp->x, jmp->y);

	Window window, root;
	int x_root, y_root, x, y, mask;
	XQueryPointer(dpy, root_window, &root, &window, &x_root, &y_root,
		      &x, &y, &mask);
	XImage *xim;
	if (window) {
		xim = XGetImage(dpy, (Drawable) window, x, y, SCAN_SIZE, 100, 
				AllPlanes, ZPixmap);
		if (xim == NULL) {
			return 0;
		}
		int i;		
		for (i=0; i<SCAN_SIZE; i++) {
			unsigned long pixel;
			pixel = XGetPixel(xim, i, 0);
			if (jmp->scan[i] != pixel)
				return 0;
		}
	}
	XFree(xim);
	return 1;
}     

void click()
{
	XEvent event;
	bzero(&event, sizeof(XEvent));

	event.type = ButtonPress;
	event.xbutton.button = Button1;
	event.xbutton.same_screen = True;

	XQueryPointer(dpy, DefaultRootWindow(dpy),
		      &event.xbutton.root, &event.xbutton.window,
		      &event.xbutton.x_root, &event.xbutton.y_root,
		      &event.xbutton.x, &event.xbutton.y,
		      &event.xbutton.state);

	event.xbutton.subwindow = event.xbutton.window;

	while (event.xbutton.subwindow) {
		event.xbutton.window = event.xbutton.subwindow;
		XQueryPointer(dpy, event.xbutton.window,
			      &event.xbutton.root, &event.xbutton.subwindow,
			      &event.xbutton.x_root, &event.xbutton.y_root,
			      &event.xbutton.x, &event.xbutton.y,
			      &event.xbutton.state);
	}
	
	if (!XSendEvent(dpy, PointerWindow, True, 0xFFF, &event)) {
		fprintf(stderr, "%s: !XSendEvent();\n", progname);
	}
	usleep(100000);

	event.type = ButtonRelease;
	event.xbutton.state = 0x100;
	if (!XSendEvent(dpy, PointerWindow, True, 0xFFF, &event)) {
		fprintf(stderr, "%s: !XSendEvent();\n", progname);
	}
	usleep(100000);
	XSync(dpy, 0);
}

int jumps_init(int size) {
	if (!(jmp=(struct jump *)malloc(sizeof(struct jump) * size))) {
		fprintf(stderr, "%s: Allocation error.\n", progname);
		return 0;
	}
	return 1;
}

void jumps_free() {
	if (jmp)
		free(jmp);       
}

int jumps_load()
{
	int file;
	int index, i;
	
	file = open(jumps_file, O_RDONLY);
	if (file == -1)
		return 0;
	if (read(file, &steps, sizeof(int)) == -1) {
		close(file);
		return 0;
	}
	if (!jumps_init(steps)) {
		close(file);
		return 0;
	}
	for (index=0; index<steps; index++) {
		if (read(file, &(jmp+index)->x, sizeof(int)) == -1) {
			close(file);
			return 0;
		}
		if (read(file, &(jmp+index)->y, sizeof(int)) == -1) {
			close(file);
			return 0;
		}
		for (i=0; i<SCAN_SIZE; i++) {
			if (read(file, &(jmp+index)->scan[i], sizeof(unsigned long)) == -1) {
				close(file);
				return 0;
			}
		}
	}
	close(file);
	return 1;
}

int jumps_save(void)
{
	int file;
	int index, i;
	
	file = open(jumps_file, O_WRONLY|O_CREAT|O_TRUNC, 0644);
	if (file == -1)
		return 0;

	write(file, &steps, sizeof(int));
	for (index=0; index<steps; index++) {
		write(file, &(jmp+index)->x, sizeof(int));
		write(file, &(jmp+index)->y, sizeof(int));
		for (i=0; i<SCAN_SIZE; i++) {
			write(file, &(jmp+index)->scan[i], sizeof(unsigned long));
		}
	}

	close(file);
	return 1;
}

int jump(struct jump *jmp)
{
	if (check_pointer(&jmp[0])) {
		click();
		return 0;
	}
	return 1;
}

int jump_set(struct jump *jmp) {		
	Cursor cursor;
	cursor = XCreateFontCursor (dpy, XC_crosshair);
	if (cursor == None) {
		fprintf(stderr, "%s: unable to create selection cursor\n", progname);
		return 0;
	}

	Window root_window;
	root_window = DefaultRootWindow(dpy);
	if (XGrabPointer(dpy, root_window, False, ButtonReleaseMask, GrabModeSync, 
			  GrabModeAsync, None, cursor, CurrentTime) != GrabSuccess) {
		fprintf(stderr, "%s: unable to grab cursor\n", progname);
		return 0;
	}

	XEvent event;
	do {
		XAllowEvents(dpy, SyncPointer, CurrentTime);
		XWindowEvent(dpy, root_window, ButtonReleaseMask, &event);
	} while (event.type != ButtonRelease);

	XUngrabPointer(dpy, CurrentTime);
	XFreeCursor(dpy, cursor);
	XSync(dpy, 0);

	if (event.xbutton.button != 1) {
		return 0;
	}
	
	Window window, root;
	int x_root, y_root, x, y, mask;
	XQueryPointer(dpy, root_window, &root, &window, &x_root, &y_root, &x, &y, &mask);

	XImage *xim;
	if (window) {
		xim = XGetImage(dpy, (Drawable) window, x, y, SCAN_SIZE, 100, 
				AllPlanes, ZPixmap);
		if (xim == NULL) {
			return 0;
		}
		int i;
		for (i=0; i<SCAN_SIZE; i++) {
			jmp->scan[i] = (unsigned long) XGetPixel(xim, i, 0);
		}
		jmp->x = x;
		jmp->y = y;
	}
	XFree(xim);
	return 1;
}

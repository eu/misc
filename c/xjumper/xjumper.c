
#include <stdio.h>
#include <stdlib.h>
#include <X11/Xlib.h>

#include "xjumper.h"

char *progname;
Display *dpy;

char *jumps_file;
int steps;

static int click_pause, sleep_time, repeat;

extern struct jump *jmp;

int init_display(void);
void close_display(void);

extern int check_pointer(struct jump *);
extern int jumps_init(int);
extern void jumps_free(void);
extern int jumps_load(void);
extern int jumps_save(void);
extern int jump(struct jump *);
extern int jump_set(struct jump *);

void usage() {
	fprintf(stderr, "Usage: %s -f jumps_file -p click_pause(s) -s sleep_time(s) -r repeat\n",
		progname);
	exit(1);
}

void main(int argc, char **argv)
{
	progname = *argv;
	
	if (argc != 9)
		usage();
	int i;
	for (i=1; i<argc; i++) {

		char *arg = *(argv + i);
		if (*arg = '-') {
			i++;
			switch (*(arg + 1)) {
			case 'f':
				jumps_file = *(argv + i);
				break;
			case 'p':
				click_pause = atoi(*(argv + i));
				break;
			case 's':
				sleep_time = atoi(*(argv + i));
				break;
			case 'r':
				repeat = atoi(*(argv + i));
				break;
			default:
				usage();
			}
		} else {
			usage();
		}
	}	
	if (!click_pause || !sleep_time || !repeat) {
		fprintf(stderr, "%s: Check times!\n", progname);
		exit(1);
	}	
		
	if (!init_display())
		exit(1);

	jumps_init(MAX_JUMPS);
	steps = 0;

	fprintf(stdout, "%s: file:%s, repeat=%d\n", progname, jumps_file, repeat);

	int n;
	unsigned char key[256];
	*key = '!';		
	fprintf(stdout, "8 - show (m)enu\n");
	do {		
		fprintf(stdout, "> ");
		fscanf(stdin, "%s", key);
		switch (*key) {
		case '0':
			*key = 'q';
			break;
		case '1':
		case 'r':
			if (!steps) {
				fprintf(stdout, "No jumps!\n");
				break;
			}
			fprintf(stdout, "Run jumper:\n");
			for (n=0; n<repeat; n++) {
				fprintf(stdout, "%d of %d\n", n+1, repeat);
				for (i=0; i<steps; i++) {
					jump(&jmp[i]);
					sleep(click_pause);
				}
				if (n < repeat-1) 
					sleep(sleep_time);
			}
			break;
		case '2':
		case 'c':
			jumps_free();
			jumps_init(MAX_JUMPS);
			steps = 0;
			fprintf(stdout, "All jumps cleared.\n");
			break;
		case '3':
		case 'n':
			if (!steps) {
				fprintf(stdout, "No jumps!\n");
				break;
			}
			fprintf(stdout, "--- Set jump (n) ---\n");
			fprintf(stdout, "(max %d) enter n = ", steps-1);
			fscanf(stdin, "%s", key);
			n = atoi(key);
			if (n < steps)
				jump_set(&jmp[n]);
			else
				fprintf(stdout, "error! n > max\n");
			break;
		case '4':
		case 't':
			if (!steps) {
				fprintf(stdout, "No jumps!\n");
				break;
			}
			fprintf(stdout, "--- Test jump (n) ---\n");
			fprintf(stdout, "(max %d) enter n = ", steps-1);
			fscanf(stdin, "%s", key);
			n = atoi(key);
			if (n > steps) {
				fprintf(stdout, "error! n > max\n");
				break;
			}
			fprintf(stdout, "test ...");
			if (check_pointer(&jmp[n]))
				fprintf(stdout, "good.\n");
			else
				fprintf(stdout, "bad.\n");
			break;
		case '5':
		case 'a':
			fprintf(stdout, "Left mouse click - add jump.\nRight mouse click - end.\n");
			while (steps < MAX_JUMPS) {
				fprintf(stdout, "jump[%2d] ... ", steps);
				if (!jump_set(&jmp[steps]))
					break;
				fprintf(stdout, "ok.\n");
				jump(&jmp[steps]);
				steps++;
			}
			fprintf(stdout, "end.\n");
			break;
		case '6':
		case 's':
			if (!steps) {
				fprintf(stdout, "No jumps!\n");
				break;
			}
			if (!jumps_save()) {
				fprintf(stderr, "%s: Can't save jumps.\n", progname);
				close_display();
				jumps_free();
				exit(1);
			}
			fprintf(stdout, "%d jump(s) saved.\n", steps);
			break;
		case '7':
		case 'l':
			/* Load jumps*/
			if (!jumps_load()) {
				fprintf(stderr, "%s: Can't load jumps.\n", progname);
				close_display();
				jumps_free();
				exit(1);
			}
			fprintf(stdout, "%d jump(s) loaded.\n", steps);
			break;
		case '8':
		case 'p':
			fprintf(stdout, "--- %s menu: ---\n", progname);
			fprintf(stdout, "0 - quit\n");
			fprintf(stdout, "1 - (r)un xjumper\n");
			fprintf(stdout, "2 - (c)lear all jumps\n");
			fprintf(stdout, "3 - set jump (n)\n");
			fprintf(stdout, "4 - (t)est step n\n");
			fprintf(stdout, "5 - (a)dd jump\n");
			fprintf(stdout, "6 - (s)ave jumps\n");
			fprintf(stdout, "7 - (l)oad jumps\n");
			fprintf(stdout, "8 - (p)rint this menu\n");
			break;
		}		
	} while (*key != 'q');

	close_display();
	jumps_free();

	exit(0);
}

int init_display()
{
	char *display = NULL;
	dpy = XOpenDisplay(display);
	if (!dpy) {
		fprintf(stderr, "%s: unable to open display \"%s\"\n",
			progname, XDisplayName(display));
		return 0;
	}
	return 1;
}

void close_display()
{
	XCloseDisplay(dpy);
}

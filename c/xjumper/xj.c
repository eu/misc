
#include <stdio.h>
#include <stdlib.h>
#include <X11/Xlib.h>

#include "xjumper.h"

char *progname;
Display *dpy;

char *jumps_file;
int steps;

static int click_pause, sleep_time, repeat;

extern struct jump *jmp;

int init_display(void);
void close_display(void);

extern int check_pointer(struct jump *);
extern int jumps_init(int);
extern void jumps_free(void);
extern int jumps_load(void);
extern int jump(struct jump *);
extern int jump_set(struct jump *);

void usage() {
	fprintf(stderr, "Usage: %s -f jumps_file -p click_pause(s) -s sleep_time(s) -r repeat\n",
		progname);
	exit(1);
}

void main(int argc, char **argv)
{
	progname = *argv;
	
	if (argc != 9)
		usage();
	int i;
	for (i=1; i<argc; i++) {

		char *arg = *(argv + i);
		if (*arg = '-') {
			i++;
			switch (*(arg + 1)) {
			case 'f':
				jumps_file = *(argv + i);
				break;
			case 'p':
				click_pause = atoi(*(argv + i));
				break;
			case 's':
				sleep_time = atoi(*(argv + i));
				break;
			case 'r':
				repeat = atoi(*(argv + i));
				break;
			default:
				usage();
			}
		} else {
			usage();
		}
	}	
	if (!click_pause || !sleep_time || !repeat) {
		fprintf(stderr, "%s: Check times!\n", progname);
		exit(1);
	}	
		
	if (!init_display())
		exit(1);

	int n;
	if (jumps_load()) {
		if (steps) {
			fprintf(stdout, "Autorun %s: file:%s, repeat=%d\n",
				progname, jumps_file, repeat);
			for (n=0; n<repeat; n++) {
				fprintf(stdout, "%d of %d\n", n+1, repeat);
				for (i=0; i<steps; i++) {
					jump(&jmp[i]);
					sleep(click_pause);
				}
				if (n < repeat-1) 
					sleep(sleep_time);
			}
		} 
	} else {
		fprintf(stderr, "%s: Bad jumps file: %s\n",
			progname, jumps_file);
	}
	close_display();
	jumps_free();

	exit(0);
}

int init_display()
{
	char *display = NULL;
	dpy = XOpenDisplay(display);
	if (!dpy) {
		fprintf(stderr, "%s: unable to open display \"%s\"\n",
			progname, XDisplayName(display));
		return 0;
	}
	return 1;
}

void close_display()
{
	XCloseDisplay(dpy);
}

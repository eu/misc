#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define SIZE 7
#define DATA {1, 2, 4, 8, 16, 32, 64}
#define RES  {0, 0, 0, 0, 0, 0, 0}

int** SolvePuzzle (int *clues);

static int clues[][SIZE * 4] = {
  { 7, 0, 0, 0, 2, 2, 3,
    0, 0, 3, 0, 0, 0, 0,
    3, 0, 3, 0, 0, 5, 0,
    0, 0, 0, 0, 5, 0, 4 },
  { 0, 2, 3, 0, 2, 0, 0,
    5, 0, 4, 5, 0, 4, 0,
    0, 4, 2, 0, 0, 0, 6,
    5, 2, 2, 2, 2, 4, 1 },
  { 3, 3, 2, 1, 2, 2, 3,
    4, 3, 2, 4, 1, 4, 2,
    2, 4, 1, 4, 5, 3, 2,
    3, 1, 4, 2, 5, 2, 3}
};

static int expected[][SIZE][SIZE] = {
  { { 1, 5, 6, 7, 4, 3, 2 },
    { 2, 7, 4, 5, 3, 1, 6 },
    { 3, 4, 5, 6, 7, 2, 1 },
    { 4, 6, 3, 1, 2, 7, 5 },
    { 5, 3, 1, 2, 6, 4, 7 },
    { 6, 2, 7, 3, 1, 5, 4 },
    { 7, 1, 2, 4, 5, 6, 3 } },
  { { 7, 6, 2, 1, 5, 4, 3 },
    { 1, 3, 5, 4, 2, 7, 6 },
    { 6, 5, 4, 7, 3, 2, 1 },
    { 5, 1, 7, 6, 4, 3, 2 },
    { 4, 2, 1, 3, 7, 6, 5 },
    { 3, 7, 6, 2, 1, 5, 4 },
    { 2, 4, 3, 5, 6, 1, 7 } }
};

int check(int **solution, int (*expected)[SIZE])
{
  int result = 0;
  if (solution && expected) {
    result = 1;
    for (int i = 0; i < SIZE; i++)
      if (memcmp(solution[i], expected[i], SIZE * sizeof(int))) {
        result = 0;
        break;
      }
  }

  return result;
}

void bin(unsigned int n)
{
	for(int shift=SIZE-1; shift>=0; shift--) {
		if (n >> shift & 1)
			printf("1");
		else
			printf("0");
	}
}

int pop(unsigned);
inline int pop(register unsigned x)
{
	x -= ((x >> 1) & 0x55555555);
	x =  (x & 0x33333333) + ((x >> 2) & 0x33333333);
	x =  (x + (x >> 4)) & 0x0F0F0F0F;
	x += (x >> 8);
	x += (x >> 16);
	return x & 0x0000003F;
}

int* perm(int *limit, char c1, char c2, unsigned mask, char *p, char p_len)
{
	static const char s[SIZE] = DATA;
	static int *res;
	if (!p_len)
		res = (int *) calloc(SIZE, sizeof(int));
	if (!res)
		return NULL;

	for (register char i=0; i<SIZE; i++) {
		unsigned bit = 1 << i;
		if (mask & bit || !(limit[p_len] & bit))
			continue;

		char out[SIZE];
		sprintf(out, "%s%c", p, s[i]);

		if (p_len+1 == SIZE) {
			char v1=0, v2=0, h1=0, h2=0;
			for (i=0; i<SIZE; i++) {
				if (h1 < out[i]) {
					h1 = out[i];
					v1++;
				}
				if (h2 < out[SIZE-i-1]) {
					h2 = out[SIZE-i-1];
					v2++;
				}
			}
			if ((c1 && c1 != v1) || (c2 && c2 != v2))
				return NULL;

			for (i=0; i<SIZE; i++)
				res[i] |= out[i];

			return NULL;
		}
		perm(limit, c1, c2, mask|bit, out, p_len+1);
	}
	return res;
}

int solve(int **, int *);
inline int solve(int **s, int *clues)
{
	register char i, j;
	char done, clear, c1, c2;
	int *r;
	do {
		done = 1;
		for (i=0; i<SIZE; i++) {
			// check rows
			c1 = SIZE*4-i-1;
			c2 = SIZE+i;
			clear = 1;
			for (j=0; j<SIZE; j++) {
				if (pop(s[i][j]) > 1) {
					clear = 0;
					break;
				}
			}
			if (!clear) {
				r = perm(s[i], clues[c1], clues[c2], 0, "", 0);
				for (j=0; j<SIZE; j++) {
					if (s[i][j] != r[j]) {
						s[i][j] = r[j];
						done = 0;
					}
				}
				free(r);
			}

			//check columns
			int limit[SIZE];
			c1 = i;
			c2 = SIZE*3-i-1;

			clear = 1;
			for (j=0; j<SIZE; j++) {
				limit[j] = s[j][i];
				if (pop(limit[j]) > 1)
					clear = 0;
			}
			if (!clear) {
				r = perm(limit, clues[c1], clues[c2], 0, "", 0);
				for (j=0; j<SIZE; j++) {
					if (s[j][i] != r[j]) {
						s[j][i] = r[j];
						done = 0;
					}
				}
				free(r);
			}
		}
	} while (!done);

	for (j=0; j<SIZE; j++) {
		for(i=0; i<SIZE; i++) {
			if (!s[j][i])
				return 1;
		}
	}

	int **sc = (int **) malloc(SIZE * sizeof(int *));
	for(i=0; i<SIZE; i++)
		*(sc+i) = (int *) malloc(SIZE * sizeof(int));
	if (!*(sc+i-1))
		return -1;

	for (j=0; j<SIZE; j++) {
		for (i=0; i<SIZE; i++) {
			if (pop(s[j][i]) > 1) {
				for (register int k=0; k<SIZE; k++) {
					unsigned int b = 1 << k;
					if (!(s[j][i] & b))
						continue;
					register char c, r;
					for (c=0; c<SIZE; c++)
						memcpy(sc[c], s[c], SIZE * sizeof(int));
					sc[j][i] = b;
					if (!solve(sc, clues)) {
						for (c=0; c<SIZE; c++)
							memcpy(s[c], sc[c], SIZE * sizeof(int));
						break;
					}
				}
				goto leave;
			}
		}
	}
leave:
	for(i=0; i<SIZE; i++)
		free(*(sc+i));
	free(sc);
	return 0;
}

int** SolvePuzzle(int *clues) 
{
	register char i, j;
	int **s = (int **) malloc(SIZE * sizeof(int *));
	for(i=0; i<SIZE; i++)
		*(s+i) = (int *) malloc(SIZE * sizeof(int));
	if (!*(s+i-1))
		return NULL;
	{
		register char ones = (int) pow(2, SIZE) - 1;
		for (j=0; j<SIZE; j++)
			for (i=0; i<SIZE; i++)
				s[j][i] = ones;
	}

	solve(s, clues);

	for (j=0; j<SIZE; j++) {
		for (i=0; i<SIZE; i++) {
			s[j][i] = (int) log2(s[j][i])+1;
			printf("%2d ", s[j][i]);
		}
		printf("\n");
	}
	printf("\n");
	return s;
}

int main()
{
	printf("result: %d\n", check(SolvePuzzle(clues[0]), expected[0]));
	printf("result: %d\n", check(SolvePuzzle(clues[1]), expected[1]));
	printf("result: %d\n", check(SolvePuzzle(clues[2]), expected[1]));
	return 0;
}

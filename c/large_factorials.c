/*
 * Large Factorials
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define MAX(a,b) (a) > (b) ? (a) : (b)

#define ERR_MEM "Memory Allocation Failed!"
#define ERR_STR "Invalid character in string!"

char *mul(char *, char *);
char *factorial(int n);

char *mul(char *p1, char *p2)
{
	int l1 = strlen(p1);
	int l2 = strlen(p2);
	int n = MAX(l1, l2);

	char *a = (char *) calloc(n, sizeof(char));
	char *b = (char *) calloc(n, sizeof(char));
	char *m = (char *) calloc(n*n, sizeof(char));
	char *res = (char *) malloc(n*2+1 * sizeof(char));

	if (!a || !b || !m || !res)
		return ERR_MEM;

	register int i, j;
	for (i=0; i<l1; i++) {
		if (!isdigit(p1[i]))
			return ERR_STR;
		a[i+n-l1] = p1[i] - '0';
	}
	for (i=0; i<l2; i++) {
		if (!isdigit(p2[i]))
			return ERR_STR;
		b[i+n-l2] = p2[i] - '0';
	}

	char *p = m;
	for (j=0; j<n; j++) {
		for (i=0; i<n; i++)
			*(p++) = a[j] * b[i];
	}
	free(a);
	free(b);

	unsigned long int o=0;
	int s;
	p = res + n*2;
	for (j=n*2-2; j>=0; j--) {
		unsigned long int sum = o;
		register int k;
		for (i=n-1; i>=0; i--) {
			k = j-i;
			if (k>=0 && k<n && i+k == j)
				sum += *(m + n*k + i);

		}
		s = sum % 10;
		o = sum / 10;
		*(--p) = '0' + s;
	}

	while (o) {
		s = o % 10;
		o = o / 10;
		*(--p) = '0' + s;
	}
	free(m);

	for (i=n*2 - (p-res); i>1; i--) {
		if (*p != '0')
			break;
		p++;
	}

	s = (res + n*2) - p;
	char *result = (char *) malloc(s+1 * sizeof(char));
	if (!result)
		return ERR_MEM;
	strncpy(result, p, s);
	*(result + s) = 0;
	free(res);

	return result;
}

char *factorial(int n)
{
	if (n < 0)
		return "";
	if (n == 1)
		return "1";

	char *result = mul("1", "1");
	for (register int i=2; i<=n; i++) {
		char m[10], *sum;
		sprintf(m, "%d", i);
		sum = mul(result, m);
		free(result);
		result = sum;
	}
	return result;
}

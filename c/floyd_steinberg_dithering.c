static void floyd_steinberg_dithering(XImage *xim, int width, int height)
{
	Display dpy = XOpenDisplay(display);
	int screen_num = DefaultScreen(dpy);
	Visual *visual = DefaultVisual(dpy, screen_num);
	char *data = (char *) malloc(width * height * 4);
	XImage *img = XCreateImage(dpy,visual, DefaultDepth(dpy, screen_num), ZPixmap,
		0, data, width, height, 32, 0);

	for (int y = 0; y < height; y++) {
		for (int x = 0; x < width; x++) {
			unsigned long pixel = XGetPixel(xim, x, y);
			unsigned long new_pixel = pixel > 0xFFFFFF/2 ? 0xFFFFFF : 0;
			XPutPixel(xim, x, y, new_pixel);

			long quant_error = pixel - new_pixel;

			pixel = XGetPixel(xim, x+1, y);
			XPutPixel(xim, x+1, y, pixel + quant_error * 7 / 16);

			pixel = XGetPixel(xim, x-1, y+1);
			XPutPixel(xim, x-1, y+1, pixel + quant_error * 3 / 16);

			pixel = XGetPixel(xim, x, y+1);
			XPutPixel(xim, x, y+1, pixel + quant_error * 5 / 16);

			pixel = XGetPixel(xim, x+1, y+1);
			XPutPixel(xim, x+1, y+1, pixel + quant_error * 1 / 16);
		}
	}

	GC gc = DefaultGC(dpy, DefaultScreen(dpy));
	XPutImage(dpy, (Drawable) window, gc, xim, 0, 0, 0, 0, width, height);
	XDestroyImage(img);
}


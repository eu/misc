#### *Precedence and Order of Evaluation*

|Operators|Associativity|
| --- | --- |
|() [] -> .|> left to right|
|! ~ ++ -- + - *( type ) sizeof|<<|
|* / %|>|
|+ -|>|
|>>|>|
|< <= > >=|>|
|== !=|>|
|&|>|
|^|>|
|\||>|
|&&|>|
|\|\| |>|
|?:|<<|
|= += -= *= /= %= &= ^= \|= <<= >>= |<<|
|,|>| 

/*
 * CFLAGS = -march=native -O3 -pipe
 */

#include <stdio.h>
#include <stdlib.h>

int pop(unsigned);

long cache[] = {1, 3, 8, 20, 48, 112, 256, 576, 1280, 2816, 6144, 13312, 28672, 61440, 131072, 278528, 589824, 1245184, 2621440, 5505024, 11534336, 24117248, 50331648, 104857600, 218103808, 452984832, 939524096, 1946157056, 4026531840, 8321499136, 17179869184, 35433480192, 73014444032, 150323855360, 309237645312, 635655159808, 1305670057984, 2680059592704, 5497558138880, 11269994184704, 23089744183296, 47278999994368, 96757023244288, 197912092999680, 404620279021568, 826832744087552, 1688849860263936, 3448068464705536};

inline int pop(register unsigned x)
{
	x -= ((x >> 1) & 0x55555555);
	x =  (x & 0x33333333) + ((x >> 2) & 0x33333333);
	x =  (x + (x >> 4)) & 0x0F0F0F0F;
	x += (x >> 8);
	x += (x >> 16);
	return x & 0x0000003F;
}

long slow_count(long from, long to)
{
	long sum = 0;
	for (register long i=from; i<=to; i++)
		sum += pop(i);
		//sum += __builtin_popcount(i);
	return sum;
}

long fast_count(long x)
{
	if (!x)
		return 0;
	long b;
	__asm__("bsrq %0, %1" : "=r"(b) : "r"(x) : "cc");
	
	long n = 1 << b;
	long sum = 0;
	int ones = 0;
	for (register int i=b; i>=0; i--) {
		if (x & n) {
			for (register int j=i-1; j>=0; j--)
				sum += cache[j];
			sum += ones * (1 << i) + 1;
			ones++;
		}
		n >>= 1;
	}
	return sum;
}

/*
void make_cache(int limit)
{
	printf("long cache[] = {1");
	for (register int i=2; i<=limit; i++)
		printf(", %ld", (i+1) * (long) 1 << (i-2));
	printf("};\n");
}
*/
int main(int argc, char *argv[])
{
	//make_cache(48);
	if (argc != 3) {
		printf("Usage: %s [from] [to]\n", argv[0]);
		return 1;
	}
	long from = atol(argv[1]);
	long to = atol(argv[2]);

	printf("slow sum: %ld\n", slow_count(from, to));
	printf("fast sum: %ld\n", fast_count(to) - fast_count(from-1));
	return 0;
}

"
" Name: Clean Light Colorscheme
" Author: Eugene P.
" URL: https://github.com/eu01/vim-colors
"

hi clear
if exists("syntax_on")
  syntax reset
endif
let g:colors_name = "clean_light"
" italic fix for old vim
let &t_ZH="\e[3m"
let &t_ZR="\e[23m"
set background=light

"color 0  24364b  # Black
"color 1  e76d6d  # Red
"color 2  88c563  # Green
"color 3  ecb534  # Yellow
"color 4  65baf5  # Blue
"color 5  ae8fc1  # Purple
"color 6  69c5b4  # Cyan
"color 7  e4e4dd  # White
"color 8  6194ba  # Bright Black
"color 9  edbabf  # Bright Red
"color 10 bfedba  # Bright Green
"color 11 dfc56d  # Bright Yellow
"color 12 9fcce7  # Bright Blue
"color 13 cebaee  # Bright Purple
"color 14 85f1a1  # Bright Cyan
"color 15 a5b1bf  # Bright White

hi Normal		gui=NONE	guibg=#F5F8FA   guifg=#24292E   cterm=NONE	ctermbg=7	    ctermfg=black
hi NonText						guifg=#24292E			ctermbg=7	    ctermfg=black
hi comment		gui=ITALIC			guifg=#209020	cterm=ITALIC	ctermbg=7	    ctermfg=darkgray
hi SpecialComment	gui=ITALIC			guifg=#209020	cterm=ITALIC	ctermbg=7	    ctermfg=darkgray
hi constant						guifg=#24292E			ctermbg=7	    ctermfg=black
hi string						guifg=#005CC5			ctermbg=7	    ctermfg=darkblue
hi identifier						guifg=#24292E			ctermbg=7	    ctermfg=black
hi statement		gui=BOLD			guifg=#24292E	cterm=BOLD	ctermbg=7	    ctermfg=black
hi define		gui=BOLD			guifg=#24292E	cterm=BOLD	ctermbg=7	    ctermfg=black
hi preproc		gui=BOLD			guifg=#24292E	cterm=BOLD	ctermbg=7	    ctermfg=black
hi type			gui=BOLDITALIC			guifg=#24292E  cterm=BOLDITALIC ctermbg=7	    ctermfg=black
hi special						guifg=#24292E			ctermbg=7	    ctermfg=black
hi Underlined		gui=UNDERLINE			guifg=#24292E	cterm=UNDERLINE	ctermbg=7	    ctermfg=black
hi label		gui=BOLD			guifg=#24292E   cterm=BOLD	ctermbg=7	    ctermfg=black
hi operator						guifg=#24292E			ctermbg=7	    ctermfg=black
hi delimiter		gui=NONE			guifg=#D73A49	cterm=NONE	ctermbg=7	    ctermfg=darkred
hi phpParent		gui=NONE			guifg=#24292E	cterm=NONE	ctermbg=7	    ctermfg=black

hi ErrorMsg		gui=BOLD	guibg=lightred	guifg=#24292E	cterm=BOLD	ctermbg=7	    ctermfg=black
hi WarningMsg		gui=BOLD			guifg=#24292E	cterm=BOLD	ctermbg=7	    ctermfg=black
hi ModeMsg		gui=NONE			guifg=#24292E			ctermbg=7	    ctermfg=black
hi MoreMsg		gui=NONE			guifg=#24292E			ctermbg=7	    ctermfg=black
hi Error		gui=BOLD			guifg=white	cterm=BOLD	ctermbg=7	    ctermfg=black

hi MatchParen		gui=UNDERLINE	guibg=#F6F8FA	guifg=#D73A49	cterm=UNDERLINE	ctermbg=7	    ctermfg=darkred
hi Todo							guifg=#24292E			ctermbg=7	    ctermfg=black
hi Cursor				guibg=lightred	guifg=#24292E			ctermbg=white	    ctermfg=black
hi Search				guibg=#FFFF80	guifg=#24292E			ctermbg=yellow	    ctermfg=black
hi IncSearch						guifg=#24292E			ctermbg=7	    ctermfg=black
hi LineNr						guifg=darkgray			ctermbg=7	    ctermfg=8
hi title		gui=BOLD			guifg=#24292E	cterm=BOLD	ctermbg=7
hi StatusLine		gui=NONE	guibg=#F6F8FA	guifg=black	cterm=NONE	ctermbg=7	    ctermfg=black
hi StatusLineNC		gui=NONE	guibg=#F6F8FA	guifg=black	cterm=NONE	ctermbg=7	    ctermfg=black
hi VertSplit		gui=NONE			guifg=#24292E			ctermbg=7	    ctermfg=black

hi Visual				guibg=lightgray	guifg=#24292E			ctermbg=white	    ctermfg=black

hi DiffChange						guifg=#24292E			ctermbg=7	    ctermfg=black
hi DiffText						guifg=#24292E			ctermbg=7	    ctermfg=black
hi DiffAdd						guifg=#24292E			ctermbg=7	    ctermfg=black
hi DiffDelete						guifg=#24292E			ctermbg=7	    ctermfg=black

hi Pmenu		gui=NONE	guibg=lightgray guifg=#24292E	cterm=NONE	ctermbg=8	    ctermfg=black
hi PmenuSel		gui=reverse	guibg=lightgray guifg=#24292E	cterm=reverse	ctermbg=white	    ctermfg=black
hi PmenuSbar		gui=NONE	guibg=#BDE9F4	guifg=NONE	cterm=NONE	ctermbg=8	    ctermfg=black
hi PmenuThumb		gui=NONE	guibg=#6194BA	guifg=NONE	cterm=NONE	ctermbg=8	    ctermfg=black

hi Folded		gui=NONE	guibg=#F6F8FA	guifg=#D73A49	cterm=NONE	ctermbg=7	    ctermfg=brown
hi FoldColumn		gui=NONE	guibg=#F6F8FA	guifg=#D73A49	cterm=NONE	ctermbg=7	    ctermfg=brown
hi cIf0							guifg=#005CF5			ctermbg=7	    ctermfg=blue

hi Tabline		gui=NONE	guibg=darkgray   guifg=#24292E   cterm=NONE	ctermbg=darkgray    ctermfg=black
hi TablineFill		gui=NONE	guibg=darkgray   guifg=#24292E   cterm=NONE	ctermbg=darkgray    ctermfg=black
hi TablineSel				guibg=lightgray	guifg=#24292E			ctermbg=lightgray   ctermfg=black

hi ModeMsg				guibg=lightred	guifg=#24292E			ctermbg=white	    ctermfg=black
hi Directory				guibg=#F6F8FA	guifg=#005CC5			ctermbg=7	    ctermfg=blue
hi CursorColumn				guibg=lightred	guifg=#24292E	cterm=NONE	ctermbg=white	    ctermfg=black
hi CursorLine				guibg=lightred	guifg=#24292E	cterm=NONE	ctermbg=white	    ctermfg=black
hi SpecialKey						guifg=lightred					    ctermfg=darkred
hi NonText						guifg=lightred					    ctermfg=darkred
hi EndOfBuffer						guifg=darkgray			ctermbg=7	    ctermfg=8


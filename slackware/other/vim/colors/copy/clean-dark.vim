"
" Name: Clean Dark Colorscheme
" Author: Eugene P.
" URL: https://github.com/eu01/vim-colors
"

hi clear
if exists('syntax_on')
  syntax reset
endif
let g:colors_name='clean_dark'
" italic fix for old vim
let &t_ZH="\e[3m"
let &t_ZR="\e[23m"
set background=dark

"color 0  24364b  # Black
"color 1  e76d6d  # Red
"color 2  88c563  # Green
"color 3  ecb534  # Yellow
"color 4  65baf5  # Blue
"color 5  ae8fc1  # Purple
"color 6  69c5b4  # Cyan
"color 7  e4e4dd  # White
"color 8  6194ba  # Bright Black
"color 9  edbabf  # Bright Red
"color 10 bfedba  # Bright Green
"color 11 dfc56d  # Bright Yellow
"color 12 9fcce7  # Bright Blue
"color 13 cebaee  # Bright Purple
"color 14 85f1a1  # Bright Cyan
"color 15 a5b1bf  # Bright White

hi Cursor           ctermbg=7    ctermfg=NONE cterm=NONE           guibg=#dbe9f4  guifg=NONE     gui=NONE

hi Normal           ctermbg=0    ctermfg=7    cterm=NONE           guibg=#14263b  guifg=#dbe9f4  gui=NONE
hi Title            ctermbg=NONE ctermfg=7    cterm=BOLDUNDERLINE  guibg=NONE     guifg=#dbe9f4  gui=BOLDUNDERLINE
hi Todo             ctermbg=NONE ctermfg=4    cterm=BOLDUNDERLINE  guibg=NONE     guifg=#65baf5  gui=BOLDUNDERLINE
hi Directory        ctermbg=NONE ctermfg=6    cterm=NONE           guibg=NONE     guifg=#69c5b4  gui=NONE

hi LineNr           ctermbg=NONE ctermfg=8    cterm=NONE           guibg=NONE     guifg=#126180  gui=NONE
hi CursorLineNr     ctermbg=NONE ctermfg=8    cterm=NONE           guibg=NONE     guifg=#6194ba  gui=NONE
hi ColorColumn      ctermbg=8    ctermfg=NONE cterm=NONE           guibg=#6194ba  guifg=NONE     gui=NONE
hi SignColumn       ctermbg=8    ctermfg=NONE cterm=NONE           guibg=#6194ba  guifg=NONE     gui=NONE

hi Folded	    ctermbg=NONE ctermfg=3    cterm=NONE           guibg=NONE     guifg=#ecb534  gui=NONE
hi FoldColumn       ctermbg=NONE ctermfg=3    cterm=NONE           guibg=NONE     guifg=#ecb534  gui=NONE

hi Underlined       ctermbg=NONE ctermfg=NONE cterm=UNDERLINE      guibg=NONE     guifg=NONE     gui=UNDERLINE

hi Visual           ctermbg=NONE ctermfg=7    cterm=REVERSE        guibg=NONE     guifg=#88ACE0  gui=REVERSE
hi VisualNOS        ctermbg=NONE ctermfg=NONE cterm=UNDERLINE      guibg=NONE     guifg=NONE     gui=UNDERLINE
hi MatchParen       ctermbg=NONE ctermfg=3    cterm=NONE           guibg=NONE     guifg=#ecb534  gui=UNDERLINE
hi IncSearch        ctermbg=11   ctermfg=0    cterm=NONE           guibg=#dfc56d  guifg=#14263b  gui=NONE
hi Search           ctermbg=11   ctermfg=0    cterm=NONE           guibg=#dfc56d  guifg=#14263b  gui=NONE
hi CursorColumn     ctermbg=8    ctermfg=7    cterm=NONE           guibg=#6194ba  guifg=#dbe9f4  gui=NONE
hi CursorLine       ctermbg=8    ctermfg=7    cterm=NONE           guibg=#6194ba  guifg=#dbe9f4  gui=NONE

hi StatusLine       ctermbg=0    ctermfg=8    cterm=NONE           guibg=#14263b  guifg=#6194ba  gui=NONE
hi StatusLineNC     ctermbg=0    ctermfg=8    cterm=NONE           guibg=#14263b  guifg=#6194ba  gui=NONE
hi VertSplit        ctermbg=NONE ctermfg=8    cterm=NONE           guibg=NONE     guifg=#6194ba  gui=NONE
hi WildMenu         ctermbg=12   ctermfg=0    cterm=NONE           guibg=#9fcce7  guifg=#14263b  gui=NONE
hi ModeMsg          ctermbg=NONE ctermfg=11   cterm=BOLDITALIC     guibg=NONE     guifg=#ffff00  gui=BOLD
hi DiffAdd          ctermbg=2    ctermfg=0    cterm=NONE           guibg=#88c563  guifg=#14263b  gui=NONE
hi DiffDelete       ctermbg=1    ctermfg=0    cterm=NONE           guibg=#e76d6d  guifg=#14263b  gui=NONE
hi DiffChange       ctermbg=0    ctermfg=3    cterm=UNDERLINE      guibg=#14263b  guifg=#ecb534  gui=UNDERLINE
hi DiffText         ctermbg=3    ctermfg=0    cterm=NONE           guibg=#ecb534  guifg=#14263b  gui=NONE

hi Pmenu            ctermbg=7    ctermfg=0    cterm=NONE           guibg=#dbe9f4  guifg=#14263b  gui=NONE
hi PmenuSel         ctermbg=0    ctermfg=12   cterm=REVERSE        guibg=#14263b  guifg=#9fcce7  gui=REVERSE
hi PmenuSbar        ctermbg=7    ctermfg=NONE cterm=NONE           guibg=#dbe9f4  guifg=NONE     gui=NONE
hi PmenuThumb       ctermbg=8    ctermfg=NONE cterm=NONE           guibg=#6194ba  guifg=NONE     gui=NONE

hi SpellBad         ctermbg=NONE ctermfg=1    cterm=UNDERCURL      guibg=NONE     guifg=#e76d6d  gui=UNDERCURL
hi SpellCap         ctermbg=NONE ctermfg=1    cterm=UNDERCURL      guibg=NONE     guifg=#e76d6d  gui=UNDERCURL
hi SpellLocal       ctermbg=NONE ctermfg=9    cterm=UNDERCURL      guibg=NONE     guifg=#edbabf  gui=UNDERCURL
hi SpellRare        ctermbg=NONE ctermfg=9    cterm=UNDERCURL      guibg=NONE     guifg=#edbabf  gui=UNDERCURL

hi ErrorMsg         ctermbg=1    ctermfg=7    cterm=BOLD           guibg=#e76d6d  guifg=#14263b  gui=BOLD
hi WarningMsg       ctermbg=NONE ctermfg=1    cterm=NONE           guibg=NONE     guifg=#e76d6d  gui=NONE
hi MoreMsg          ctermbg=NONE ctermfg=4    cterm=NONE           guibg=NONE     guifg=#65baf5  gui=NONE
hi Question         ctermbg=NONE ctermfg=4    cterm=NONE           guibg=NONE     guifg=#65baf5  gui=NONE

hi TabLine          ctermbg=8    ctermfg=15   cterm=NONE           guibg=#6194ba  guifg=white	 gui=NONE
hi TabLineSel       ctermbg=0    ctermfg=15   cterm=BOLD           guibg=#14263b  guifg=white	 gui=BOLD
hi TabLineFill      ctermbg=8    ctermfg=15   cterm=NONE           guibg=#6194ba  guifg=white	 gui=NONE

hi Error            ctermbg=NONE ctermfg=1    cterm=REVERSE        guibg=NONE     guifg=#e76d6d  gui=REVERSE
hi Ignore           ctermbg=NONE ctermfg=NONE cterm=NONE           guibg=NONE     guifg=NONE     gui=NONE

hi Comment          ctermbg=0	 ctermfg=31   cterm=ITALIC         guibg=NONE     guifg=#536878  gui=ITALIC
hi SpecialComment   ctermbg=0	 ctermfg=31   cterm=BOLDITALIC     guibg=NONE     guifg=#536878  gui=BOLDITALIC
hi Statement        ctermbg=0    ctermfg=15   cterm=BOLD           guibg=#14263b  guifg=#dbe9f4  gui=BOLD
hi Type             ctermbg=0    ctermfg=15   cterm=BOLDITALIC     guibg=#14263b  guifg=#dbe9f4  gui=BOLDITALIC
"hi Function         ctermbg=0    ctermfg=15   cterm=NONE           guibg=#14263b  guifg=#A0D6B4  gui=NONE
hi PreProc          ctermbg=0    ctermfg=7    cterm=BOLD           guibg=#14263b  guifg=#dbe9f4  gui=BOLD

hi Constant         ctermbg=0    ctermfg=7    cterm=NONE           guibg=#14263b  guifg=#dbe9f4  gui=NONE
hi String           ctermbg=0    ctermfg=74   cterm=NONE           guibg=#14263b  guifg=#93CCEA  gui=NONE
hi Identifier       ctermbg=0    ctermfg=7    cterm=NONE           guibg=#14263b  guifg=#dbe9f4  gui=NONE
hi Label            ctermbg=0    ctermfg=7    cterm=BOLD           guibg=#14263b  guifg=#dbe9f4  gui=BOLD
hi Operator         ctermbg=0    ctermfg=7    cterm=NONE           guibg=#14263b  guifg=#dbe9f4  gui=NONE

hi EndOfBuffer      ctermbg=0    ctermfg=8    cterm=NONE           guibg=#14263b  guifg=#6194ba  gui=NONE
hi SpecialKey       ctermbg=0    ctermfg=1    cterm=NONE           guibg=#14263b  guifg=#ff0000  gui=NONE
hi NonText          ctermbg=0    ctermfg=1    cterm=NONE           guibg=#14263b  guifg=#ff0000  gui=NONE

hi Delimiter        ctermbg=NONE ctermfg=3    cterm=NONE           guibg=NONE     guifg=#ecb534  gui=NONE
hi phpParent        ctermbg=0    ctermfg=7    cterm=NONE           guibg=#14263b  guifg=#dbe9f4  gui=NONE

hi clear Number
hi clear Character
hi clear Function
hi clear Special
hi clear Boolean
hi clear Conceal

hi link helpSectionDelim   Comment
hi link helpHyperTextJump  Directory
hi link helpExample        Comment
hi link helpNote           Todo
hi link helpHyperOption    SpecialComment
hi link helpSpecial        SpecialComment
hi link helpHyperTextEntry SpecialComment
hi link helpCommand        SpecialComment


#!/bin/bash

case "$1" in
    suspend|hibernate)
	sync	
	;;
    resume|thaw)
	sleep 1
	sudo /root/sdb start
	sleep 2
	sudo /root/sdb stop
	sleep 1
	;;
esac

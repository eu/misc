{
 Copyright (c) 2007 Eugene P. <pieu@mail.ru>
 Written for my beloved little sister

 Permission to use, copy, modify, and distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.

 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
}
uses crt;
var
	field: array[1..80, 1..48] of integer;
	worm: array[1..50, 1..2] of integer;
	score, lives, worm_size, worm_grow, eat, speed, worm_color: integer;
	key, direction: char;

{------------------------------------------------------ Показать приветствие --}
procedure show_rules;
begin
	textbackground(1);
	clrscr;
	textcolor(14);
	gotoxy(32, 2);
	write('N I B B L E S');

	textcolor(7);
	gotoxy(1, 5);
	writeln('keys: 8, 4, 5, 6');
	writeln('');
	writeln('');
	writeln('(c) 2007 Eugene P. <pieu@mail.ru>');
	textcolor(3);
	gotoxy(1, 12);
	write('задержка 1000-10000: ');read(speed);
	write('цвет червяка 2-7: ');read(worm_color);
	if (worm_color > 7 ) or (worm_color < 2) then worm_color := 2;
	clrscr;
end;

{----------------------------------------------------------- Спрятать курсор --}
procedure hide_cursor;
begin
	asm
		mov ah,$01
		mov ch,$FF
		mov dh,$FF
		int $10
	end;
end;

{------------------------------------------------------------ Вернуть курсор --}
procedure show_cursor;
begin
	asm
		mov ah,$01
		mov ch,$0E
		mov dh,$0D
		int $10
	end;
end;

{------------------------------------------------------- Начальные установки --}
procedure init_game;
var
	x, y: integer;
begin
	randomize;

	score := 0;
	lives := 3;
	eat := 1;

	direction := 'e'; {направление n, s, w, e - с, ю, з, в}
	worm_grow := 0;   {прирост}
	worm_size := 5;   {длина}

	      {x}               {y}
	worm[1, 1] := 10; worm[1, 2] := 10;
	worm[2, 1] := 11; worm[2, 2] := 10;
	worm[3, 1] := 12; worm[3, 2] := 10;
	worm[4, 1] := 13; worm[4, 2] := 10;
	worm[5, 1] := 14; worm[5, 2] := 10;

	{очистка поля}
	for y:=1 to 48 do
	begin
		for x:=1 to 80 do
		begin
			field[x, y] := 0;
		end;
	end;

	{рисование рамки - горизонт.линии}
	for x:= 1 to 80 do
	begin
		field[x,  1] := 02;
		field[x, 48] := 02;
	end;

	{рисование рамки - верт.линии}
	for y:= 2 to 47 do
	begin
		field[1,  y] := 02;
		field[80, y] := 02;
	end;
end;

{---------------------------------------------------------- Нарисовать еду  --}
procedure draw_eat;
var
	eat_x, eat_y, x, y: integer;
begin
	for y:=1 to 48 do
	begin
		for x:=1 to 80 do
		begin
			if field[x, y] = 03 then
				field[x, y] := 0;
		end;
	end;

	while 1=1 do
	begin
		eat_x := random(79);
		eat_y := random(20);
		if (field[eat_x, eat_y*2] = 00) and (field[eat_x, eat_y*2-1] = 00) then
		begin
			field[eat_x, eat_y*2]   := 03;
			field[eat_x, eat_y*2-1] := 03;
			textcolor(15);
			textbackground(1);
			gotoxy(eat_x, eat_y);
			write(eat);
			break;
		end;
	end;
end;

{-------------------------------------------------------- Вывести результаты --}
procedure show_score;
begin
	textcolor(14);
	textbackground(1);
	gotoxy(70, 25);
	write('score: ', score);
end;

{---------------------------------------------------------- Червячок-ползок --}
function step_worm:integer;
var
	this_x, this_y, i: integer;
begin
{	worm x1 x2 x3 x4 x5
	     y1 y2 y3 y4 y5	}

	step_worm := 0000;

        this_x := worm[worm_size, 1];
	this_y := worm[worm_size, 2];

	{выбор - куда шагнуть. В this_xy }
	case direction of
		'n': this_y := this_y - 1;
		's': this_y := this_y + 1;
		'w': this_x := this_x - 1;
		'e': this_x := this_x + 1;
	end;

	if worm_grow > 0 then
	begin
		worm_size := worm_size + 1;
		worm_grow := worm_grow - 1;
	end
	else
	begin
		{затереть хвост}
		field[(worm[1,1]), (worm[1,2])] := 00;
		for i:= 1 to worm_size-1 do
		begin
			{переползание червяка}
			worm[i, 1] := worm[i+1, 1];
			worm[i, 2] := worm[i+1, 2];
		end;
	end;
	{шаг}
	worm[worm_size, 1] := this_x;
	worm[worm_size, 2] := this_y;

	{еда}
	if (field[this_x, this_y] = 03) then
	begin
		worm_grow := worm_grow + eat;
		score := score + eat;
		eat := eat + 1;
		if eat > 9 then
		begin
			step_worm := 0002;
			exit;
		end
		else
			draw_eat;
	end;
	{препятствие}
	if ((field[this_x, this_y] = 02) or (field[this_x, this_y] = 01)) then
	begin
		step_worm := 0001;
	end;

	for i:= 1 to worm_size do
	begin
		this_x := worm[i, 1];
		this_y := worm[i, 2];
		field[this_x, this_y] := 01;
	end;

end;

{------------------------------------------------------------ Рисовать поле --}
procedure draw_field;
var
	x, y: integer;
	u, d: integer; {up, down}
begin
{	field:
	00 - пустая клетка (синий=1)
	01 - червячье тело (зеленый=2)
	02 - препятствие (красный=4)
	03 - еда, клетка игнорируется }

	for y:= 1 to 24 do
	begin
		for x:= 1 to 80 do
		begin
			{nibbles}
			case field[x, y*2-1] of
				00: u := 1;
				01: u := worm_color;
				02: u := 4;
			else
				u := -1;
			end;
			case field[x, y*2] of
				00: d := 1;
				01: d := worm_color;
				02: d := 4;
			else
				d := -1;
			end;
			if (u > 0) and (d > 0) then
			begin
				textcolor(u);
				textbackground(d);
				gotoxy(x, y);
				write(chr($DF));
			end;
		end;
	end;
end;

{------------------------------------------------ Начало основной программы --}
begin
	show_rules;
	hide_cursor;

	while 1=1 do
	begin
		init_game;
		draw_eat;
		show_score;

		key := ' ';
		repeat
			delay(speed);
			draw_field;
			case step_worm of
				0001:	begin
					init_game;
					draw_eat; {жизнь-1}
					end;
				0002: 	begin
					break; {уровень+1}
					end;
			end;
			show_score;
			if keypressed then
			begin
				key := readkey;
				case key of
					'8': begin
						if direction <> 's' then
							direction := 'n';
						end;
					'5': begin
						if direction <> 'n' then
							direction := 's';
						end;
					'4': begin
						if direction <> 'e' then
							direction := 'w';
						end;
					'6': begin
						if direction <> 'w' then
							direction := 'e';
						end;
				end;

			end;
		until key = chr(27); {ESC}
		break;
	end;
	show_cursor;
end.

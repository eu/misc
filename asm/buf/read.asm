                .model  tiny
                .586
                .code
                org     100h
start:
                mov     ax,3D02h
                lea     dx,fname
                int     21h
                jb      short quit

                mov     bx,ax
                mov     ah,3Fh
                lea     dx,file_buffer
                mov     cx,0Ch
                int     21h
                jae     short close
                mov     ah,9
                lea     dx,file_buffer
                int     21h
close:
                mov     ah,3Eh
                int     21h
                mov     ah,9
                lea     dx,msg2
                int     21h
quit:
                ret
fname           db      "c:\test.AVI",0
file_buffer     db      100h dup (0)
msg1            db      "error!$"
msg2            db      "ok.$"
                end     start

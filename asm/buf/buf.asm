
                .model  tiny
                .code
                .586
                org     100h

;-------------------------------------------------------------------- Options -

BUF_SIZE        equ     0FF00h
BLOCK_SIZE      equ     01000h

;------------------------------------------------------------------------------
start:
                jmp     init

;------------------------------------------------------- Trap interrupt (21h) -

trap_int        proc    far
                cli
                pusha
;--------
trap_open:
;--------
                cmp     ah,3Dh
                jne     short trap_seek
                push    es
                push    ds
                pop     es
                mov     di,dx
                mov     al,'.'
                mov     cx,0FFFFh
                cld
                repne   scasb
                pop     es
                mov     ax,[di]
                cmp     ax,"AV";VA
                jne     trap_int_ret
                popa
                int     80h
                jb      short bad_handler
                pop     cs:w1
                pop     cs:w2
                popf
                clc
                push    cs:w2
                push    cs:w1
                mov     cs:avi_handler,ax
                sti
                retf
bad_handler:
                hlt
;--------
trap_seek:
;--------
                cmp     ah,42h
                jne     short trap_close
                cmp     bx,cs:avi_handler
                jne     trap_int_ret
                mov     cs:tail,offset cs:buf
                jmp     trap_int_ret
;---------
trap_close:
;---------
                cmp     ah,3Eh
                jne     short trap_read
                cmp     bx,cs:avi_handler
                jne     short trap_int_ret
                mov     cs:avi_handler,0
                mov     cs:tail,offset cs:buf
                jmp     short trap_int_ret
;--------
trap_read:
;--------
                cmp     ah,3Fh
                jne     short trap_int_ret
                cmp     bx,cs:avi_handler
                jne     short trap_int_ret
                push    ds
                push    es
                push    ds      ; es = ds
                pop     es
                push    cs      ; ds = cs
                pop     ds
                mov     how,cx
                sti
repeat:
                lea     si,buf
                mov     ax,tail
                sub     ax,si
                cmp     ax,cx
                jb      short repeat
                cli
                sub     tail,cx
                mov     di,dx
                cld
                rep     movsb
                push    cs      ; es = cs
                pop     es
                lea     di,buf
                mov     cx,tail
                sub     cx,offset buf
                rep     movsb
                pop     es
                pop     ds
                popa
                pop     cs:w1
                pop     cs:w2
                popf
                clc
                push    cs:w2
                push    cs:w1
                mov     ax,cs:how
                sti
                retf
trap_int_ret:
                popa
                sti
                jmp     dword ptr cs:old_int21h

old_int21h      dd      0
w1              dw      0
w2              dw      0
how             dw      0
trap_int        endp

;----------------------------------------------- Buffer write interrupt (1Ch) -

buf_write_int   proc    far
                ;int     81h
                cmp     word ptr cs:avi_handler,0
                je      short buf_write_int_ret
                cmp     byte ptr cs:avi_lock,0
                jne     short buf_write_int_ret
                mov     byte ptr cs:avi_lock,0FFh
                pusha
                push    es
                mov     ah,34h
                int     34h
                mov     ah,es:[bx]
                mov     al,es:[bx-1]
                pop     es
                test    ax,ax
                jne     short dos_busy
                push    ds
                push    cs
                pop     ds
                mov     cx,BLOCK_SIZE
                lea     ax,buf
                add     ax,BUF_SIZE
                sub     ax,tail
                je      short buffer_full
                cmp     ax,cx
                jae     short write_block
                mov     cx,ax
write_block:
                mov     ah,3Fh
                mov     bx,avi_handler
                mov     dx,tail
                int     80h
                jae     short ok
                hlt
ok:
                add     tail,ax
buffer_full:

                pop     ds
		jmp	short skip_beep
dos_busy:
		pusha
		mov     al,10110110b
                out     43h,al
                mov     al,0Dh
                out     42h,al
                mov     al,11h
                out     42h,al
                in      al,61h
                or      al,00000011b
                out     61h,al
                mov     cx,0000h
                mov     dx,001h
                mov     ah,86h
                int     15h
                in      al,61h
                and     al,11111100b
                out     61h,al
		popa
skip_beep:
                popa
                mov     byte ptr cs:avi_lock,0
buf_write_int_ret:
                iret

old_int1Ch      dd      0
avi_lock        db      0
buf_write_int   endp

;------------------------------------------------------------------- TSR data -


buf             db      BUF_SIZE dup (0)
tail            dw      buf
avi_handler     dw      0
tsr_end         equ     $

;---------------------------------------------------------------- Entry point -

init:
                mov     ax,3521h
                int     21h
                mov     word ptr old_int21h,bx
                mov     word ptr old_int21h+2,es
                mov     ax,351Ch
                int     21h
                mov     word ptr old_int1Ch,bx
                mov     word ptr old_int1Ch+2,es

                mov     ax,2580h
                mov     dx,word ptr old_int21h+2
                mov     ds,dx
                mov     dx,word ptr cs:old_int21h
                int     21h
                mov     ax,2581h
                mov     dx,word ptr cs:old_int1Ch+2
                mov     ds,dx
                mov     dx,word ptr cs:old_int1Ch
                int     21h

                push    cs
                pop     ds
                mov     ax,2521h
                lea     dx,trap_int
                int     21h
                mov     ax,251Ch
                lea     dx,buf_write_int
                int     21h

                mov     ah,9
                lea     dx,msg_title
                int     80h
                mov     ah,27h
                lea     dx,tsr_end
                int     27h

                ret

;----------------------------------------------------------------------- Data -

msg_title       db      "Buf installed.$"
end             start
                .model  tiny
                .code
                .386
                org     100h
start:          jmp     short init

buf_int 	proc    far
                cli
                pusha
                cmp     ah,40h
                jnz     skip_beep
                mov     al,10110110b
                out     43h,al
                mov     al,0Dh
                out     42h,al
                mov     al,11h
                out     42h,al
                in      al,61h
                or      al,00000011b
                out     61h,al
                mov     cx,0000h
                mov     dx,00120h
                mov     ah,86h
                int     15h
                in      al,61h
                and     al,11111100b
                out     61h,al
skip_beep:      popa
                sti
                jmp     dword ptr cs:old_int
old_int         dd      0
buf_int         endp
buf_int_end     equ     $

init:           mov     ax,3521h
                int     21h
                mov     word ptr old_int,bx
                mov     word ptr old_int+2,es
                mov     ax,2580h
                mov     dx,word ptr old_int+2
                mov     ds,dx
                mov     dx,word ptr cs:old_int
                int     21h
                mov     ax,2521h
                lea     dx,buf_int
                int     21h
                mov     ah,27h
                lea     dx,buf_int_end
                int     27h
                ret
end             start

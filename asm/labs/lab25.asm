
		.model  tiny
                .code
                .586
                org     100h

;------------------------------------------------------------------------------

X	equ	6
Y	equ	-2

start:
;---------------------------------------------------------------- Entry point -
        mov     ah,9
        lea     dx,msg_title	; show title
        int     21h

		mov	si,X		; si = X
		mov	cx,Y		; cx = Y
		cmp	si,cx
		jle	short l1	; if (x < y) goto l1

		mov	ax,si
		shl	ax,2		; z = x * 4
		cwd
		idiv	cx		; z = z / y
		mov	bx,ax
		mov	ax,bx
		imul	bx
		imul	bx		; z = z ^ 3

		jmp	short l2	; ax = result
l1:
		mov	ax,si
		imul	ax,ax,22h	; ax = x * 34
		mov	si,ax		; x = ax
		mov	ax,cx
		imul	cx		;
		mov	cx,ax		; cx = y * y;

		mov	ax,si		; abs(si - cx)
		sub	ax,cx
		cwd
		xor	ax,dx
		sub	ax,dx
l2:
		mov	z,ax

		; --------------------
		; printf("z = %d", z);
		; --------------------

		;mov	ax,-123		; *** debug print ***

		or	ax,ax
		jge	short positive	; if (z < 0)
		mov	sign, "-"
		cwd			        ; abs(z)
		xor	ax,dx
		sub	ax,dx
		jmp	short positive

		sub	ax,2710h
		inc	byte ptr res
positive:
		cmp	ax,270Fh	; while > 9999
		jg	$-10		; jump previous sub
		jmp	short $+9	; jump next cmp ;)

		sub	ax,3E8h
		inc	byte ptr res+1

		cmp	ax,3E7h		; while > 999
		jg	$-10
		jmp	short $+9

		sub	ax,64h
		inc	byte ptr res+2

		cmp	ax,63h		; while > 99
		jg	$-10
		jmp	short $+9

		sub	ax,0Ah
		inc	byte ptr res+3

		cmp	ax,9h		; while > 9
		jg	$-10
		mov	res+4,al

		add	word ptr res+0,3030h
		add	word ptr res+2,3030h
		add	byte ptr res+4,30h

shift:	mov	ax, word ptr [res]
		cmp	al, 30h
		jne	short shift_complete
		cmp	ah, 0Dh
		je	short shift_complete
		lea	si,res+1	; copy from
		lea	di,res		; copy to
		mov	cx, 5
		cld
		rep movsb
		jmp	short shift
shift_complete:
		mov     ah,9
        lea     dx,msg_res
        int     21h

        ret
;----------------------------------------------------------------------- Data -
z       		dw	?
msg_title       db  "if (x > y) z = (4*x/y)^3", 0Dh, 0Ah
		        db	"else z = abs(34*x - y^2)", 0Dh, 0Ah
        		db	24 dup ("-"), 0Dh, 0Ah, "$"
msg_res	    	db	"z = "
sign		    db	" "
res     		db	5 dup (0), 0Dh, 0Ah, "$"
trash	    	db	5 dup (?) ; for shift
end             start

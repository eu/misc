
		.model  tiny
                .code
                .586
                org     100h

;------------------------------------------------------------------------------

;			p4
; z = max( min(a+b, b-2*c, a+c-3*b), (a+2*b)**3)
; 		 p1   p2       p3         p5

wA	equ	3
wB	equ	6
wC	equ	3

start:                
;---------------------------------------------------------------- Entry point -
        mov     ah,9
        lea     dx,msg_title	; show title
        int     21h

		mov	di,wA		; a
		mov	si,wB		; b
		mov	c,wC		; c

		mov	ax,di
		add	ax,si
		mov	p1,ax		; p1 = a + b

		mov	ax,c
		add	ax,ax
		mov	dx,si
		sub	dx,ax
		mov	p2,dx		; p2 = b - 2 * c

		mov	ax,si
		imul	ax,ax,3
		mov	dx,di
		add	dx,c
		sub	dx,ax
		mov	p3,dx		; p3 = a + c - 3 * b

		mov	ax,p1
		cmp	ax,p2
		jge	short l1
		mov	ax,p1
		jmp	short l2
l1:                                     
		mov	ax,p2		; p4 = min(p1, p2, p3)
l2:					    ; p4 (ax) = min(p1, p2)
		mov	cx,ax
		cmp	cx,p3
		jge	short l3
		mov	ax,cx
		jmp	short l4
l3:
		mov	ax,p3
l4:
		mov	cx,ax		; p4 (cx) = min(p4, p3)

		mov	ax,si		
		add	ax,ax       ; b*2
		add	ax,di		; b+a
		mov	bx,ax
		mov	ax,bx
		imul	bx
		imul	bx		; **3
		mov	bx,ax		; p5 (bx) = (a+2*b)**3

		cmp	cx,bx
		jle	short l5
		mov	ax,cx
		jmp	short l6
l5:
		mov	ax,bx
l6:
		mov	z,ax
		int 	3

		mov	cx,ax		; SHOW Z = 
		shr	ax,12
		lea	bx,hex_table
		xlatb	
		mov	res,al
		mov	ax,cx
		shr	ax,8
		and	ax,0000000000001111b
		lea	bx,hex_table
		xlatb	
		mov	res+1,al
		mov	ax,cx
		shr	ax,4
		and	ax,0000000000001111b
		lea	bx,hex_table
		xlatb	
		mov	res+2,al
		mov	ax,cx
		and	ax,0000000000001111b
		lea	bx,hex_table
		xlatb	
		mov	res+3,al

		mov     ah,9
                lea     dx,msg_res
                int     21h
		int	3

                ret
;----------------------------------------------------------------------- Data -

msg_title       db      "z = max( min(a+b, b-2*c, a+c-3*b), (a+2*b)**3)"
		db	0Ah,0Dh,"$"
msg_res         db      "z = "
res		db	4 dup(?)
		db	"h",0Ah,0Dh,"$"
hex_table	db	"0123456789ABCDEF"
c		dw	?
z		dw	?
p1		dw	?
p2		dw	?
p3		dw	?
end             start

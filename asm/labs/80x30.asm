		.model	tiny
		.code
		.186
		org	100h
start:
		mov	ax,3
		int	10h

		mov	dx,3CCh
		in	al,dx
		mov	dl,0C2h
		or	al,0C0h
		out	dx,al

		mov	dx,03D4h
		lea	si,crt480
		mov	cx,crt480_l
		rep	outsw

		push	0040h
		pop	es
		mov	byte ptr es:[84h],29
		ret

crt480		dw	0C11h,0B06h,3E07h,0EA10h,0DF12h,0E715h,0416h
crt480_l = ($-crt480)/2

end		start	
		

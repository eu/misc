#!/bin/sh
#
# Written by Eugene P. <pieu@mail.ru>
#

echo
echo "--------------------------------------------"
echo "|  Скрипт настройки vpn (L2TP) соединений  |"
echo "--------------------------------------------"
echo

if [ `/usr/bin/id -u` != 0 ] ; then
	echo "Только пользователь с правами root может настраивать VPN"
	echo "Воспользуйтесь командой 'su'"
	exit 1
fi

if [ -r /etc/slackware-version ]; then
	cat /etc/slackware-version
	echo
else
	echo "Внимание! Cкрипт писался для Slackware!"
fi

echo -n "Введите название vpn соединения (enter = didan): "
read vpn_name
if [ "$vpn_name" = "" ]; then
	vpn_name="didan"
fi

echo
echo "Для числовых логинов/паролей используйте кавычки \"12345\""
echo -n "Введите ваш логин     (enter = login): "
read vpn_login
if [ "$vpn_login" = "" ]; then
	vpn_login="login"
fi

echo -n "Введите ваш пароль (enter = password): "
read vpn_password
if [ "$vpn_password" = "" ]; then
	vpn_password="password"
fi

echo
echo "Имеющиеся сетевые интерфейсы ethernet:"
ifconfig eth | grep eth
echo
echo -n "Использовать интерфейс eth (0-3): "
read eth
if [ "$eth" = "" ]; then
	eth="0"
fi

echo -n "Использовать dhcp для eth$eth (Y/n): "
read dhcp
if [ "$dhcp" = "" ]; then
	dhcp="y"
fi

if [ "$dhcp" = "y" -o "$dhcp" = "Y" ]; then
	echo -n "Получать dns с помощью dhcp? (Y/n): "
	read dhcp_keepresolv
	if [ "$dhcp_keepresolv" = "" ]; then
		dhcp_keepresolv="y"
	fi
fi

echo
echo -n "IP адрес vpn сервера (enter = 172.17.192.252): "
read vpn_server
if [ "$vpn_server" = "" ]; then
	vpn_server="172.17.192.252"
fi

echo
echo "cat /etc/resolv.conf"
cat /etc/resolv.conf

echo
echo -n "Адрес dns1 (enter = 172.17.192.254): "
read vpn_dns1
if [ "$vpn_dns1" = "" ]; then
	vpn_dns1="172.17.192.254"
fi

echo -n  "Адрес dns2 (enter = 172.17.192.240): "
read vpn_dns2
if [ "$vpn_dns2" = "" ]; then
	vpn_dns2="172.17.192.240"
fi

echo
route
echo
echo -n "Адрес шлюза (enter = 172.17.205.1): "
read vpn_gate
if [ "$vpn_gate" = "" ]; then
	vpn_gate="172.17.205.1"
fi

echo
echo -n "Установить пакет openl2tp? (Y/n): "
read openl2tp_inst
if [ "$openl2tp_inst" = "" ]; then
	openl2tp_inst="y"
fi

echo
echo -n  "Установить rc.firewall (Y/n): "
read firewall_inst
if [ "$firewall_inst" = "" ]; then
	firewall_inst="y"
fi

echo

# install openl2tp.
# https://slackbuilds.org/repository/13.37/network/openl2tp/
if [ "$openl2tp_inst" = "y" -o "$openl2tp_inst" = "Y" ]; then
	echo "Устанавливается пакет openl2tp"
	installpkg openl2tp-1.8-i486-1_SBo.tgz
fi

mv /etc/ppp/options /etc/ppp/options~
cat /dev/null > /etc/ppp/options
chmod 755 /etc/rc.d/rc.rpc

# autorun openl2tpd
local="/etc/rc.d/rc.local"
if [ ! -r $local ]; then
	echo "#!/bin/sh" > $local
	echo "#" >> $local
	echo "# /etc/rc.d/rc.local:  Local system initialization script." >> $local
	echo "#" >> $local
else
	cp $local "$local~"
	chmod 644 "$local~"
fi

if [ `cat $local | grep -c "rc.openl2tpd start"` = 0 ]; then
	cp $local "$local~"
	echo >> $local
	echo "# Initialize L2TP" >> $local
	echo "if [ -x /etc/rc.d/rc.routes ]; then" >> $local
	echo "  . /etc/rc.d/rc.routes" >> $local
	echo "fi" >> $local
	echo >> $local
	echo "if [ -x /etc/rc.d/rc.openl2tpd ]; then" >> $local
	echo "  . /etc/rc.d/rc.openl2tpd start" >> $local
	echo "fi" >> $local
else
	echo "Автозапуск openl2tpd - пропущен!"
	if [ `cat $local | grep -c "rc.routes"` = 0 ]; then
		echo "Удалите секцию Initialize L2TP в rc.local и повторите установку"
	fi
fi
chmod 755 $local

# create rc.routes
routes="/etc/rc.d/rc.routes"
echo "#!/bin/sh" > $routes
echo >> $routes
echo 'echo -n "Adding VPN routes ... "' >> $routes
echo "route add -host $vpn_dns1 gw $vpn_gate dev eth$eth > /dev/null" >> $routes
echo "route add -host $vpn_dns2 gw $vpn_gate dev eth$eth > /dev/null" >> $routes
echo "route add -host $vpn_server gw $vpn_gate dev eth$eth > /dev/null" >> $routes
echo 'echo "[done]"' >> $routes
chmod 755 $routes

# configure dhcp
dhcp_conf="/etc/rc.d/rc.inet1.conf"
mv $dhcp_conf "$dhcp_conf~"
if [ "$dhcp" = "y" -o "$dhcp" = "Y" ]; then
	if [ "$dhcp_keepresolv" = "y" -o "$dhcp_keepresolv" = "Y" ]; then
		sed -e "s/^USE_DHCP\\[$eth.*$/USE_DHCP\\[$eth\\]=\"yes\"/g" \
		    -e "s/^DHCP_KEEPRESOLV\\[$eth.*$/DHCP_KEEPRESOLV\\[$eth\\]=\"yes\"/g" \
		    "$dhcp_conf~" > $dhcp_conf
	else
		if [ `cat "$dhcp_conf~" | grep -c "^DHCP_KEEPRESOLV\[$eth"` = 0 ]; then
			sed -e "s/^USE_DHCP\\[$eth\\]=\".*$/USE_DHCP\\[$eth\\]=\"yes\"\\nDHCP_KEEPRESOLV\\[$eth\\]=\"no\"/g" \
				"$dhcp_conf~" > $dhcp_conf
		else
			sed -e "s/USE_DHCP\\[$eth\\]=\"\"/USE_DHCP\\[$eth\\]=\"yes\"/g" \
			    -e "s/^DHCP_KEEPRESOLV\\[$eth.*$/DHCP_KEEPRESOLV\\[$eth\\]=\"no\"/g" \
			    "$dhcp_conf~" > $dhcp_conf
		fi
		cp /etc/resolv.conf /etc/resolv.conf~
		echo "nameserver $vpn_dns1" > /etc/resolv.conf
		echo "nameserver $vpn_dns2" >> /etc/resolv.conf
	fi
else
	sed -e "s/^USE_DHCP\\[$eth.*$/USE_DHCP\\[$eth\\]=\"\"/g" \
	    -e "s/^DHCP_KEEPRESOLV\\[$eth.*$/DHCP_KEEPRESOLV\\[$eth\\]=\"\"/" \
	    "$dhcp_conf~" > $dhcp_conf

fi
chmod 600 $dhcp_conf

# create /etc/openl2tpd.conf
l2tpd_conf=/etc/openl2tpd.conf
if [ -r $l2tpd_conf ]; then
	mv $l2tpd_conf "$l2tpd_conf~"
fi
echo "system modify deny_remote_tunnel_creates=yes \\" >> $l2tpd_conf
echo "	tunnel_establish_timeout=60 \\" >> $l2tpd_conf
echo "	session_establish_timeout=30 \\" >> $l2tpd_conf
echo "	tunnel_persist_pend_timeout=60 \\" >> $l2tpd_conf
echo "	session_persist_pend_timeout=30" >> $l2tpd_conf
echo >> $l2tpd_conf
echo "peer profile modify profile_name=default lac_lns=lac" >> $l2tpd_conf
echo >> $l2tpd_conf
echo "ppp profile modify profile_name=default \\" >> $l2tpd_conf
#echo "	mtu=1460 mru=1460 \\" >> $l2tpd_conf
echo "	auth_pap=no auth_eap=no auth_none=no auth_mschapv1=no auth_mschapv2=yes \\" >> $l2tpd_conf
echo "	default_route=no proxy_arp=no" >> $l2tpd_conf
echo >> $l2tpd_conf
echo "tunnel create tunnel_name=$vpn_name dest_ipaddr=$vpn_server persist=yes" >> $l2tpd_conf
echo >> $l2tpd_conf
echo "session create tunnel_name=$vpn_name session_name=$vpn_name \\" >> $l2tpd_conf
echo "	user_name=$vpn_login user_password=$vpn_password" >> $l2tpd_conf
chmod 644 $l2tpd_conf

# create chap-secrets
mv /etc/ppp/chap-secrets /etc/ppp/chap-secrets~
echo "$vpn_login	*	$vpn_password" > /etc/ppp/chap-secrets

#Добавление шлюзов по умолчанию в /etc/ppp/ip-up
if [ -r /etc/ppp/ip-up ]; then
	mv /etc/ppp/ip-up /etc/ppp/ip-up~
	chmod 644 /etc/ppp/ip-up~
fi
echo "#!/bin/sh" > /etc/ppp/ip-up
echo >> /etc/ppp/ip-up
echo "PATH=/usr/local/sbin:/usr/sbin:/sbin:/usr/local/bin:/usr/bin:/bin" >> /etc/ppp/ip-up
echo "export PATH" >> /etc/ppp/ip-up
echo >> /etc/ppp/ip-up
echo "ifconfig \$1 mtu 1460" >> /etc/ppp/ip-up
echo "route del default" >> /etc/ppp/ip-up
echo "route add default dev \$1 metric 5" >> /etc/ppp/ip-up
chmod 744 /etc/ppp/ip-up

#Добавление шлюзов по умолчанию в /etc/ppp/ip-down
if [ -r /etc/ppp/ip-down ]; then
	mv /etc/ppp/ip-down /etc/ppp/ip-down~
	chmod 644 /etc/ppp/ip-down~
fi
echo "#!/bin/sh" > /etc/ppp/ip-down
echo >> /etc/ppp/ip-down
echo "PATH=/usr/local/sbin:/usr/sbin:/sbin:/usr/local/bin:/usr/bin:/bin" >> /etc/ppp/ip-down
echo "export PATH" >> /etc/ppp/ip-down
echo >> /etc/ppp/ip-down
echo "route del default" >> /etc/ppp/ip-down
echo "route add default gw $vpn_gate dev eth$eth" >> /etc/ppp/ip-down
chmod 744 /etc/ppp/ip-down

# install firewall
if [ "$firewall_inst" = "y" -o "$firewall_inst" = "Y" ]; then
	if [ -r /etc/rc.d/rc.firewall ]; then
		echo "Обнаружен /etc/rc.d/rc.firewall"
		echo "Сохранен /etc/rc.d/rc.firewall~"
		mv /etc/rc.d/rc.firewall /etc/rc.d/rc.firewall~
		chmod 644 /etc/rc.d/rc.firewall~
	fi
	cp rc.firewall /etc/rc.d/rc.firewall
	chmod 744 /etc/rc.d/rc.firewall
fi

chmod 755 /etc/rc.d/rc.openl2tpd

echo
echo "Все измененные файлы сохранены «file~»"
echo "Для очистки логов и временных файлов запустить ./clean"

# end
echo "Настройка завершена."
echo
exit 0


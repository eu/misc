#!/usr/bin/perl
#
# A script for creating and sending reports metering of electricity consumption.
# Layouts: 30818, 30917.

# Copyright (c) 2016 Eugene P. <pieu@mail.ru>

# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.

# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

use warnings;
use strict;
use XML::Simple;
use Time::Moment;
use Cwd;
use Mail::Outlook;
#use Data::Dumper;

# Editable data:
my $warnings = 0;
my %meters = (
	# Чайкино 1
	'SL7_53026867' => { 'code'=>'5522', 'multiplier'=>1200, 'error'=>0 },
	# Чайкино 2
	'SL7_53001869' => { 'code'=>'5511', 'multiplier'=>1200, 'error'=>0 },
	# Орджоникидзе
	'SL7_53026866' => { 'code'=>'5533', 'multiplier'=>900,  'error'=>0 },
	# ПП1
	'SL7_53026870' => { 'code'=>'5544', 'multiplier'=>1800, 'error'=>0 }
	);
my $email = 'askue@gppes.donetsk.ua';
my $attempts = 10;
my $delay = 60;

my ($doc, $xml, $from, $to, $allow_mailing, $layout, $meter, $fh);
my $error_log = '';
#-------------------------------------------------------------------------------
sub dt(;$)
{
	my $time = shift;
	$time ||= time();
	my ($min, $hour, $day, $month, $year) = (localtime($time))[1,2,3,4,5];
	$day = sprintf("%02d", $day);
	$month = sprintf("%02d", $month+1);
	$year += 1900;
	$min = sprintf("%02d", $min);
	$hour = sprintf("%02d", $hour);
	return ($day, $month, $year, $hour, $min);
}
#-------------------------------------------------------------------------------
sub write_log($)
{
	my $msg = shift;
	open(my $log, '>>', 'error.log')
		or die "[ ERROR ] Can't open error.log file!\n";
	my ($day, $month, $year, $hour, $min) = dt();
	my $str = "$day.$month.$year $hour:$min $msg\n";
	print $log $str;
	close $log;
	$error_log .= $str;
}
#-------------------------------------------------------------------------------
sub warning(@)
{
	my ($dev, $msg) = @_;
	$meters{$dev}{'error'} = 1;
	($warnings)
		and print("[WARNING] Device $dev $msg\n");
}
#-------------------------------------------------------------------------------
sub error($)
{
	my $msg = shift;
	write_log($msg);
	die "[ ERROR ] $msg\n";
}
#-------------------------------------------------------------------------------
sub billing_data(@)
{
	my ($dev, $mem) = @_;
	my $v = $xml->{data}->{select}->{device}->{$dev}->{par}->{$mem}->{v};
	return (defined $v) ? split('[,:]', $v) : ();
}
#-------------------------------------------------------------------------------
sub read_daily(@)
{
	my ($dev, $mem, $mul) = @_;
	$mul ||= 1;

	my @bd = billing_data($dev, '11601000');
	my $v = $xml->{data}->{select}->{device}->{$dev}->{par}->{$mem}->{v};
	unless (@bd && defined $v) {
		warning($dev, "($mem) no daily data!");
		return '';
	}
	my @val = split('[,]', $v);
	my $size = scalar @val;
	unless (($bd[0] ne '') && ($val[0] =~ /^\d+T21:(\d+)$/)) {
		warning($dev, "($mem) bad first value in daily data!");
		return '';
	}
	my $period = 0;
	for (my $i=1; $i<$size; $i++) {
		unless ($val[$i] =~ /^(\d+):(\d+)$/) {
			warning($dev, "($mem) bad daily data!");
			return '';
		}
		$period += $1;
	}
	if ($period != 86400) {
		warning($dev, "($mem) bad daily data! $period sec.");
		return '';
	}
	unless ($val[$size-1] =~ /^\d+:(\d+)$/) {
		warning($dev, "($mem) bad last value in daily data!");
		return '';
	}
	return ($1 * $mul / 1000);
}
#-------------------------------------------------------------------------------
sub read_hhour(@)
{
	my ($dev, $mem) = @_;
	my $empty = ':' x 50;
	my @bd = billing_data($dev, '3245160');
	my $v = $xml->{data}->{select}->{device}->{$dev}->{par}->{$mem}->{v};
	unless (@bd && defined $v) {
		warning($dev, "($mem) no half-hour data!");
		return $empty;
	}
	my @val = split('[,:]', $v);
	if ((scalar @bd < 49) || (scalar @val < 49)) {
		warning($dev, "($mem) not enough half-hour values!");
		return $empty;
	}
	my $out = '';
	my $sum = 0;
	for (my $i=1; $i<49; $i++) {
		if (($bd[$i] ne '') && ($val[$i] ne '')) {
			$sum += ($val[$i] / 1000);
			$out .= ($val[$i] / 1000);
		}
		$out .= ':';
	}
	return ":$sum\:$out";
}
#-------------------------------------------------------------------------------
sub daily_data($)
{
	my $dev = shift;
	my $code = $meters{$dev}{'code'};
	my $mul  = $meters{$dev}{'multiplier'};
	$mul = 1; #Not used now.
	my ($total, $zone1, $zone2, $zone3, $out);
	$meters{$dev}{'error'} = 0;
	$total = read_daily($dev, '1082165224', $mul);
	$zone1 = read_daily($dev, '1082165225', $mul);
	$zone2 = read_daily($dev, '1082165226', $mul);
	$zone3 = read_daily($dev, '1082165227', $mul);
	$out = "(${code}1)\:$total\:$zone1\:$zone2\:$zone3\::\n";
	$total = read_daily($dev, '1082197992', $mul);
	$zone1 = read_daily($dev, '1082197993', $mul);
	$zone2 = read_daily($dev, '1082197994', $mul);
	$zone3 = read_daily($dev, '1082197995', $mul);
	$out .= "(${code}2)\:$total\:$zone1\:$zone2\:$zone3\::\n";
	$total = read_daily($dev, '1082230760', $mul);
	$zone1 = read_daily($dev, '1082230761', $mul);
	$zone2 = read_daily($dev, '1082230762', $mul);
	$zone3 = read_daily($dev, '1082230763', $mul);
	$out .= "(${code}3)\:$total\:$zone1\:$zone2\:$zone3\::\n";
	$total = read_daily($dev, '1082263528', $mul);
	$zone1 = read_daily($dev, '1082263529', $mul);
	$zone2 = read_daily($dev, '1082263530', $mul);
	$zone3 = read_daily($dev, '1082263531', $mul);
	$out .= "(${code}5)\:$total\:$zone1\:$zone2\:$zone3\::\n";
	$out =~ s/\./,/g;
	return $out;
}
#-------------------------------------------------------------------------------
sub halfhour_data($)
{
	my $dev = shift;
	my $code = $meters{$dev}{'code'};
	my $out;
	$meters{$dev}{'error'} = 0;
	$out  = "(${code}1)".read_hhour($dev, '1082167912')."\n";
	$out .= "(${code}2)".read_hhour($dev, '1082200680')."\n";
	$out .= "(${code}3)".read_hhour($dev, '1082233448')."\n";
	$out .= "(${code}4)".read_hhour($dev, '1082266216')."\n";
	$out =~ s/\./,/g;
	return $out;
}
#-------------------------------------------------------------------------------
sub check_layout($)
{
	my $l = shift;
	my $err_meters = '';
	foreach my $dev (sort(keys %meters)) {
		$meters{$dev}{'error'}
			and $err_meters .= $dev.', ';
	}
	if ($err_meters eq '') {
		print "Layout $l is successfully created.\n";
	} else {
		$err_meters = substr($err_meters, 0, -2);
		write_log("[$l] no data from meter(s): $err_meters!");
		print "Layout $l contains empty values!\n";
	}
}
################################## M A I N #####################################

scalar @ARGV
	or die "Usage: $0 {YYYYMMDDT | last} {!send}\n".
	       "  last  - start receive data from meters;\n".
	       "  !send - disallow send mail.\n";
($to, $allow_mailing) = @ARGV;

#Select time interval FROM=YYYYMMDDT21 TO=YYYYMMDDT2130
if (defined $allow_mailing) {
	$allow_mailing = ($allow_mailing =~/!send/i) ? 'no' : 'yes';
} else {
	$allow_mailing = 'yes';
}
my $need_receive_data = 0;
if ($to =~/LAST/i) {
	my ($day, $month, $year) = dt(time()-84600);
	$to = "$year$month${day}T";
	$need_receive_data = 1;
} else {
	($to =~/^\d{8}T$/)
		or die "Date format should be the YYYYMMDDT or last!\n";
}
my $time = Time::Moment->from_string($to.'00Z')->strftime('%s');
my ($day, $month, $year) = dt($time-84600);
$from = "$year$month${day}T21";
$to .= '2130';
print "Selected time interval from: $from to: $to, send mail: $allow_mailing.\n";

# Unlink old files
unlink "data.xml";
unlink glob "LogBook*.txt";
#unlink glob "layouts/*";

# Check xml files
foreach $meter (keys %meters) {
	(-e "$meter.xml")
		or error("File $meter.xml - not found!");
}
(-e "template.xml")
	or error("File template.xml - not found!");

#############################
# Retrieve data from meters #
#############################
if ($need_receive_data) {
	my $warn = "Can't read data from meter(s):";
	while ($attempts > 0) {
		my $err_meters = '';
		foreach $meter (sort(keys %meters)) {
		# "Electro" return error = 1 as normal program termination.
			if ($meters{$meter}{'error'} != 1) {
				system("c:/electro/xmlgen ACTION=5 ".
				       "INPUT=$meter.xml FROM=DAY2");
				$meters{$meter}{'error'} = ($? >> 8);
				($meters{$meter}{'error'} == 1)
					or $err_meters .= $meter.', ';
			}
		}
		($err_meters eq '')
			and last;
		$err_meters = substr($err_meters, 0, -2);
		$attempts--;
		print "\n[WARNING] $warn $err_meters!\n";
		if ($attempts) {
			print "Attempts remaining: $attempts\n".
			      "Repeat after $delay seconds...\n";
			sleep($delay);
		} else {
			write_log("$warn $err_meters!");
		}
	}
}
# Create data.xml
system("c:/electro/xmlgen ACTION=10 FALLPAR=0 FINFO=0 ".
       "INPUT=template.xml OUTPUT=data.xml FROM=$from TO=$to");
(($? >> 8) == 1)
	or error('Can\'t create data.xml file!');
print "\ndata.xml is created.\n";

# Parse xml data
$doc = XML::Simple->new();
$xml = $doc->XMLin('data.xml', ForceArray => [ 'device' ],
		   KeyAttr => { par => 'memind', device => 'serialnumb' });
#print Dumper($xml)."\n";

# Check date in data.xml
($from eq $xml->{data}->{from})
	or error("Wrong date in data.xml!");

# Set time variables
$xml->{data}->{to} =~ /^\d{2}(\d{2})(\d{2})(\d{2})T/;
my $mmdd = "$2$3";
my $stamp = "$3$2$1";

#######################
# Create layout 30818 #
#######################
$layout  = "((//30818:$mmdd\:1234:++\n";
foreach $meter (sort(keys %meters)) {
	$layout .= daily_data($meter);
}
$layout .= "==))\n";
my $ri = "layouts/RI${stamp}TOR.818";
open($fh, '>', $ri)
	or error("Can't open $ri file!");
print $fh $layout;
close $fh;
check_layout('30818');

#######################
# Create layout 30917 #
#######################
$layout  = "((//30917:$mmdd\:12345:++\n";
foreach $meter (sort(keys %meters)) {
	$layout .= halfhour_data($meter);
}
$layout .= "==))\r\n";
my $rh = "layouts/RH${stamp}TOR.917";
open($fh, '>', $rh)
	or error("Can't open $rh file!");;
print $fh $layout;
close $fh;
check_layout('30917');

###################
# Mailing layouts #
###################
if ($allow_mailing eq 'yes') {
	my $outlook = new Mail::Outlook();
	my $msg = $outlook->create();
	$msg->To($email);
	$msg->Cc('pieu@mail.ru');
	$msg->Subject('Макеты ДП "ТОР"');
	my $message = "Макеты АСКУЭ: 30818, 30917.\n\n";
	$message .= ($error_log eq '') ? "Выполнено без ошибок.\n" :
					 "Выполнено с ошибками:\n".$error_log;
	$msg->Body($message);
	$msg->Attach(getcwd().'/'.$ri);
	$msg->Attach(getcwd().'/'.$rh);
	#$msg->save();
	$msg->send();
	print "Layouts sent by email to: $email.\n";
}
if ($error_log ne '') {
	print "Completed with errors:\n";
	print "$error_log";
	exit 1;
}
print "Successfully completed.\n";
exit 0;

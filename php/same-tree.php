<?php
class TreeNode
{
    public $val = null;
    public $left = null;
    public $right = null;

    function __construct($val = 0, $left = null, $right = null)
    {
        $this->val = $val;
        $this->left = $left;
        $this->right = $right;
    }
}

class Solution
{
    /**
     * @param TreeNode $p
     * @param TreeNode $q
     * @return Boolean
     */
    function isSameTree($p, $q)
    {
        if (is_null($p) && is_null($q))
            return true;
        if (is_null($p) || is_null($q) || $p->val !== $q->val)
            return false;
        return ($this->isSameTree($p->left, $q->left) &&
                $this->isSameTree($p->right, $q->right)) ? true : false;
    }
}

$p = new TreeNode(1, new TreeNode(2, new TreeNode(22), null), new TreeNode(3));
$q = new TreeNode(1, new TreeNode(2, null, new TreeNode(22)), new TreeNode(3));

$o = new Solution;
var_dump($o->isSameTree($p, $q));

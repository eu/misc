<?php
class Solution
{
    /**
     * @param String $s
     * @return Boolean
     */
    function isValid($s)
    {
        $stack = [];
        $push = [ '(', '[', '{' ];
        $pop  = [ ')', ']', '}' ];
        $len = strlen($s);

        for ($i = 0; $i < $len; $i++) {
            if (in_array($s[$i], $push)) {
                array_push($stack, $s[$i]);
                continue;
            }

            $index = array_search($s[$i], $pop);
            if ($index === false)
                continue;

            if (array_pop($stack) !== $push[$index])
                return false;
        }
        return (count($stack) !== 0) ? false : true;
    }
}

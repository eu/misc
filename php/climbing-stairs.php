<?php
class Solution
{
    /**
     * @param Integer $n
     * @return Integer
     */
    function climbStairs($n)
    {
        // Fibonacci number
        $a = $b = 1;
        for ($i = 2; $i <= $n; $i++) {
            $c = $a + $b;
            $a = $b;
            $b = $c;
        }
        return $b;
    }
}

$o = new Solution;
for ($i = 1; $i < 10; $i++)
    echo "$i : " . $o->climbStairs($i) . PHP_EOL;

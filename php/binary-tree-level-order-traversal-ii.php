<?php

class TreeNode
{
    public $val = null;
    public $left = null;
    public $right = null;

    function __construct($val = 0, $left = null, $right = null)
    {
        $this->val = $val;
        $this->left = $left;
        $this->right = $right;
    }
}

class Solution
{
    private $res = [];

    private function traversal(TreeNode $node): void
    {
        $l = (is_null($node->left)) ? null : $node->left->val;
        $r = (is_null($node->right)) ? null : $node->right->val;

        if (is_null($l) && is_null($r))
            return;

        if (is_null($l))
            array_unshift($this->res, [$r]);
        elseif (is_null($r))
            array_unshift($this->res, [$l]);
        else
            array_unshift($this->res, [$l, $r]);

        if (!is_null($l))
            $this->traversal($node->left);
        if (!is_null($r))
            $this->traversal($node->right);
    }
    /**
     * @param TreeNode $root
     * @return Integer[][]
     */
    function levelOrderBottom($root)
    {
        if (!is_null($root->val)) {
            $this->res[] = [$root->val];
            $this->traversal($root);
        }
        return $this->res;
    }    
}

$t = new TreeNode(1, new TreeNode(2, new TreeNode(4)), new TreeNode(3, null, new TreeNode(5)));
$o = new Solution;
print_r($o->levelOrderBottom($t));


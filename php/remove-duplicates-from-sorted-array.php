<?php
class Solution
{
    /**
     * @param Integer[] $nums
     * @return Integer
     */
    function removeDuplicates(&$nums)
    {
        $len = count($nums);

        for ($i = 0, $p = null; $i < $len; $i++) {
            if ($p === $nums[$i])
                unset($nums[$i]);
            else
                $p = $nums[$i];
        }
        return count($nums);
    }
}

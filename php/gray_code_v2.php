<?
/*
 * Mystery Function
 */

function mystery(int $n): int 
{
  return $n ^ ($n >> 1);
}

function mystery_inv(int $n): int {
    for ($b = 0; $n; $n >>= 1)
      $b ^= (int) $n;
    return $b;
}

function name_of_mystery(): string {
  return 'Gray code';
}

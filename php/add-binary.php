<?php
/**
 * @param String $a
 * @param String $b
 * @return String
 */

function addBinary($a, $b)
{
    $len = max(strlen($a), strlen($b));

    $a = str_pad($a, $len, '0', STR_PAD_LEFT);
    $b = str_pad($b, $len, '0', STR_PAD_LEFT);
    $c = str_repeat('0', $len);

    for ($i = $len - 1; $i >= 0; $i--) {
        $r = $a[$i] + $b[$i] + $c[$i];
        $c[$i] = ($r & 1) ? 1 : 0;

        if ($r < 2)
            continue;
        if ($i)
            $c[$i - 1] = 1;
        else
            $c = "1$c";
    }
    return $c;
}

function check($a, $b) {
    return strval(decbin(bindec($a) + bindec($b)));
}

for ($i = 0; $i < 1000000; $i++) {
    $a = strval(decbin(rand(0, 1000000)));
    $b = strval(decbin(rand(0, 1000000)));
    //$a = strval(decbin(rand(0, PHP_INT_MAX))).strval(decbin(rand(0, PHP_INT_MAX)));
    //$b = strval(decbin(rand(0, PHP_INT_MAX))).strval(decbin(rand(0, PHP_INT_MAX)));
    if (addBinary($a, $b) !== check($a, $b)) {
        echo 'Error! '.check($a, $b).PHP_EOL;
    }
}
echo "end\n";

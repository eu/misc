<?php
function plusOne($digits)
{
    $len = count($digits);
    $len = ($len) ? $len - 1 : 0;
    $digits[$len]++;
    for ($i = $len; $i >= 0; $i--) {
        if ($digits[$i] === 10) {
            $digits[$i] = 0;
            if (!$i)
                array_unshift($digits, 1);
            else
                $digits[$i - 1]++;
        }
    }
    return $digits;
}

print_r( plusOne([0]) );
print_r( plusOne([7,2,8,5,0,9,1,2,9,5,3,6,6,7,3,2,8,4,3,7,9,5,7,7,4,7,4,9,4,7,0,1,1,1,7,4,0,0,6]) );

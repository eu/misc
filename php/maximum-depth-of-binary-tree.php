<?php
class TreeNode
{
    public $val = null;
    public $left = null;
    public $right = null;

    function __construct($val = 0, $left = null, $right = null)
    {
        $this->val = $val;
        $this->left = $left;
        $this->right = $right;
    }
}

class Solution
 {
    /**
     * @param TreeNode $root
     * @return Integer
     */
    function depth($node, $n)
    {
        $left = (is_null($node->left)) ? 0 : $this->depth($node->left, $n + 1);
        $right = (is_null($node->right)) ? 0 : $this->depth($node->right, $n + 1);
        return max($n, $left, $right);
    }
    function maxDepth($root)
    {  
       return (is_null($root->val)) ? 0 : $this->depth($root, 1); 
    }
}

$t = new TreeNode(3, new TreeNode(9), new TreeNode(20, new TreeNode(15, new TreeNode()),
    new TreeNode(7)));

$o = new Solution;
echo $o->maxDepth($t);


<?php

$array = [ '2 - 5', '-1 - 4', '7 - 9', '1 - 3', '2 - 6.5', '8 - 10' ];

natsort($array);
foreach ($array as $a)
    $in[] = explode(' - ', $a);
//$in = array_map(function($a) { return explode(' - ', $a); }, $array);
print_r($in);

$res = [];
$step = 0;
while (!empty($in)) {
    list($l, $r) = $in[ array_key_first($in) ];
    do {
	$f = 0;
	foreach ($in as $key => $range) {
	    list($li, $ri) = $range;
	    $step++;
	    if ($l <= $ri && $r >= $li) {
		$l = min($l, $li);
		$r = max($r, $ri);
		unset($in[$key]);
		$f = 1;
	    } else break; // If sorted
	}
    } while ($f);

    $res[] = [ $l, $r ];
}

print_r($res);
echo "Steps = $step\n";

<?php

function f($n)
{
    return $n > 1 ? $n * f($n-1) : 1;
}

for ($i=0; $i<11; $i++)
    echo f($i).PHP_EOL;

<?php
class Solution
{
    /**
     * @param Integer[] $nums1
     * @param Integer $m
     * @param Integer[] $nums2
     * @param Integer $n
     * @return NULL
     */
    function merge(&$nums1, $m, $nums2, $n) {
        for ($i = 0; $i < $n; $i++)
            $nums1[$m + $i] = $nums2[$i];
        sort($nums1);
    }
}

$am = [1,2,9,0,0,0];
$m = 3;

$an = [2,5,6];
$n = 3;

$o = new Solution;
$o->merge($am, $m, $an, $n);

print_r($am);


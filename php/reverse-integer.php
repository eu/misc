<?php
class Solution {

    /**
     * @param Integer $x
     * @return Integer
     */
    private function is_valid($i)
    {
        return ($i < -2147483648 || $i > 2147483647) ? 0 : 1;
    }

    function reverse($x)
    {
        $x = (int) $x;
        if (!$this->is_valid($x))
            return 0;

        $result = '';
        $x = strval($x);

        if ($x[0] == '-') {
            $x = substr($x, 1);
            $result = '-';
        }
        
        $result .= ltrim(strrev($x), '0');
        $result = ($result) ? $result : 0;

        if (!$this->is_valid((int) $result))
            return 0;

        return $result;
    }
}

$o = new Solution;
echo $o->reverse('1000') . PHP_EOL;

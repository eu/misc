<?php
/**
 * Definition for a binary tree node.
 * class TreeNode {
 *     public $val = null;
 *     public $left = null;
 *     public $right = null;
 *     function __construct($val = 0, $left = null, $right = null) {
 *         $this->val = $val;
 *         $this->left = $left;
 *         $this->right = $right;
 *     }
 * }
 */

class Solution
{
    function isSameTree($p, $q)
    {
        if (is_null($p) && is_null($q))
            return true;
        if (is_null($p) || is_null($q) || $p->val !== $q->val)
            return false;
        return ($this->isSameTree($p->left, $q->right) &&
                $this->isSameTree($p->right, $q->left)) ? true : false;
    }
    /**
     * @param TreeNode $root
     * @return Boolean
     */
    function isSymmetric($root)
    {
        return $this->isSameTree($root->left, $root->right);
    }
}

<?php
class Solution
{
    /**
     * @param String[] $strs
     * @return String
     */
    function longestCommonPrefix($strs)
    {
        $w = $strs[0] ?? '';
        $min = strlen($w);

        foreach($strs as $s) {
            $min = min(strlen($s), $min);
            for($i = 0; $i < $min; $i++)
                if ($w[$i] !== $s[$i]) {
                    $min = $i;
                    break;
                }
        }
        return substr($w, 0, $min);
    }
}

$o = new Solution;
echo $o->longestCommonPrefix(["abacv", "aba", "abac"]);
echo PHP_EOL;

<?php declare(strict_types=1);

define('KEY', 'magma.key');

function expand_key(array $key)
{
    $r = [];
    $r[7] = $r[15] = $r[23] = $r[24] = array_slice($key, 0,  4);
    $r[6] = $r[14] = $r[22] = $r[25] = array_slice($key, 4,  4);
    $r[5] = $r[13] = $r[21] = $r[26] = array_slice($key, 8,  4);
    $r[4] = $r[12] = $r[20] = $r[27] = array_slice($key, 12, 4);
    $r[3] = $r[11] = $r[19] = $r[28] = array_slice($key, 16, 4);
    $r[2] = $r[10] = $r[18] = $r[29] = array_slice($key, 20, 4);
    $r[1] = $r[9]  = $r[17] = $r[30] = array_slice($key, 24, 4);
    $r[0] = $r[8]  = $r[16] = $r[31] = array_slice($key, 28, 4);
    return $r;
}

function add(array &$r, array &$a, array&$b)
{
    for ($i = 0; $i < 4; $i++)
	$r[$i] = $a[$i] ^ $b[$i];
}

function add32(array &$r, array &$a, array &$b)
{
    $internal = 0;
    for ($i = 0; $i < 4; $i++) {
        $internal = $a[$i] + $b[$i] + ($internal >> 8);
        $r[$i] = $internal & 0xff;
    }
}

function t(array &$out, array &$in)
{
    $p = [ # 8x16 bytes
	[12,4,6,2,10,5,11,9,14,8,13,7,0,3,15,1],
	[6,8,2,3,9,10,5,12,1,14,4,7,11,13,0,15],
	[11,3,5,8,2,15,10,13,14,1,7,4,12,9,6,0],
	[12,8,2,1,13,4,15,6,7,0,10,5,3,14,9,11],
	[7,15,5,10,8,1,6,13,0,9,3,14,11,4,2,12],
	[5,13,15,6,9,2,12,10,11,7,8,1,4,3,14,0],
	[8,14,2,5,6,9,1,12,15,4,11,0,13,10,3,7],
	[1,7,14,13,0,5,8,3,4,15,10,6,9,12,11,2]
    ];

    for ($i = 0; $i < 4; $i++) {
        $b0 = $in[$i] & 0x0f;
        $b1 = ($in[$i] & 0xf0) >> 4;
	$b0 = $p[$i * 2][$b0];
        $b1 = $p[$i * 2 + 1][$b1];
        $out[$i] = ($b1 << 4) | $b0;
    }
}

function g(array &$r, array &$k, array &$a)
{
    $internal = [ 0, 0, 0, 0 ];

    add32($internal, $a, $k);
    t($internal, $internal);

    $r32 = $internal[3];
    $r32 = ($r32 << 8) + $internal[2];
    $r32 = ($r32 << 8) + $internal[1];
    $r32 = ($r32 << 8) + $internal[0];
    $r32 =  0xffffffff & (($r32 << 11) | ($r32 >> 21));

    $r[0] = 0xff & $r32;
    $r[1] = 0xff & $r32 >> 8;
    $r[2] = 0xff & $r32 >> 16;
    $r[3] = 0xff & $r32 >> 24;
}

function magma_g(array &$r, array &$k, array &$a, bool $last=false)
{

    $a0 = $a1 = $g = [ 0, 0, 0, 0 ];
    for ($i = 0; $i < 4; $i++) {
	$a0[$i] = $a[$i];
	$a1[$i] = $a[$i + 4];
    }
    g($g, $k, $a0);
    add($g, $a1, $g);

    if ($last) {
	for ($i = 0; $i < 4; $i++)
	    $a1[$i] = $g[$i];
    } else {
	for ($i = 0; $i < 4; $i++) {
	    $a1[$i] = $a0[$i];
	    $a0[$i] = $g[$i];
	}
    }

    for ($i = 0; $i < 4; $i++) {
	$r[$i] = $a0[$i];
	$r[$i + 4] = $a1[$i];
    }
}

function magma_encrypt(array &$key, array &$text)
{
    $res = [];
    magma_g($res, $key[0], $text);

    for ($i = 1; $i < 31; $i++)
        magma_g($res, $key[$i], $res);
    magma_g($res, $key[31], $res, true);

    ksort($res);
    return $res;
}

function magma_decrypt(array &$key, array &$cipher)
{
    $res = [];
    magma_g($res, $key[31], $cipher);
    for ($i = 30; $i > 0; $i--)
        magma_g($res, $key[$i], $res);
    magma_g($res, $key[31], $res, true);

    ksort($res);
    return $res;
}

function key_gen(string $key_name)
{
    $bytes = random_bytes(32);
    return file_put_contents($key_name, base64_encode($bytes));
}

function key_read(string $key_name)
{
    $data = file_get_contents($key_name);
    $data = base64_decode($data);
    return array_map('ord', str_split($data, 1));
}

function encrypt(string $text)
{
    $key = key_read(KEY);
    $r = expand_key($key);

    $data = str_split($text, 8);
    $i = array_key_last($data);
    $data[$i] = str_pad($data[$i], 8, ' ');

    $res = [];
    foreach ($data as $i => $text) {
	$text = array_map('ord', str_split($text, 1));
	$a = magma_encrypt($r, $text);
	$res[] = implode('', array_map('chr', $a));
    }

    return base64_encode(implode('', $res));
}

function decrypt(string $b64_cipher)
{
    $key = key_read(KEY);
    $r = expand_key($key);
    $cipher = base64_decode($b64_cipher);
    $data = str_split($cipher, 8);

    $res = [];
    foreach ($data as $i => $cipher) {
	$cipher = array_map('ord', str_split($cipher, 1));
	$a = magma_decrypt($r, $cipher);
	$res[] = implode('', array_map('chr', $a));
    }

    return implode('', $res);
}


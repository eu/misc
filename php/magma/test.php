<?php declare(strict_types=1);

require_once 'magma.php';

$test_key = [ # 32 bytes
    0xff, 0xfe, 0xfd, 0xfc, 0xfb, 0xfa, 0xf9, 0xf8,
    0xf7, 0xf6, 0xf5, 0xf4, 0xf3, 0xf2, 0xf1, 0xf0,
    0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77,
    0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff
];

$encrypt_test_string = [
    0x10, 0x32, 0x54, 0x76, 0x98, 0xba, 0xdc, 0xfe
];

$decrypt_test_string = [
    0x3d, 0xca, 0xd8, 0xc2, 0xe5, 0x01, 0xe9, 0x4e
];

function b4(array $a)
{
    return sprintf("%02x%02x%02x%02x", $a[0], $a[1], $a[2], $a[3]);
}

function b8(array $a)
{
    return sprintf("%02x%02x%02x%02x%02x%02x%02x%02x",
		    $a[0], $a[1], $a[2], $a[3], $a[4], $a[5], $a[6], $a[7]);
}

$r = expand_key($test_key);

for ($i = 0; $i < 32; $i++) {
    echo "K".($i+1)."=".b4($r[$i])."\n";
}

echo "Text:\n".b8($encrypt_test_string).PHP_EOL;
$res = magma_encrypt($r, $encrypt_test_string);
echo "Encrypted text:\n".b8($res).PHP_EOL;

echo "Cipher text:\n".b8($decrypt_test_string).PHP_EOL;
$res = magma_decrypt($r, $decrypt_test_string);
echo "Decrypted text:\n".b8($res).PHP_EOL;

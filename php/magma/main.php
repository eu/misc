<?php declare(strict_types=1);

require_once 'magma.php';

$cipher = encrypt('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod '.
		  'tempor '.'incididunt ut labore et dolore magna aliqua. Ut enim ad minim '.
		  'veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea '.
		  'commodo consequat. ');
echo "Encrypted:\n$cipher\n\n";

$text = decrypt($cipher);
echo "Decrypted:\n$text".PHP_EOL;


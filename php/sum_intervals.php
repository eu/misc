<?php
/*
 * Sum of Intervals
 */

function sum_intervals(array $a): int
{
	do {
		$len = count($a);
		for ($repeat=0, $j=0; $j<$len; $j++) {
			for ($i=0; $i<$len && $i != $j; $i++) {
				$max = max($a[$j][1], $a[$i][1]);
				$min = min($a[$j][0], $a[$i][0]);
                if(($a[$j]==$a[$i]) || 
                   ($a[$j][1]-$a[$j][0] + $a[$i][1]-$a[$i][0]) > ($max-$min)) {
					$a[$j] = [ 0 => $min, 1 => $max ];
					array_splice($a, $i, 1);
					$repeat = 1;
					break;
				}
			}
			if ($repeat)
				break;
		}
	} while($repeat);

	$len = count($a);
	for ($res=0, $i=0; $i<$len; $i++)
		$res += $a[$i][1] - $a[$i][0];

	return $res;
}

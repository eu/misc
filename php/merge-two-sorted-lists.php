<?php
/**
 * Definition for a singly-linked list.
 */

class ListNode
{
    public $val = 0;
    public $next = null;
    function __construct($val = 0, $next = null)
    {
        $this->val = $val;
        $this->next = $next;
    }
}

class Solution
{
    /**
     * @param ListNode $l1
     * @param ListNode $l2
     * @return ListNode
     */
    function mergeTwoLists($l1, $l2)
    {
        $root = $node = new ListNode();

        while (isset($l1->val)) {
            while (isset($l2->val) && $l2->val <= $l1->val) {
                $node->next = new ListNode($l2->val);
                $node = $node->next;
                $l2 = $l2->next;
            }
            $node->next = new ListNode($l1->val);
            $node = $node->next;
            $l1 = $l1->next;
        }
        while (isset($l2->val)) {
            $node->next = new ListNode($l2->val);
            $node = $node->next;
            $l2 = $l2->next;
        }
        return $root->next;
    }        
}

/*
    Input: 1->2->4, 1->3->4
    Output: 1->1->2->3->4->4
 */

$l1 = new ListNode(1, new ListNode(2, new ListNode(4)));
$l2 = new ListNode(1, new ListNode(3, new ListNode(4, new ListNode(9))));

$o = new Solution;
$l3 = $o->mergeTwoLists($l1, $l2);

while(isset($l3->val)) {
    echo "$l3->val";
    $l3 = $l3->next;
    echo isset($l3->val) ? '->' : PHP_EOL;
}


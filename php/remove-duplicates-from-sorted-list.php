<?php
class ListNode
{
    public $val = 0;
    public $next = null;
    function __construct($val = 0, $next = null)
    {
        $this->val = $val;
        $this->next = $next;
    }
}

class Solution
{
    /**
     * @param ListNode $head
     * @return ListNode
     */
    function deleteDuplicates($head)
    {
        // head->val
        // head ->next
        $root = $node = new ListNode();
        $node->val = $head->val;
        while (isset($head->val)) {
            if ($head->val !== $node->val) {
                $node->next = new ListNode($head->val);
                $node = $node->next;
            }
            $head = $head->next;
        }
        return $root;
    }
}

$l1 = new ListNode(0, new ListNode(1, new ListNode(1, new ListNode(1))));
$o = new Solution;
$l2 = $o->deleteDuplicates($l1);

while(isset($l2->val)) {
    echo "$l2->val";
    $l2 = $l2->next;
    echo isset($l2->val) ? '->' : PHP_EOL;
}

<?php
class Solution
{
    /**
     * @param Integer[] $nums
     * @return Integer
     */
    function maxSubArray($nums)
    {
        $len = count($nums);
        if (!$len)
            return null;

        for ($i = 1, $n = $res = $nums[0]; $i < $len; $i++) {
            $n = $nums[$i] + ($n > 0 ? $n : 0);
            $res = max($res, $n);
        }
        return $res;
    }
}


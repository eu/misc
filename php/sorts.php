<?php declare(strict_types=1);

const SIZE = 100;

/*
 * Shell sort
 */
function shell_sort(&$arr)
{
    for ($step = intdiv(SIZE, 2); $step > 0; $step = intdiv($step, 2)) {
        for ($i = $step; $i < SIZE; $i++) {

            $a = $arr[$i];

            for ($j = $i - $step; $j >= 0 && $arr[$j] > $a; $j -= $step)
                $arr[$j + $step] = $arr[$j];

            $arr[$j + $step] = $a;
        }
    }
}


/*
 * Bubble sort
 */
function bubble_sort(&$arr)
{
    for ($j=SIZE-1; $j>1; $j--)
        for ($i=0; $i<$j; $i++)
            if ($arr[$i] > $arr[$i+1]) {
                $a = $arr[$i];
                $arr[$i] = $arr[$i+1];
                $arr[$i+1] = $a;
            }
}

/*
 * Line sort
 */
function line_sort(&$arr)
{
    for ($j=0; $j<SIZE; $j++) {
        $min = $j;
        for ($i=$j; $i<SIZE; $i++) {
            if ($arr[$i] < $arr[$min])
                $min = $i;
        }
        if ($j != $min) {
            $a = $arr[$j];
            $arr[$j] = $arr[$min];
            $arr[$min] = $a;
        }
    }
}

$a = range(1, SIZE);
shuffle($a);

//bubble_sort($a);
//line_sort($a);
shell_sort($a);

echo implode(', ', $a) . PHP_EOL;


<?php
/**
 * @param Integer $x
 * @return Integer
 */

function mySqrt($x)
{
    if ($x < 0)
        return null;
    if ($x < 2)
        return $x;

    $n = $x;
    do 
        $n = intdiv($n, 2);
    while ($n * $n > $x);

    for ($i = $n; $i < $x; $i++) {
        $n = $i * $i;
        if ($n > $x)
            break;
        elseif ($n == $x)
            return $i;
    }
    return $i - 1;
}

for ($x = 0; $x < 10; $x++) {
    $n = mySqrt($x);
    echo "$n * $n = $x\n";
}

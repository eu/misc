<?php
class Solution 
{
    /**
     * @param Integer[] $nums
     * @param Integer $val
     * @return Integer
     */
    function removeElement(&$nums, $val)
    {
        while (($i = array_search($val, $nums)) !== false) 
            unset($nums[$i]);
        return count($nums);
    }
    function _removeElement(&$nums, $val)
    {
        for ($i = count($nums); $i >= 0; $i--)
            if ($nums[$i] === $val)
                unset($nums[$i]);
        return count($nums);
    }

}

$o = new Solution;
$a = [1, 2, 1, 2];
echo $o->removeElement($a, 2).PHP_EOL;
echo $o->_removeElement($a, 2).PHP_EOL;

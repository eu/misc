<?php

class Solution
{
    /**
     * @param String $haystack
     * @param String $needle
     * @return Integer
     */
    function strStr($haystack, $needle)
    {
        $len = strlen($needle);
        if (!$len)
            return 0;

        $shift = strlen($haystack) - $len;
        for ($i = 0; $i <= $shift; $i++) {
            $s = substr($haystack, $i, $len);
            if (!strncmp($s, $needle, $len))
                return $i;
        }
        return -1;    
    }
}

$s1 = "hello";
$s2 = "ll";

$o = new Solution;
echo $o->strStr($s1, $s2) . PHP_EOL;

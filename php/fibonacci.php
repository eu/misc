<?php

const NUM = 10;

function fibonacci($n)
{
    $a = $b = 1;
    for ($i = 2; $i < $n; $i++) {
        $c = $a + $b;
        $a = $b;
        $b = $c;
    }
    return $b;
}

for ($n = 1; $n <= NUM; $n++)
    echo fibonacci($n).PHP_EOL;


<?php
/*
 * Mystery Function
 */

function mystery(int $num): int 
{
    $num++;
    $t = log($num, 2);
    if ($t > $t%10)
        $t = (int) $t + 1;

    $n = pow(2, $t);
    $a = [];

    for ($i=0; $i<$t; $i++) {
        $m = (int) $n / 2;

        if ($num > $m) {
            $num -= $m;
            $a[$i] = 1;
        } else
            $a[$i] = 0;
        $b = $a[$i];
        $n /= 2;
    }

    for ($i=$t-1; $i>0; $i--)
        $a[$i] ^= (int) $a[$i-1];

    return bindec(implode($a));
}

function mystery_inv(int $num): int {
    $a = str_split(decbin($num));
    $t = count($a);
    $n = pow(2, $t);

    for ($i=1; $i<$t; $i++)
        $a[$i] ^= (int) $a[$i-1];

    $num = 0;
    for ($i=0; $i<$t; $i++) {
        $m = (int) $n / 2;

        if ($a[$i] == 1)
            $num += $m;

        $n /= 2;
    }
    return $num; 
}

function name_of_mystery(): string {
  return 'Gray code';
}

<?php
function searchInsert($nums, $target)
{
    $len = count($nums);
    for ($i = 0; $i < $len; $i++)
        if ($nums[$i] >= $target)
            return $i;
    return $len;
}

$nums = [1,3,5,6];
echo searchInsert($nums, 5).PHP_EOL;
echo searchInsert($nums, 2).PHP_EOL;
echo searchInsert($nums, 7).PHP_EOL;
echo searchInsert($nums, 0).PHP_EOL;

<?php declare(strict_types=1);

defined('__CACHE__') || define('__CACHE__', '/tmp/');
defined('CACHE_EXPIRE') || define('CACHE_EXPIRE', 60);

/*
 * File Cache Get
 */
function file_cache_get(string $key, array $args=[])
{
    $vars = '';
    if (!empty($args)) {
	$vars = serialize($args);
	$key .= '-'.md5($vars);
    }

    $cache = __CACHE__.$key;
    if (!file_exists($cache))
	return;
    $file = @fopen($cache, 'r');
    if (!$file)
        return;

    if (flock($file, LOCK_SH)) {
        $data = fread($file, filesize($cache));
        if (!$data)
            return;
        flock($file, LOCK_UN);
    } else {
        fclose($file);
        return;
    }
    fclose($file);

    $fc = unserialize($data);

    // Checking cache expiration
    if ($fc[0] < (int) microtime(true))
	return;

    if ($fc[1] === $vars)
	return $fc[2];
    // Cache collision
    return;
}

/*
 * File Cache Set
 */
function file_cache_set(string $key, &$data, array $args=[], int $ttl=CACHE_EXPIRE): bool
{
    $vars = '';
    if (!empty($args)) {
	$vars = serialize($args);
	$key .= '-'.md5($vars);
    }

    $expire = (int) microtime(true) + $ttl;

    $cache = __CACHE__.$key;
    $file = @fopen($cache, 'w');
    if (!$file)
	return false;

    if (flock($file, LOCK_EX)) {
        if (!@fwrite($file, serialize([ $expire, $vars, $data ])))
	    return false;
        fflush($file);
        flock($file, LOCK_UN);
    } else {
        fclose($file);
        return false;
    }
    fclose($file);
    return true;
}

/*
 * Remove key from File Cache
 */
function file_cache_del(string $key, array $args=[]): bool
{
    if (!empty($args))
	$key .= '-'.md5(serialize($args));

    $cache = __CACHE__.$key;

    // If key "name-md5hash" - delete single key
    if (preg_match("/^[a-z0-9\_]+-[a-f0-9]{32}/i", $key)) {
	if (file_exists($cache))
	    return unlink($cache);
	return false;
    }
    // Otherwise remove all keys starting with "name*"
    // WARNING: Empty key value will remove all keys!
    return (bool) array_map('unlink', glob($cache.'*'));
}

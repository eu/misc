<?php declare(strict_types=1);

defined('CACHE_EXPIRE') || define('CACHE_EXPIRE', 60);
define('SK', 'CACHE');

/*
 * Session Cache Get
 */
function sess_cache_get(string $key, array $args=[])
{
    $vars = '';
    if (!empty($args)) {
	$vars = serialize($args);
	$key .= '-'.md5($vars);
    }

    if (!isset($_SESSION[SK][$key][0], $_SESSION[SK][$key][1], $_SESSION[SK][$key][2]))
	return;

    // Checking cache expiration
    if ($_SESSION[SK][$key][0] < (int) microtime(true))
	return;

    if ($_SESSION[SK][$key][1] === $vars)
	return $_SESSION[SK][$key][2];
    // Cache collision
}

/*
 * Session Cache Set
 */
function sess_cache_set(string $key, &$data, array $args=[], int $ttl=CACHE_EXPIRE): bool
{
    $vars = '';
    if (!empty($args)) {
	$vars = serialize($args);
	$key .= '-'.md5($vars);
    }

    $expire = (int) microtime(true) + $ttl;
    $_SESSION[SK][$key] = [ $expire, $vars, $data ];
    return true;
}

/*
 * Remove key from File Cache
 */
function sess_cache_del(string $key, array $args=[]): bool
{
    if (!isset($_SESSION[SK]))
	return false;

    if (!empty($args))
	$key .= '-'.md5(serialize($args));

    // If key "name-md5hash" - unset single key
    if (preg_match("/^[a-z0-9\_]+-[a-f0-9]{32}/i", $key)) {
	unset($_SESSION[SK][$key]);
	return true;
    }
    die($key);
    // Otherwise remove all keys starting with "name*"
    // WARNING: Empty key value will unset all keys!
    foreach ($_SESSION as $k => $v) {
	if (strpos($k, $key) === 0)
	    unset($_SESSION[SK][$k]);
    }
    return true;
}

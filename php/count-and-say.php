<?php
/*
 1.     1
 2.     11
 3.     21
 4.     1211
 5.     111221 
 6.     312211
 7.     13112221
 8.     1113213211
 9.     31131211131221
10.     13211311123113112211
 */

class Solution
{
    private function step($s)
    {
        $len = strlen($s);
        if (!$len)
            return null;

        $result = '';
        for ($i = $count = 0, $num = $s[0]; $i < $len; $i++) {
            if ($num !== $s[$i]) {
                $result .= $count . $num;
                $num = $s[$i];
                $count = 1;
            } else
                $count++;
        }
        $result .= $count . $num;
        
        return $result;
    }
    /**
     * @param Integer $n
     * @return String
     */
    function countAndSay($n)
    {
        if ($n < 1 || $n > 30)
            return null;
        for ($i = 0, $s = '1', $n--; $i < $n; $i++) {
            $s = $this->step($s);
            echo $s . PHP_EOL;
        }
    }
}

$o = new Solution;
$o -> countAndSay(15);

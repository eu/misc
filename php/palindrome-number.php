<?php
class Solution {

    /**
     * @param Integer $x
     * @return Boolean
     */
    function isPalindrome($x) {
        $s = strval($x);
        $r = strrev($s);
        return (strcmp($s, $r)) ? false : true;
        #return (bool) !strcmp("$x", strrev("$x"));
    }
}

$o = new Solution;
var_dump($o->isPalindrome(123454321));

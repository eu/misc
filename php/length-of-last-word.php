<?php
class Solution
{
    /**
     * @param String $s
     * @return Integer
     */
    function lengthOfLastWord($s)
    {
        $s = rtrim($s);
        $len = strlen($s);

        if (!$len)
            return 0;

        for($i = $len - 1; $i >= 0; $i--) {
            if ($s[$i] === ' ')
                break;
        }
        return $len - $i - 1;
    }
}

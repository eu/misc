<?php
class Solution
{
    /**
     * @param String $s
     * @return Integer
     */
    function romanToInt($s)
    {    
        $roman = [
            'M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 
            'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 
            'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1
        ];

        $len = strlen($s);
        $integer = 0;

        for ($i = 0; $i < $len; $i++) {
            if ($len - $i > 1) {
                $key = substr($s, $i, 2);
                if (array_key_exists($key, $roman)) {
                    $integer += $roman[$key];
                    $i++;
                    continue;
                }
            }
            $integer += $roman[ $s[$i] ];
        }
        return $integer;
    }
}

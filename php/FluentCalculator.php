<?php
/*
 * Your task is to create a class that implements a simple calculator
 * with fluent syntax.
 */

class FluentCalculator
{
    const MAX = 999999999;
    protected $exp = null;

    public static function init() { return new FluentCalculator; }

    function __get($property)
    {
        $n = null;
        switch ($property) {
        case 'one':     $n = 1; break;
        case 'two':     $n = 2; break;
        case 'three':   $n = 3; break;
        case 'four':    $n = 4; break;
        case 'five':    $n = 5; break;
        case 'six':     $n = 6; break;
        case 'seven':   $n = 7; break;
        case 'eight':   $n = 8; break;
        case 'nine':    $n = 9; break;
        case 'zero':    $n = 0; break;
        case 'minus':
            if (!strlen($this->exp))
                $n = '-';
        }
        if ($n !== null) {
            $this->exp .= $n;
            return $this;
        }

        switch($property) {
        case 'plus':    $n = '+'; break;
        case 'minus':   $n = '-'; break;
        case 'times':   $n = '*'; break;
        case 'dividedBy':$n = '/'; break;
        case 'result':  $n = ''; break;
        default:
        throw new InvalidInputException;
        }
        if (preg_match("#[+-/*]+$#", $this->exp))
            $this->exp = substr($this->exp, 0, -1);

        $round = 'floor';
        if (preg_match("#^(-?\d+)([-+/*])(\d+)$#", $this->exp, $m)) {
            if ($m[1] > self::MAX || $m[3] > self::MAX)
                throw new DigitCountOverflowException;
            $this->exp = (int) $m[1] . $m[2]. (int) $m[3];
            if ($m[2] == '/')
                if ($m[1] < 0)
                    $round = 'ceil';
                elseif($m[1] < $m[3])
                    $this->exp = '0';
        } else if (preg_match("#^(-?\d+)$#", $this->exp, $m))
            $this->exp = (int) $m[1];

        if (preg_match("#/0$#", $this->exp))
            throw new DivisionByZeroException;

        @eval('$result = (int) '."$round($this->exp);");
        if (abs($result) > self::MAX)
            throw new DigitCountOverflowException;

        $this->exp = $result . $n;
        return $this;
    }

    function __call($method, $argv)
    {
        $this->__get($method);
        $this->__get('result');
        return (int) $this->exp;
    }
}

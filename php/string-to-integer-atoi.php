<?php
class Solution 
{
    /**
     * @param String $str
     * @return Integer
     */
    function myAtoi($str)
    {
        $i = (preg_match('/^([-+\s\d]+)/', $str, $str) === 1) ? intval($str[1]) : 0;

        if ($i < -2147483648)
            return -2147483648;
        if ($i > 2147483647)
            return 2147483647;
        return $i;
    }
}

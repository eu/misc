// ==UserScript==
// @id             bot
// @name           bbgam bot
// @version        1.0
// @include        http://s.bbgam.com/game/*
// @grant GM_getValue
// @grant GM_setValue
// ==/UserScript==

var stop=1;
var allow_strong_casts = 1;
var auto_kill = 0;

var crit_hp = 30;
var crit_mp = 300;
var low_hp  = 50;

function show_tag(tag)
{
	var out = "";
	for (var i=0; i<15; i++) {
		out += (document.getElementsByTagName(tag)[i]) ? tag+"["+i+"]: "+document.getElementsByTagName(tag)[i].textContent+"\n" : "";
	}
	window.alert(window.location.href+"\n"+out);
}
function hp()
{
	for (var i=1; i<3; i++) {
		if (document.getElementsByTagName('b')[i]) {
			if (/document/.test(document.getElementsByTagName('b')[i].textContent))
			return ~~(/(\d+)\/\d+$/.exec(document.getElementsByTagName('b')[i].textContent))[1];
		}
	}
	return 0;
}
function mp()
{
	for (var i=1; i<3; i++) {
		if (document.getElementsByTagName('b')[i]) {
			if (/document/.test(document.getElementsByTagName('b')[i].textContent)) {
				if (document.getElementsByTagName('b')[i+1]) {
					return ~~(/(\d+)\/\d+$/.exec(document.getElementsByTagName('b')[i+1].textContent))[1];
				}
			}
		}
	}
	return 0;
}
function enemy_hp()
{
	for (var i=0; i<3; i++) {
		if (document.getElementsByTagName('p')[i]) {
			if (/Раунд/.test(document.getElementsByTagName('p')[i].textContent))
				return ~~(/\((\d+)\/\d+\)$/.exec(document.getElementsByTagName('p')[i].textContent))[1];
		}
	}
	return 0;
}

window.onload = function()
{
	if (stop)
		return;
	document.body.insertAdjacentHTML('afterBegin',
'<div style="width:98%; margin:2px; color:#888">'+
'auto='+auto_kill+', strong='+allow_strong_casts+'.'+
'</div>');
	for (var i=0; i<16; i++) {
		if (!document.getElementsByTagName("a")[i])
			continue;
		if (document.getElementsByTagName("a")[i].textContent.match(/Продолжить/) ||
		    document.getElementsByTagName("a")[i].textContent.match(/Сражение завершено/) ||
		    (document.getElementsByTagName("a")[i].textContent.match(/атаковать/) && auto_kill)) {
			document.getElementsByTagName("a")[i].click();
			return;
		}
		if (hp() > low_hp) {
			var cast = /Магическая стрела/;
			var e_hp = enemy_hp();
			switch(allow_strong_casts ? true : 0) {
				case (e_hp > 20):
					cast = /Шаровая молния/;
					break;
				case (e_hp > 12):
					cast = /Огненный шар/;
					break;
				case (e_hp > 5):
					cast = /Ледяная стрела/;
					break;
				case (e_hp > 1):
					cast = /Разряд молнии/;
					break;
			}
			if (document.getElementsByTagName("a")[i].textContent.match(cast)) {
				document.getElementsByTagName("a")[i].click();
				return;
			}
		} else {
			if (document.getElementsByTagName("a")[i].textContent.match(/Лечение легких ранений/)) {
				if ((hp()>crit_hp) && (mp()>crit_mp)) {
					document.getElementsByTagName("a")[i].click();
					return;
				}
			}
		}
	}

	if (document.getElementsByTagName("input")[1]) {
		window.alert('CAPTCHA !');
		return;
	}
	window.setInterval("window.location.reload()", Math.floor(Math.random()*5000)+30000);
}
